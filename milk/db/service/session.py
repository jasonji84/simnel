# -*- coding: utf-8 -*-


"""
对话表单
数据库服务

TODO: 1. 对话数据库服务(历史数据存留)
      2. 对话缓存服务(业务使用)

REMARK: 
"""

#
from common.api import MysqlDB


class SessionDB(MysqlDB):
    """
    @summary: 对话表单 
              数据库操作封装
    """
    _tag = 'Session'

    def __init__(self, params={}):
        """
        @summary: 构造器
        """
        super(SessionDB, self).__init__(params)
        pass
