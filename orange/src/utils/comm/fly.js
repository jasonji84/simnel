// 通信Flyio API

import {baseUrl} from '@/utils/comm/config'
import {sessionKeyLocalStorage as sessionKey} from '@/utils/cache/config'
import {getStorageSync} from '@/utils/cache/storage'

export function request (url, method, data, success, fail, withSession = false) {
  // console.log('[request] method: ', method)
  // 目前支持GET和POST请求
  // 其中，GET不带会话，POST都可以
  var Fly = require('flyio/dist/npm/wx')
  var fly = new Fly()
  // 预处理
  let obj = prepare(url, method, data, withSession)
  // console.log('[request] posts: ', posts)
  // 执行请求
  return fly.request(obj.url, obj.data, {
    method: obj.method,
    timeout: 5000 // 超时设置为5s
  }).then(function (res) {
    typeof success === 'function' && success(res)
  }).catch(function (error) {
    typeof fail === 'function' && fail(error)
  })
}

// 通信预处理
export function prepare (url, method, data, withSession) {
  let posts = {}
  let fullUrl = url.startsWith('http') ? url : (baseUrl + url)
  // 如果请求是GET，则在后面加上参数（固定规则）
  if (method === 'GET' || method === 'get') {
    for (var key in data) {
      if (fullUrl.indexOf('?') >= 0) {
        fullUrl += '&' + key + '=' + data[key]
      } else {
        fullUrl += '?' + key + '=' + data[key]
      }
    }
  } else if (method === 'POST' || method === 'post') {
    posts['Data'] = data
    if (withSession === true) {
      posts['Session'] = getStorageSync(sessionKey)
    }
  }
  return {url: fullUrl, data: posts, method: method}
}
