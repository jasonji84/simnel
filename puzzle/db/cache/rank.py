# -*- coding: utf-8 -*-

"""
榜单点评
缓存数据服务

TODO: 1. 榜单缓存数据服务;

REMARK: 
"""

from common.api import BaseComponent
from common.api import CacheService


class CommentRankCache(BaseComponent):
    """
    @summary: 点评榜单
              缓存服务操作封装
    --------------------------------------------
    @note: 2019.03.13   新增     完成
           2019.xx.xx
    """
    # 利率点评榜单
    # key: CommentRank##Counter
    # value: [{}, {}], 格式
    #        - Rank: int, 排名
    #        - ProductId: str, 产品ID
    #        - Rate: float, 平均利率
    #        - Score': int, 利率指数 
    # 投票点评榜单
    # key: CommentRank##Pop
    # value: [{}, {}], 格式
    #        - Rank: int, 排名
    #        - ProductId: str, 产品ID
    #        - Rate: float, 平均利率
    #        - Score': int, 点评热度
    _head = 'CommentRank'
    _tags = ['Counter', 'Pop']

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(CommentRankCache, self).__init__(params)
        #
        self.cs = CacheService()

    def query(self, tag):
        """
        @summary: 榜单查询 
        @param tag: str, 榜单标签 
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.03.13    新增接口     完成
               2019.xx.xx
        """
        res = None
        if tag in self._tags:
            res = self.cs.get('{}##{}##'.format(self._head, tag))
        return res 

    def set(self, tag, data):
        """
        @summary: 新增记录
        @param tag: str, 榜单标签  
        @param data: dict, 数据对象
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.03.13    新增接口     完成
               2019.xx.xx
        """
        res = False
        if tag in self._tags:
            res = self.cs.set('{}##{}##'.format(self._head, tag), data)
        return res 

    def remove(self, tag):
        """
        @summary: 删除数据
        @param tag: str, 榜单标签  
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.03.13  新增接口     完成
               2019.xx.xx
        """
        res = False
        if tag in self._tags:
            res = self.cs.remove('{}##{}##'.format(self._head, tag))
        return res 

    def mdel(self):
        """
        @summary: 批量删除数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.03.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##*'.format(self._head))

