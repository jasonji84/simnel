# -*- coding: utf-8 -*-

"""
榜单数据服务测试

TODO: 1. 产品榜单缓存服务(更新+查询)
      2. 热搜榜单缓存服务
      3. 产品统计（搜索使用, 后续关联产品榜单）

REMARK: 
"""

# 产品统计
from puzzle.biz.stats import updateProductStats
updateProductStats()

# 首页榜单 
from puzzle.biz.rank import updateCommentRank
#updateCommentRank()
from milk.db.api import getCommentRank
print(getCommentRank(tag='Counter'))

