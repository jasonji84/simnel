# -*- coding: utf-8 -*-

"""
任务定义

TODO: 1. 产品统计
      2. 榜单统计 
      3. 点评统计

REMARK: 
"""
   
from clock import updateCommentRankTask
from clock import updateCommentStatsTask
from clock import updateProductStatsTask

updateCommentStatsTask(True)
updateProductStatsTask()
updateCommentRankTask()
