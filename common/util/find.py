# -*- coding: utf-8 -*-

"""
数据获取组件

TODO: 1. 参数查询

"""


def find_value(data, keys=[], default_value=None):
    """
    @summary: 参数查询  
    @param data: dict类型, 数据集合
    @param keys: list类型, 查询字段列表
    @param default_value: 若不存在, 返回默认值
    @return: 返回查询结果
    ---------------------------------------------
    @note: 2019.03.16   增加注释        完成
           2019.xx.xx 
    """
    ret = data 
    for key in keys:
        if key not in ret:
            return default_value
        ret = ret[key]
    return ret 
             


