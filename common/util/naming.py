# coding=utf-8

"""
命名相关组件

TODO: 1. 随机命名(uuid + 时间戳) 
      2. 随机序列(base64 + 指定长度)
      3. 哈希命名(hash   + 指定长度) 

REMARK: 
"""

def randomNaming():
    """
    @summary: 随机命名
    @return: str, 随机序列(格式: 时间戳_uuid) 
    ---------------------------------------------
    @note: 2019.03.12   增加注释        完成
           2019.xx.xx   
    """ 
    import uuid
    import datetime as dt
    #
    prefix = dt.datetime.now().strftime('%Y%m%d%H%M%S')
    suffix = str(uuid.uuid4())
    return prefix+'-'+suffix


def randomSeries(size=168):
    """
    @summary: 随机序列(采用urandom)
    @return: str, 随机序列
    ---------------------------------------------
    @note: 2019.03.12   增加注释        完成
           2019.xx.xx
    """
    import os
    from base64 import b64encode
    #
    return b64encode(os.urandom(size)).decode('utf-8')


def hashNaming(text, size=10):
    """
    @summary: hash编码命名
    @param text: str, 字符串
    @param size: int, 指定长度
    @return: str, 序列
    ---------------------------------------------
    @note: 2019.03.12   增加注释        完成
           2019.xx.xx   
    """
    import hashlib
    #
    return hashlib.sha256(text.encode('utf-8')).hexdigest()[:size]
