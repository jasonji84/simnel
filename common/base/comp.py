# coding=utf-8

"""
通用组件对象 

TODO: 1. 基本成员变量 
      2. 参数载入功能 
      3. 日志功能

REMARK:

"""

import sys
import imp
imp.reload(sys)
#
from abc import ABCMeta, abstractmethod
from logbook import Logger


class BaseComponent(object):
    """
    @summary: 组件的抽象类 
              设定为所有对象（包括数据库代理，数据服务，算法模型，等）
    """
    __metaclss__ = ABCMeta

    def __init__(self, params={}):
        """
        @summary: constructor
        @param params: dict, parameter set
        @return:
        """        
        self.log = self.__init_logger()

    def _load_param(self, data, key, default=None):
        """
        @summary: load parameter 
        @param data: dict, parameter set 
        @param key: str, field
        @param default: default value
        @return: result value
        """
        return (data[key] if key in data else default)

    def __init_logger(self):
        """
        @summary; initialize logger
        @return: logger object
        """
        logger = Logger('[{}]'.format(self.__class__.__name__))
        return logger
