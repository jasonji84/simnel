# -*- coding: utf-8 -*-

"""
用户行为表单
数据库服务

TODO: 1. 登录行为
      2. 搜索行为1 - 搜索栏搜索
      3. 搜索行为2 - 热词榜单搜索
      4. 提交产品名称
      5. 分享行为

REMARK: 1. 问卷点评行为（关闭）
        2. 页面进出行为（回炉再造）
"""

#
from common.api import MysqlDB


class LoginActionDB(MysqlDB):
    """
    @summary: 登陆登出行为
              表单及数据读写封装
    """
    _tag = 'LoginAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(LoginActionDB, self).__init__(params)
        pass


class SearchbarActionDB(MysqlDB):
    """
    @summary: 搜索栏行为
              表单及数据读写封装
    """
    _tag = 'SearchbarAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(SearchbarActionDB, self).__init__(params)
        pass


class KeywordActionDB(MysqlDB):
    """
    @summary: 热搜榜单行为
              表单及数据读写封装
    """
    _tag = 'KeywordAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(KeywordActionDB, self).__init__(params)
        pass


class UploadProductActionDB(MysqlDB):
    """
    @summary: 用户上传自定义产品名称
              表单及数据读写封装
    """
    _tag = 'UploadProductAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(UploadProductActionDB, self).__init__(params)
        pass


class AccessActionDB(MysqlDB):
    """
    @summary: 页面进出行为
              表单及数据读写封装
    """
    _tag = 'AccessAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return: 
        """
        super(AccessActionDB, self).__init__(params)
        pass


class ShareActionDB(MysqlDB):
    """
    @summary: 用户分享行为 
              表单及数据读写封装
    """
    _tag = 'ShareAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return: 
        """
        super(ShareActionDB, self).__init__(params)
        pass


class HitShareActionDB(MysqlDB):
    """
    @summary: 用户点击分享行为
              表单及数据读写封装
    """
    _tag = 'HitShareAction'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(HitShareActionDB, self).__init__(params)
        pass
