// 分享相关

// 随机序列
import {randomSeries} from '@/utils/random'
// 路由相关
import {getRouting, isEmpty} from '@/utils/router/route'
// 通信相关
import {actionShareUrl} from '@/utils/comm/config'
import {request} from '@/utils/comm/fly'
// 用户行为
import {shareApp} from '@/utils/profile/api'

export function sharePage (from = '', fromPage = '', toPage = '', params = {}, slogan = '', action = {}) {
  // 生成分享链接
  let url = getRouting(toPage)
  if (url === '') {
    // 容错处理
    url = getRouting('home')
  }
  // 添加来源页面
  url = url + '?source=' + fromPage
  // 添加操作类型
  url = url + '&type=share'
  // 添加分享码
  let code = randomSeries(128)
  url = url + '&code=' + code
  // 添加传参
  if (!isEmpty(params)) {
    url = url + '&message=' + JSON.stringify(params)
  }
  // 设置标语
  var title = '青柠网贷点评'
  if (slogan !== '') {
    title = slogan
  } else {
    title = getSlogan(fromPage)
  }
  // 记录分享行为
  postShare(from, fromPage, code)
  // 用户行为
  let posts = {
    action: from,
    scene: getRouting(fromPage)
  }
  if (Object.keys(action).length > 0) {
    posts.product = {}
    posts.product.id = action.id
    posts.product.name = action.name
  }
  console.log(action)
  shareApp(posts)
  return {
    title: title,
    path: url
  }
}

// 记录用户分享
function postShare (source, page, code, others, successHandler, failHandler) {
  // console.log('[doShare] called ', source, page, others)
  let posts = {OpenType: source, Page: page, ShareCode: code, ProductId: '', BizCode: ''}
  if (typeof others !== 'undefined') {
    posts.ProductId = others.id || ''
    posts.BizCode = others.code || ''
  }
  request(actionShareUrl, 'post', posts,
    function success (res) {
      typeof successHandler === 'function' && successHandler(res)
    },
    function fail (res) {
      typeof failHandler === 'function' && failHandler(res)
    }, true)
}

// 获取分享标语
function getSlogan (page = '') {
  let slogan = '借钱别被忽悠，听网友的'
  if (page === 'counter' || page === 'counterbro') {
    slogan = '借钱真的便宜？真实利率你会算吗?'
  } else if (page === 'result') {
    slogan = '借钱真的便宜？真实利率是...'
  }
  return slogan
}
