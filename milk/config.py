# coding=utf-8

"""
系统配置

TODO:

REMARK:
"""
from . import projectPath

"""
@summary: 系统配置
"""

# 路径
imagePath = '{}/data/image'.format(projectPath)
#
tablePath = '{}/data/table/'.format(projectPath)
#
databaseConfigFile = '{}/data/db.ini'.format(projectPath)
#
robotFile = '{}/data/other/robots.txt'.format(projectPath)

# 页面定义
#pages = ['home']

# 数据库
#database = 'prism'

# 默认网贷产品LOGO
defaultIcon = 'default_icon.png'
