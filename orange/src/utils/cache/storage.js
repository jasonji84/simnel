// 本地存储相关操作

// 存储数据（同步）
export function setStorageSync (key, value) {
  try {
    wx.setStorageSync(key, value)
  } catch (e) {
    console.warn('[setStorageSync] fail  key: ', key, ' value: ', value)
  }
}

// 获取数据（同步）
export function getStorageSync (key, defaultValue = null) {
  var res = defaultValue
  try {
    let value = wx.getStorageSync(key)
    if (value) {
      res = value
    }
  } catch (e) {
    console.warn('[getStorageSync] fail  key:', key)
  }
  return res
}

// 存储数据（非同步）
export function getStorage (key, callback1, callback2, callback3) {
  wx.getStorage({
    key: key,
    success: function (res) {
      typeof callback1 === 'function' && callback1(res)
    },
    fail: function (res) {
      typeof callback2 === 'function' && callback2(res)
    },
    complete: function (res) {
      typeof callback3 === 'function' && callback3(res)
    }
  })
}

// 存储数据（非同步）
export function setStorage (key, value, callback1, callback2, callback3) {
  let data = value
  wx.setStorage({
    key: key,
    data: data,
    success: function (res) {
      typeof callback1 === 'function' && callback1(res)
    },
    fail: function (res) {
      typeof callback2 === 'function' && callback2(res)
    },
    complete: function (res) {
      typeof callback3 === 'function' && callback3(res)
    }
  })
}

// 删除数据（非同步）
export function removeStorage (key, callback1, callback2, callback3) {
  wx.removeStorage({
    key: key,
    success: function (res) {
      typeof callback1 === 'function' && callback1(res)
    },
    fail: function (res) {
      typeof callback2 === 'function' && callback2(res)
    },
    complete: function (res) {
      typeof callback3 === 'function' && callback3(res)
    }
  })
}
