# coding=utf-8

"""
表单配置组件

TODO: 1. 载入表单配置
      2. 获取表单配置 

REMARK:
"""

import os 
import json
#
from ..base.comp import BaseComponent


class TableConfig(BaseComponent):
    """
    @summary: 表单配置组件 
    """
    _table_map = {}

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 配置参数
        """
        super(TableConfig, self).__init__(params)
        # load table config
        files = self._load_param(params, 'TableConfigFileList', [])
        #
        self._table_map = {}
        for fname in files:
            if os.path.isfile(fname):
                with open(fname, 'r') as f:
                    data = json.load(f)
                    for da in data:
                        self._table_map[da['Tag']] = {'Table':self._load_param(da, 'Table', ''),
                                                      'Field':self._load_param(da, 'Field', [])}
        pass

    def find_table(self, key):
        """
        @summary: 表单配置信息查询
        @param key: str类型, 查询值
        @return: dict类型, 查询结果
        """
        ret = {}
        if key in self._table_map:
            ret = self._table_map[key]
        return ret
