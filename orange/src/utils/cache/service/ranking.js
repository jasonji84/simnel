// 榜单数据缓存
//
// 远程链接
import {productRankUrl as rankingUrl, productImageUrl as imageUrl} from '@/utils/comm/config'
// 远程通信
import {request} from '@/utils/comm/fly'
// 时间函数
import {getCurrentTime, calTimeSpan} from '@/utils/clock'
// 本地缓存
import {productRankLocalStorage as rankingKey} from '@/utils/cache/config'
import {setStorage, getStorageSync} from '@/utils/cache/storage'

// 频道列表
var channels = [
  {label: '低利率排序', tag: 'Counter'},
  {label: '点评热度排序', tag: 'Pop'}
]
// 限定条件：2个小时
var threshold = 2
var unit = 'hr'
// 数据仓库
var databox = {}
// 后处理回调函数
var postCallback = null
// var currentTag = ''

export function getChannel () {
  // 获取榜单频道列表
  return channels
}

export function getRanking (tag, force = false, callback = null) {
  // console.log('[getRanking] tag: ', tag, ' force: ', force)
  // 获取产品榜单数据
  postCallback = callback
  // console.log('typeof postCallback: ', (typeof postCallback === 'function'))
  // 当前标签
  // currentTag = tag
  // console.log('[getRanking] check(tag): ', check(tag))
  // console.log('[getRanking] databox:    ', databox)
  // 1. 强制刷新; 2. 数据没有缓存或数据过期
  if (force === true || !check(tag)) {
    requestData(tag)
  } else {
    // 否则，载入缓存数据
    typeof postCallback === 'function' && postCallback(databox[tag].data)
  }
}

function check (tag) {
  // 有效性判定
  var res = false
  // console.log('[check] Object.keys(databox).length === 0: ', (Object.keys(databox).length === 0))
  // console.log('[check] Object.keys(databox).length: ', Object.keys(databox).length)
  // 数据是否初始化
  if (Object.keys(databox).length === 0) {
    databox = getStorageSync(rankingKey, {})
  }
  // console.log('[check] (tag in databox): ', (tag in databox))
  // console.log('[check] tag: ', tag)
  // console.log('[check] databox: ', databox)
  // 数据是否存在
  if (tag in databox) {
    // console.log('[check]  isExpired: ', isExpired(databox[tag].time))
    // 条件2: 是否过期
    res = isExpired(databox[tag].time)
  }
  return res
}

function isExpired (timestamp) {
  // 判定是否过期
  var res = true
  if (timestamp instanceof Date) {
    let currentTime = getCurrentTime()
    let val = calTimeSpan(timestamp, currentTime, unit)
    res = (val >= threshold)
  }
  return res
}

function requestData (tag) {
  // console.log('[ranking][requestData]: ', tag)
  // 远程获取数据
  let posts = {'Tag': tag}
  request(rankingUrl, 'post', posts,
    function success (res) {
      // 成功获取数据后处理
      // 备注：状态为200，通信已校验
      try {
        // 解析数据
        let objs = res.data.Result
        let array = Array(objs.length)
        for (var i in objs) {
          let obj = objs[i]
          let iconUrl = imageUrl + '/' + obj.Icon
          array[obj.Rank] = {id: obj.ProductId, name: obj.Name, icon: iconUrl, rate: (obj.Air * 100).toFixed(2), stars: obj.Index}
        }
        // 保存数据
        databox[tag] = {time: getCurrentTime(), data: array}
        setStorage(rankingKey, databox)
        // 后处理
        typeof postCallback === 'function' && postCallback(array)
      } catch (e) {
        console.warn('[ranking][requestData] e: ', e)
      }
    }, null, true)
}
