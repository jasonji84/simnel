# -*- coding: utf-8 -*-

"""
投票点评
数据服务

TODO: 1. ;

REMARK: 
"""

def countSingleVoteComment(pid, vid):
    """
    @summary: 投票点评统计
    @param pid: str, 网贷产品ID
    @param vid: str, 投票项目ID
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.06.03  新增接口     完成
           2019.xx.xx     
    """
    #
    from common.api import MysqlDB
    #
    sql = """
          select a.ProductId
                ,a.VoteId
                ,a.TopicId
                ,a.OptionTag
                ,count(distinct a.OpenId)
          from uc_vote_comment as a
          join op_product_flag as b 
          on b.ProductId=a.ProductId
          where a.ProductId = '{}'
                and a.VoteId = '{}'
                and a.Latest = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                and b.OpenState = 1 
                and b.VoteFlag = 1
          group by a.ProductId,a.VoteId,a.TopicId,a.OptionTag
          """.format(pid, vid)
    #print('[countSingleVoteComment] sql: ', sql)
    res = MysqlDB().execute(sql)
    #print(res)
    return res
