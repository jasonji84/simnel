# -*- coding: utf-8 -*-

"""
网贷产品表单
数据库服务

TODO: 1. 产品基本信息;
      2. 产品权限数据;
      3. 产品统计数据;

REMARK: 
"""

#
from ..mysql.api import MysqlServiceDB

class ProductBaseDB(MysqlServiceDB):
    """
    @summary: 网贷产品基本信息
              表单及数据服务封装
    """
    _tag = 'ProductBase'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(ProductBaseDB, self).__init__(params)
        pass


class ProductFlagDB(MysqlServiceDB):
    """
    @summary: 网贷产品权限状态
              表单及数据服务封装
    """
    _tag = 'ProductFlag'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(ProductFlagDB, self).__init__(params)
        pass


class ProductStatsDB(MysqlServiceDB):
    """
    @summary: 网贷产品统计
              表单及数据服务封装
    """
    _tag = 'ProductStats'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(ProductStatsDB, self).__init__(params)
        pass
