# -*- coding: utf-8 -*-

"""
利率点评
数据服务

TODO: 1. 利率点评统计
         - 点评数据统计
         - 分段区间统计

REMARK: 
"""


def updateCounterCommentStats(params={}):
    """
    @summary: 更新利率点评
              统计数据
    @param params: dict, 配置参数
    @return:  
    --------------------------------------------
    @note: 2019.02.16  新增接口           完成
           2019.02.17  加入配置参数       完成
                       - Reload: bool, 是否重置更新, 默认false
           2019.02.18  新增利率点评统计   完成
           2019.xx.xx
    """
    from ..db.stats import countDistinctCounterComment
    from ..db.stats import countCounterCommentBySegment 
    from ..db.cache.counter import CounterCommentCache
    from ..db.cache.counter import CounterCommentStatsCache
    #
    from common.api import find_value
    # 
    reload_flag = find_value(params, ['Reload'], False)
    #【产品明细页】利率分段统计
    # 利率点评区间统计数据
    raw_vals1 = countCounterCommentBySegment()
    # 格式转换
    vals1 = transform_seg(raw_vals1) 
    # 更新缓存 
    cache1 = CounterCommentCache()
    # 需要重置, 则重置点评缓存
    if reload_flag:
        cache1.mdel()
    # 更新点评缓存
    res1 = cache1.mset(vals1)
    # 
    # 【产品明细页】利率点评数统计(可视化条件)
    raw_vals2 = countDistinctCounterComment()
    vals2 = transform_stats(raw_vals2)
    # 更新缓存
    cache2 = CounterCommentStatsCache()
    # 需要重置, 则重置点评缓存
    if reload_flag:
        cache2.mdel()
    # 更新点评缓存
    res2 = cache2.mset(vals2)
    return res1 and res2


def transform_seg(data):
    """
    @summary: 格式转换
    @param data: list, 原始查询结果, 数据格式: 
                                     - (产品Id, 区间标签, 利率均值)
                                     - (byte, byte, float)
    @return: list, 格式化数据, 数据格式: (产品Id, [利率区间统计])
    """
    #
    import json
    from common.api import toString
    # 格式转换+聚合
    vals = {} 
    for da in data:
        pid = toString(da[0])
        idx = int(toString(da[1])[-1])
        cnt = da[-1]
        if pid not in vals:
            vals[pid] = [0 for i in range(5)] 
        vals[pid][idx] = cnt 
    # 
    res = []
    for key in vals:
        res.append(tuple([key, json.dumps(vals[key])]))
    return res


def transform_stats(data):
    """
    @summary: 格式转换
    @param data: list, 原始查询结果, 数据格式:
                                     - (产品Id, 点评数)
                                     - (byte, int)
    @return: list, 格式化数据, 数据格式: (产品Id, 点评数)
    --------------------------------------------
    @note: 2019.02.25  新增接口           完成
           2019.xx.xx 
    """
    #
    from common.api import toString
    # 
    res = []  
    for da in data:
        res.append((toString(da[0]), da[-1]))
    return res
