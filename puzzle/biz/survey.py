# -*- coding: utf-8 -*-

"""
问卷点评

TODO: 1. 问卷对象 
      2. 问卷题库
      3. 问卷点评统计服务

REMARK:  
"""

import os
import json
#
from common.api import BaseComponent
from common.api import find_value 


def updateSurveyCommentStats(params={}):
    """
    @summary: 更新投票点评
              统计数据
    @param params: dict, 配置参数 
    @return:  
    --------------------------------------------
    @note: 2019.02.11  新增接口        完成
           2019.02.17  加入配置参数    完成
                       - Reload: bool, 是否重置更新, 默认false
           2019.02.25  新增            完成
           2019.xx.xx
    """
    from ..db.stats import countSurveyComment
    from ..db.stats import countDistinctSurveyComment
    from ..db.cache.survey import SurveyCommentCache
    from ..db.cache.survey import SurveyCommentStatsCache
    #
    from common.api import find_value
    #
    num = find_value(params, ['Num'], 6)
    reload_flag = find_value(params, ['Reload'], False)
    #
    # 选项统计
    raw_vals1 = countSurveyComment()
    # 数据处理(格式转换+选项映射标签) 
    vals1 = process(raw_vals1, num)
    # 更新缓存
    cache1 = SurveyCommentCache()
    # 需要重置, 则重置点评缓存
    if reload_flag:
        cache1.mdel()
    # 更新点评缓存
    res1 = cache1.mset(vals1)
    #
    # 点评统计 
    raw_vals2 = countDistinctSurveyComment()
    # 数据处理(格式转换+选项映射标签)
    vals2 = transform_stats(raw_vals2)
    # 更新缓存
    cache2 = SurveyCommentStatsCache()
    # 需要重置, 则重置点评缓存
    if reload_flag:
        cache2.mdel()
    # 更新点评缓存
    res2 = cache2.mset(vals2)
    return res1 and res2


def process(data, num):
    """
    @summary: 数据处理 
    @param data: list, 原始查询结果, 数据格式: 
                                     - (产品Id, 问卷Id, 问题Id, 选项, 统计)
                                     - (byte, byte, byte, byte, int)
    @param num: int, 前多少条
    @return: list, 格式化数据, 数据格式: (问卷Id, 产品Id, [(标签a,统计),(标签b,统计)])
    """
    #
    import json
    from common.config import survey_file
    from common.util.word import toString
    #
    sids = set() 
    for da in data:
        sids.add(toString(da[1]))
    bank = SurveyBank({'DataFile': survey_file})
    for sid in sids:
        obj = bank.querySurvey(sid, to_dict=False)
        a = obj.getTagMap()
    rules = {sid: bank.querySurvey(sid, to_dict=False).getTagMap() for sid in sids}
    #    
    vals = {}
    for da in data:
        pid = toString(da[0])
        sid = toString(da[1])
        tid = toString(da[2])
        opt = toString(da[3])
        cnt = int(da[-1])
        key = '{}#{}'.format(pid, sid)
        if key not in vals:
            vals[key] = []
        # 标签映射
        tag = (rules[sid][tid][opt] if opt in rules[sid][tid] else None)
        if tag is not None:
            vals[key].append((tag,cnt))
    for key in vals:
        vals[key] = sorted(vals[key], key=lambda l:l[1], reverse=True)[:num] 
    res = []
    for key in vals:
        res.append(tuple(key.split('#')+[json.dumps(vals[key])]))
    return res


def transform_stats(data):
    """
    @summary: 格式转换
    @param data: list, 原始查询结果, 数据格式:
                                     - (产品Id, 问卷Id, 点评数)
                                     - (byte, byte, int)
    @return: list, 格式化数据, 数据格式: (产品Id, 点评数)
    --------------------------------------------
    @note: 2019.02.25  新增接口           完成
           2019.xx.xx
    """
    #
    from common.util.word import toString
    #
    res = []
    for da in data:
        pid = toString(da[0])
        sid = toString(da[1])
        res.append((pid, sid, da[-1]))
    return res


class Survey(BaseComponent):
    """
    @summary: 问卷对象
    """
    __id = ''      # 问卷id
    __topics = []  # 题目列表
    
    def __init__(self, params={}):
        """
        @summary: 初始化
        @param params: dict类型, 配置参数
        @return: 
        """
        super(Survey, self).__init__(params)
        # 参数解析 
        sid = self._load_param(params, 'SurveyId', '')
        data = self._load_param(params, 'Data', [])
        #
        objs = [{} for i in range(len(data))]
        for da in data:
            idx = find_value(da, ['Rank'])
            tid   = find_value(da, ['Id'])
            topic = find_value(da, ['Topic'])
            tips  = find_value(da, ['Tips'])
            tags  = find_value(da, ['Tags'])
            #
            objs[idx] = {'Id': tid, 'Topic': topic, 'Tips': tips, 'Tags': tags} 
        #
        self.__id = sid
        self.__topics = objs
        pass

    def getSurveyId(self):
        """
        @summary: 获取问卷ID 
        @return: str, 返回问卷ID
        """
        return self.__id

    def getTopics(self):
        """
        @summary: 获取题目
        @return: list, 返回题目列表
        """
        return self.__topics

    def getTagMap(self):
        """
        @summary: 获取标签映射
        @return: dict, 映射规则, 格式: {问题Id: {选项:标签}} 
        """
        res = {}
        for obj in self.__topics:
            res[obj['Id']] = obj['Tags']
        return res

    def toDict(self):
        """
        @summary: 转为dict
        @return: dict, 返回结果
        """
        res = {'SurveyId': self.__id, 
               'Topics': self.__topics}
        return res


class SurveyBank(BaseComponent):
    """
    @summary: 问卷题库
    """
    __bank = []  # 问卷列表 

    def __init__(self, params={}):
        """
        @summary: 初始化 
        @param params: dict类型, 配置参数
        @return: 
        """
        super(SurveyBank, self).__init__(params)
        # 参数解析
        data_file = self._load_param(params, 'DataFile', '')
        # 载入数据
        data = []
        if os.path.isfile(data_file):
            with open(data_file, 'r') as f:
                data = json.load(f)
        #
        objs = []
        for da in data:
            obj_params = {}
            obj_params['SurveyId'] = find_value(da, ['SurveyId'], '')
            obj_params['Data'] = find_value(da, ['List'], [])
            obj = Survey(obj_params)
            objs.append(obj)
        #
        self.__bank = objs 
        pass

    def querySurvey(self, sid='', to_dict=True):
        """
        @summary: 获取问卷 
        @param sid: str, 问卷ID
        @return: dict, 成功返回问卷,
                       失败返回None
        """
        res = None
        for obj in self.__bank:
            survey_id = obj.getSurveyId() 
            if survey_id == sid:
                res = obj
                if to_dict:
                    res = res.toDict()
                break
        return res
