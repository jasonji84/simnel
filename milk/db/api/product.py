# -*- coding: utf-8 -*-

"""
产品相关
数据访问接口

TODO: 1. 根据名称查询产品id

REMARK: 
"""

def queryProductId(name):
    """
    @summary: 根据名称
              获取产品id
    @param name: str, 产品名称 
    @return: str, 返回产品ID, 否则为''
    --------------------------------------------
    @note: 2019.02.19   新增           完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    from common.util.word import toString
    #
    sql = """
          select a.ProductId
          from eo_product_base as a
          join op_product_flag as b 
          on a.ProductId=b.ProductId
          where a.Name = '{}' 
                and a.Disabled = 0 
                and b.Disabled = 0
                and b.OpenState = 1 
                and b.CounterFlag = 1
          order by a.ProductId
          """.format(name)
    vals = MysqlDB().execute(sql)
    res = (toString(vals[0][0]) if vals else '')
    return res


def queryProductByName(name):
    """
    @summary: 根据名称
              获取产品信息
    @param name: str, 产品名称
    @return: list, 查询结果, 数据格式:
                             - ProductId: 产品Id 
                             - Name: 产品名称
                             - Icon: 产品图标 
    --------------------------------------------
    @note: 2019.04.17   新增           完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    from common.util.word import toString
    #
    sql = """
          select a.ProductId
                ,a.Name
                ,a.Icon
          from eo_product_base as a
          join op_product_flag as b
          on a.ProductId=b.ProductId
          where a.Name = '{}'
                and a.Disabled = 0
                and b.Disabled = 0
                and b.OpenState = 1
          """.format(name)
    vals = MysqlDB().execute(sql)
    #
    fields = ['ProductId','Name','Icon']
    res = []
    if vals:
        for val in vals:
            res.append({k:toString(v) for (k,v) in zip(fields, val)})
    return res

    
    
    return res
