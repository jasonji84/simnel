# coding=utf-8

"""
用户相关操作服务

TODO: 1. 用户登录服务
      2. 校验会话服务

REMARK: 1. 退出服务暂定为行为
"""

from . import *

user_bp = Blueprint('user_blueprint', url_prefix='/user')

@user_bp.route("/login", methods=["GET"])
async def loginHandler(request):
    """
    @summary: 登陆服务
              用户首次登陆或者无会话key
    @param request: Request, 请求对象
    @return: 200, 运行成功, 本地会话key
             404, 运行异常
    -------------------------------------------------------
    @note: 2019.01.05  用户登录流程集成                完成
           2019.01.20  校验失败返回200, 但是无Session  完成
           2019.01.29  增加用户行为记录                完成
           2019.xx.xx
    """
    #
    # 微信登录服务
    from ..biz.user import wechatLogin
    # 添加数据库
    from ..db.api import addLogin
    #
    # 参数解析
    # 临时登录凭证code
    code = request.args.get('Code')
    #  
    res = {}
    try:
        # 登录操作
        # 成功返回本地会话key, 失败返回None
        vals = wechatLogin(code)
        if vals is not None:
            # 用户行为添加
            addLogin(vals[-1], 'login')
            # 
            res = {'Session': vals[0]}
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@user_bp.route("/validate", methods=["GET"])
async def validateSessionHandler(request):
    """
    @summary: 校验会话
              是否有效服务
    @param request: Request, 请求对象
    @return: 200, 运行成功, 有效True/无效False
             404, 运行异常
    ---------------------------------------
    @note: 2019.01.05  用户登录流程集成     完成
           2019.01.29  增加用户行为记录     完成
           2019.xx.xx
    """
    # 校验会话
    from ..biz.user import checkSession 
    # 添加数据库
    from ..db.api import addLogin
    #
    # 参数解析
    # 本地会话key
    session_key = request.args.get('Session')
    #
    flag = False
    try:
        # 检查会话
        # 获取微信openid
        oid = checkSession(session_key)
        if oid is not None:
            flag = True
            # 用户登录记录
            addLogin(oid, action='login')
    except Exception as e:
        print(e)
        abort(404)
    return response.json({'Valid': flag}, status=200)
