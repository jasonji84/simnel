# -*- coding: utf-8 -*-

"""
点评相关服务

TODO: 1. 利率点评是否呈现
      2. 问卷点评是否呈现

REMARK: 
"""

def isCounterCommentReady(pid):
    """
    @summary: 利率点评
              是否满足展示条件 
    @param pid: str, 网贷产品id
    @return: bool, 成功/失败
    -------------------------------------------
    @note: 2019.02.25  新增      完成
           2019.xx.xx  
    """
    from ..db.api import queryCounterCommentCacheDistinctStats
    #
    res = 0
    stats = queryCounterCommentCacheDistinctStats(pid)
    if stats and isinstance(stats, int):
        res = stats
    return (res >= 5)


def isSurveyCommentReady(pid, sid):
    """
    @summary: 问卷点评是否成立
    @param pid: str, 网贷产品id
    @param sid: str, 问卷id
    @return: bool, 成功/失败
    -------------------------------------------
    @note: 2019.02.25  新增      完成
           2019.xx.xx
    """
    from ..db.api import querySurveyCommentCacheDistinctStats
    #
    res = 0
    stats = querySurveyCommentCacheDistinctStats(pid, sid)
    if stats and isinstance(stats, int):
        res = stats
    return (res >= 5)

