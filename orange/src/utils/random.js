//

// 随机序列
export function randomSeries (size = 128) {
  var res = ''
  while (res.length < size) {
    res += Math.random().toString(36).substr(2)
  }
  return res.substr(0, size)
}
