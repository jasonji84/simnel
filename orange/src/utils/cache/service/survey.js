// 题库缓存
//
// 远程链接
import {getSurveyBankUrl as bankUrl} from '@/utils/comm/config'
// 远程通信
import {request} from '@/utils/comm/fly'
// 时间函数
import {getCurrentTime, calTimeSpan} from '@/utils/clock'
// 本地缓存
import {surveyBankLocalStorage as bankKey} from '@/utils/cache/config'
import {setStorage, getStorageSync} from '@/utils/cache/storage'

// 限定条件：1天
var threshold = 24
var unit = 'hr'
// 数据仓库
var databox = {}
// 后处理回调函数
var postCallback = null

export function getSurvey (force = false, callback = null) {
  // 获取问卷
  postCallback = callback
  // console.log('typeof postCallback: ', (typeof postCallback === 'function'))
  // 当前标签
  // currentTag = tag
  // console.log('[getRanking] check(tag): ', check(tag))
  // console.log('[getRanking] databox:    ', databox)
  // 1. 强制刷新; 2. 数据没有缓存或数据过期
  if (force === true || !check()) {
    requestData()
  } else {
    // 否则，载入缓存数据
    typeof postCallback === 'function' && postCallback(databox.data)
  }
}

function check () {
  // 有效性判定
  var res = false
  // 数据是否初始化
  if (Object.keys(databox).length === 0) {
    databox = getStorageSync(bankKey, {})
  }
  if (typeof databox.time !== 'undefined') {
    // 条件2: 是否过期
    res = isExpired(databox.time)
  }
  return res
}

function isExpired (timestamp) {
  // 判定是否过期
  var res = true
  if (timestamp instanceof Date) {
    let currentTime = getCurrentTime()
    let val = calTimeSpan(timestamp, currentTime, unit)
    res = (val >= threshold)
  }
  return res
}

function requestData () {
  // console.log('[survey][requestData] ')
  // 远程获取数据
  request(bankUrl, 'get', {},
    function success (res) {
      // 成功获取数据后处理
      // 备注：状态为200，通信已校验
      try {
        // 解析数据
        let survey = res.data.Result.Survey
        if (typeof survey !== 'undefined') {
          databox = {time: getCurrentTime(), data: transform(survey)}
          setStorage(bankKey, databox)
          typeof postCallback === 'function' && postCallback(databox.data)
        }
      } catch (e) {
        console.warn('[survey][requestData] e: ', e)
      }
    }, null, false)
}

function transform (survey) {
  let res = {}
  // 问卷Id
  res.id = survey.SurveyId
  // 问卷内容
  var arr = Array(survey.List.length)
  for (var i = 0; i < survey.List.length; i++) {
    let obj = survey.List[i]
    let topic = {title: obj.Topic.Title, options: obj.Topic.Options, quit: obj.Topic.Exception}
    let tips = {title: obj.Tips.Title, content: obj.Tips.Content, affirm: obj.Tips.Affirm}
    let tags = obj.Tags
    arr[obj.Rank] = {id: obj.Id, topic: topic, tips: tips, tags: tags}
  }
  res.bank = arr
  return res
}
