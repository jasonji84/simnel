# -*- coding: utf-8 -*-

"""
定制化线上服务
数据库服务

TODO: 1. 产品搜索;
      2. 产品明细;
      3. 热词榜单;

REMARK: 构造多表查询
"""

def queryProductWithRank(content, pinyin, num=20):
    """
    @summary: 搜索产品
              模糊搜索 + 结果带Rank
    @param content: str, 查询内容
    @param pinyin: str, 相应拼音
    @param num: int, 输出多少个结果
    @return: list, 查询结果, 数据格式:
                             - 字段: (产品Id, 产品名称, 产品图标, 交互数, 排位)
                             - 类型: (byte, byte, byte, int, float) 
    ----------------------------------------------------
    @note: 2019.04.09    新增     完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select t.ProductId
                ,t.Name
                ,t.Icon
                ,t.TotalComment
                ,@curRank := @curRank + 1 as rank
          from (select a.ProductId
                      ,a.Name
                      ,a.Icon
                      ,b.RankFlag
                      ,(case when c.TotalUnionComment is null then 0 else c.TotalUnionComment end) as TotalComment
                      ,(case
                        when a.Name = '{}' then 10
                        when a.Name like '{}%' then 8
                        when a.Name like '%{}' then 6
                        when a.Name like '%{}%' then 7
                        when a.Pinyin = '{}' then 5
                        when a.Pinyin like '{}%' then 4
                        when a.Pinyin like '%{}' then 2
                        when a.Pinyin like '%{}%' then 3
                        else 0
                        end) as Score
                from eo_product_base as a
                join op_product_flag as b
                on a.ProductId = b.ProductId
                   and b.Disabled = 0
                   and b.OpenState = 1 and b.SearchFlag = 1
                left join da_product_stats as c
                on a.ProductId = c.ProductId
                   and c.Latest = 1
                   and c.Disabled = 0
                where a.Disabled = 0
                      and (a.Name like '%{}%' or a.Pinyin like '%{}%')) as t, (select @curRank := 0) as r
          order by t.Score desc,t.TotalComment desc,t.RankFlag desc
          limit {}
          """.format(content, content, content, content, pinyin, pinyin, pinyin, pinyin, content, pinyin, num)
    #
    #print(sql)
    res = MysqlDB().execute(sql)
    return res


def queryProductDetail(pid):
    """
    @summary: 查询网贷产品
              明细数据
    @param tag: str, 榜单标签
    @return: list, 查询结果, 数据格式:
                             - 字段: (产品Id, 产品名称, 产品图标, 所属公司, 注册链接, 利率权限, 问卷权限, 投票权限, 注册权限)
                             - 类型: (byte, byte, byte, byte, byte, int, int, int, int)
    ---------------------------------------------
    @note: 2019.01.05   产品状态开放      完成
           2019.01.17   代码升级          完成
           2019.03.16   调用接口固化      完成
                        sql模版化         
                        增加注释
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select a.ProductId
                ,a.Name
                ,a.Icon
                ,a.Company
                ,a.LoginLink
                ,b.CounterFlag
                ,b.SurveyFlag
                ,b.VoteFlag
                ,b.LoginFlag
          from eo_product_base as a
          join op_product_flag as b
          on a.ProductId=b.ProductId
          where a.Disabled = 0
                and b.Disabled = 0
                and a.ProductId = '{}'
                and b.OpenState = 1
          """.format(pid)
    #print(sql)
    res = MysqlDB().execute(sql)
    return res


def queryKeywordRank():
    """
    @summary: 获取最新
    @return: list, 查询结果, 数据格式:
                             - 字段: (排名, 热搜词)
                             - 类型: (int, byte)
    ----------------------------------------
    @note: 2019.01.05  无限定条件      完成
           2019.03.16  SQL模版化       完成
                       新增注释
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select a.Rank
                ,a.Keyword
          from op_keyword_rank as a
          where a.Disabled = 0
                and a.Latest = 1
          """
    res = MysqlDB().execute(sql)
    return res
