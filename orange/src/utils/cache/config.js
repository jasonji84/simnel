// 存储配置

// 日志Key
export const logKeyForLocalStorage = 'Log'
// 用户
// 登录会话key
export const sessionKeyLocalStorage = 'Session'
// 搜索
// 搜索榜单
export const productRankLocalStorage = 'ProductRank'
// 热词榜单
export const keywordRankLocalStorage = 'KeywordRank'
// 点评
// 问卷题目
export const surveyBankLocalStorage = 'SurveyBank'
// 利率点评 - 产品信息
export const counterProductLocalStorage = 'CounterProduct'
