// 通信API

import {sessionKeyLocalStorage as sessionKey} from '@/utils/cache/config'
import {getStorageSync} from '@/utils/cache/storage'
import {doRequest} from './weRequest'

// 通信不带会话
export function request (config) {
  // console.log('[request] config: ', config)
  doRequest(config)
}

// 通信带会话
export function requestWithSession (config, posts) {
  config.data['Session'] = getStorageSync(sessionKey)
  config.data['Data'] = posts
  doRequest(config)
}

export function postWithSession (url, posts, success, fail, complete) {
  // 通信配置
  let config = {
    url: url,
    method: 'POST',
    header: {
      'content-type': 'application/json'
    },
    dataType: 'json',
    data: {},
    success: function (res) {
      typeof success === 'function' && success(res)
    },
    fail: function (res) {
      typeof fail === 'function' && fail(res)
    },
    complete: function (res) {
      typeof complete === 'function' && complete(res)
    }
  }
  // 打包数据
  config.data['Session'] = getStorageSync(sessionKey)
  config.data['Data'] = posts
  doRequest(config)
}

export function getWithNoSession (url, args, success, fail, complete) {
  // 通信配置
  let config = {
    url: url,
    method: 'GET',
    header: {
      'content-type': 'application/json'
    },
    dataType: 'json',
    data: {},
    success: function (res) {
      typeof success === 'function' && success(res)
    },
    fail: function (res) {
      typeof fail === 'function' && fail(res)
    },
    complete: function (res) {
      typeof complete === 'function' && complete(res)
    }
  }
  // 打包数据
  config.data = args
  doRequest(config)
}
