# -*- coding: utf-8 -*-

"""
投票点评
数据服务

TODO: 1. 投票点评数据服务;

REMARK: 
"""

def updateVoteCommentStats(params={}):
    """
    @summary: 更新投票点评
              统计数据
    @param params: dict, 配置参数
    @return:  
    ------------------------------------------------
    @note: 2019.02.11  新增接口         完成
           2019.02.17  加入配置参数     完成
                       - Reload: bool, 是否重置更新, 默认false
           2019.03.09  新增假数据       未完成
           2019.xx.xx
    """
    from ..db.stats import countVoteComment
    from ..db.cache.vote import VoteCommentCache
    #
    from common.api import find_value
    #
    reload_flag = find_value(params, ['Reload'], False)
    #
    # 统计投票
    raw_vals = countVoteComment()
    # 格式转换
    vals = transform(raw_vals) 
    # 更新缓存 
    cache = VoteCommentCache()
    # 需要重置, 则重置点评缓存
    if reload_flag:
        cache.mdel()
    # 更新点评缓存
    res = cache.mset(vals)
    return res


def transform(data):
    """
    @summary: 格式转换
    @param data: list, 原始查询结果, 数据格式: 
                                     - (产品Id, 投票Id, 问题Id, 答案, 统计)
                                     - (byte, byte, byte, byte, int)
    @return: list, 格式化数据, 数据格式: (产品Id, 投票Id, 问题Id, {答案a: 统计, 答案b: 统计})
    """
    #
    import json
    from common.api import toString
    # 格式转换+聚合
    vals = {} 
    for da in data:
        pid = toString(da[0])
        vid = toString(da[1])
        tid = toString(da[2])
        opt = toString(da[3])
        cnt = int(da[-1])
        key = '{}#{}'.format(pid,vid)  
        if key not in vals:
            vals[key] = {}
        if tid not in vals[key]:
            vals[key][tid] = {}
        vals[key][tid][opt] = cnt
    # 
    res = []
    for key in vals:
        res.append(tuple(key.split('#')+[json.dumps([{'TopicId': tid, 'Option': vals[key][tid]} for tid in vals[key]])]))
    return res
