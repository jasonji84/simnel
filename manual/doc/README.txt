脚本目录结构

cold: 冷启动数据运维
- product.py: 产品基础信息和权限管理
- rank.py: 首页两个榜单管理
- keyword.py: 热搜管理
- data.xlsx: 运维数据文件
- offline.py: 数据库支持(软连接mysql的数据库模版)


mysql: 数据库运维
- draft.txt: 创建表单sql 
- cold_db.sql: 含冷启动数据的数据库备份（最初）
- template: 数据库模版目录, 生成sql使用
  - rank.py: 首页榜单统计
  - counter.py: 利率点评统计
  - vote.py: 投票点评统计
  - online.py: 其它在线数据库操作
  - offline.py: 离线数据库操作


test: 测试
- comment.py: 点评测试
- rank.py: 首页榜单测试


script: 运维脚本
- clean.sh: 清空临时文件脚本	
- mongo.sh: mongodb脚本	
- mysql.sh: mysql脚本
- redis.sh: redis脚本
