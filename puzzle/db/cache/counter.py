# -*- coding: utf-8 -*-

"""
利率点评
缓存数据服务

TODO: 1. 利率点评缓存数据服务;
      2. 利率点评统计缓存数据服务;

REMARK: 
"""

from common.api import BaseComponent
from common.api import CacheService


class CounterCommentCache(BaseComponent):
    """
    @summary: 利率点评缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.16  新增     完成
           2019.xx.xx
    """
    # key: 业务##产品ID##
    # value: [count1, count2, count3, count4, count5]
    _head = 'Counter'

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(CounterCommentCache, self).__init__(params)
        #
        self.cs = CacheService()

    def query(self, pid):
        """
        @summary: 查询记录
        @param pid: str, 产品id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##'.format(self._head, pid))

    def has(self, pid):
        """
        @summary: 记录是否存在
        @param pid: str, 产品id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##'.format(self._head, pid)) is not None

    def set(self, pid, data):
        """
        @summary: 新增记录
        @param pid: str, 产品id 
        @param data: dict, 数据对象
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return self.cs.set('{}##{}##'.format(self._head, pid), data)

    def mset(self, data):
        """
        @summary: 批量导入数据
        @param data: list, 数据集合, 格式: (sid, values) 
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        kvs = {'{}##{}##'.format(self._head, da[0]):da[1] for da in data}
        return self.cs.mset(kvs)

    def remove(self, pid):
        """
        @summary: 删除数据
        @param pid: str, 产品id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##{}##'.format(self._head, pid))

    def mdel(self):
        """
        @summary: 批量删除数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##*'.format(self._head))


class CounterCommentStatsCache(BaseComponent):
    """
    @summary: 利率点评统计缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    # 格式: CounterStats##产品ID##
    # 数据: 有效点评数
    _head = 'CounterStats'

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(CounterCommentStatsCache, self).__init__(params)
        #
        self.cs = CacheService()
    
    def query(self, pid):
        """
        @summary: 查询记录
        @param pid: str, 产品id
        @return: int, 成功返回数量
                      失败返回None
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##'.format(self._head, pid))

    def has(self, pid):
        """
        @summary: 记录是否存在
        @param pid: str, 产品id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##'.format(self._head, pid)) is not None

    def set(self, pid, data):
        """
        @summary: 新增记录
        @param pid: str, 产品id
        @param data: int, 数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return self.cs.set('{}##{}##'.format(self._head, pid), data)

    def mset(self, data):
        """
        @summary: 批量导入数据
        @param data: list, 数据集合, 格式: (pid, data) 
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        kvs = {'{}##{}##'.format(self._head, da[0]):da[1] for da in data}
        return self.cs.mset(kvs)

    def remove(self, pid):
        """
        @summary: 删除数据
        @param pid: str, 产品id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##{}##'.format(self._head, pid))

    def mdel(self):
        """
        @summary: 批量删除数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##*'.format(self._head))
