# -*- coding: utf-8 -*-

"""
定制化线上服务
数据库服务

TODO: 1. 产品搜索;
      2. 热词榜单;

REMARK: 构造多表查询
"""

def queryProduct(content):
    """
    @summary: 产品搜索
    @param content: 查询内容
    @return: list, 查询结果
    ----------------------------------------------------
    @note: 2019.01.05    产品在架 + 搜索权限      完成
           2019.03.16    sql模版                  完成 
                         产品统计表单升级             
           2019.xx.xx
    """
    from milk.db.service import ProductBaseDB
    from milk.db.service import ProductFlagDB
    from milk.db.service import ProductStatsDB
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #    
    db1 = ProductBaseDB()
    db2 = ProductFlagDB()
    db3 = ProductStatsDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    table3 = db3.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in ['ProductId','Name','Icon','Pinyin']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenState','SearchFlag']]
    fields3 = [db3.find_field(fd) for fd in ['ProductId','TotalUnion']]
    #
    sql = """
          select {}
                ,CASE WHEN c.{} is null THEN 0 ELSE c.{} END
          from {} as a
          join {} as b
          on a.ProductId = b.ProductId 
             and b.Disabled = 0
             {}
          left join {} as c
          on a.ProductId = c.ProductId 
             and c.Latest = 1 
             and c.Disabled = 0
          where a.Disabled = 0  
                and (a.{} like '%{}%' or a.{} like '{}%')
          order by c.{} desc
          """.format(addSelectedField(fields1[:3], 'a'),
                     db3.find_field('TotalUnion'),
                     db3.find_field('TotalUnion'),
                     table1,
                     table2,
                     ''.join([addCondition(1, 'b.{}'.format(fd), 1) for fd in fields2[1:]]),
                     table3,
                     fields1[1],
                     content,
                     fields1[-1],
                     content,
                     fields3[-1])
    print(sql) 
    res = db1.execute(sql)
    return res


def queryKeywordRank():
    """
    @summary: 获取最新
    @return: list, 查询结果
    ----------------------------------------
    @note: 2019.01.05  无限定条件      完成
           2019.xx.xx
    """
    from milk.db.service import KeywordRankDB
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addLatestCondition
    #
    db = KeywordRankDB()
    #
    table = db.get_table()
    fields = [db.find_field(fd) for fd in ['Rank','Keyword']]
    #
    sql = """
          select {}
          from {} as a
          where a.Disabled = 0
                and a.Latest = 1
          """.format(addSelectedField(fields, 'a'),
                     table)
    # print(sql)
    res = db.execute(sql)
    return res


def queryProductDetail(pid, fields=[['ProductId','Name','Icon','Company','LoginLink'],
                                    ['CounterFlag','SurveyFlag','VoteFlag','LoginFlag']]):
    """
    @summary: 查询网贷产品
              明细数据
    @param tag: str, 榜单标签
    @return: list, 查询结果
    ---------------------------------------------
    @note: 2019.01.05   产品状态开放      完成
           2019.01.17   代码升级          完成
           2019.xx.xx
    """
    #
    from milk.db.service import ProductBaseDB
    from milk.db.service import ProductFlagDB
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    # 数据库初始化
    db1 = ProductBaseDB()
    db2 = ProductFlagDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in fields[0]]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenState'] + fields[1]]
    #
    sql = """
          select {}
                ,{}
          from {} as a
          join {} as b
          on {}
          where a.Disabled = 0
                and b.Disabled = 0
                {}
                {}
          """.format(addSelectedField(fields1, 'a'),
                     addSelectedField(fields2[2:], 'b'),
                     table1,
                     table2,
                     addEqualCondition(fields1[:1], 'a', fields2[:1], 'b'),
                     addCondition(pid, 'a.{}'.format(fields1[0]), pid),
                     addCondition(1,   'b.{}'.format(fields2[1]),  1))
    res = db1.execute(sql)
    return res


if __name__ == "__main__":

    from milk.db.api import queryProductDetail
    #r = queryProductDetail('0a9cde-88d604')
    #print(r)
    #print(queryKeywordRank())
    print(queryProduct('360'))
    #print(queryProductDetail('0a9cde-88d604'))
    #from puzzle.db import countVoteComment
    #for r in countVoteComment():
    #    print(r)
