# coding=utf-8

"""
@summary: 产品相关接口
"""
from .product import queryProductId
# 根据名字查询网贷产品信息
from .product import queryProductByName

"""
@summary: 搜索相关接口
"""
# 网贷产品搜索
from .search import queryProductWithRank
# 获取产品明细
from .search import queryProductDetail
# 获取最新产品榜单
from .search import getCommentRank   
# 获取最新热词榜单
from .search import getKeywordRank

"""
@summary: 点评相关接口
"""
#
# 添加利率点评
from .comment import addCounterComment
# 查看利率点评分段统计
from .comment import queryCounterCommentCacheSegmentStats
# 查看利率点评有效数量
from .comment import queryCounterCommentCacheDistinctStats
#
# 添加问卷点评
from .comment import addSurveyComment
# 查看问卷点评标签统计
from .comment import querySurveyCommentCacheTagStats 
# 查看问卷点评有效数统计
from .comment import querySurveyCommentCacheDistinctStats
#
# 添加投票点评
from .comment import addVoteComment
# 查看投票点评投票统计
from .comment import queryVoteCommentCachePollStats  
# 
# 实时更新投票点评
from .comment import updateSingleVoteCommentCache

"""
@summary: 会话缓存服务接口
"""
# 查询会话缓存
from .session import querySessionCache
# 添加会话缓存
from .session import addSessionCache
# 删除会话缓存
from .session import delSessionCache
# 清空会话缓存
from .session import cleanSessionCache
# 查询用户SessionKey
from .session import querySessionKey

"""
@summary: 用户行为接口
"""
# 用户登录行为
from .action import addLogin
# 用户访问页面行为
from .action import addAccess
# 用户搜索行为
from .action import addSearch
# 用户提交自定义产品名称行为
from .action import addUploadProduct
# 用户转发行为
from .action import addShare
# 用户点击转发行为
from .action import addHitShare
