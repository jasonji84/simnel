// 时钟模块（时间函数组件）

export function getCurrentTime () {
  // 获取当前时间
  return new Date().getTime()
}

export function calTimeSpan (startTime, endTime, unit = 'hr') {
  // 计算时间间隔
  var res = null
  try {
    let times = {ms: 1, sec: 1000, min: 60000, hr: 3600000, day: 216000000}
    if (unit in times) {
      res = (endTime - startTime) / times[unit]
    }
  } catch (e) {
    console.warn('[calTimeSpan] fail  unit: ', unit)
  }
  return res
}
