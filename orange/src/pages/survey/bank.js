// 用户选项
var options = null

// 题库
var survey = {}

// 初始化（获取问卷）
function init (obj) {
  survey = obj
  let n = survey.bank.length
  options = Array(n)
  for (var i = 0; i < n; i++) {
    options[i] = -1
  }
}

function getObject (index) {
  let res = null
  if (isExist(index)) {
    res = survey.bank[index]
  }
  return res
}

function isExist (index) {
  return index < survey.bank.length
}

function getSurveyId () {
  return survey.id
}

function getTopic (index) {
  let res = null
  let obj = getObject(index)
  if (obj !== null) {
    res = obj.topic
  }
  return res
}

function getTips (index) {
  let res = {}
  let obj = getObject(index)
  if (obj !== null) {
    res = obj.tips
  }
  return res
}

function setOption (index, option) {
  if (isExist(index)) {
    options[index] = option
  }
}

function getOption (index) {
  let res = -1
  if (isExist(index)) {
    res = options[index]
  }
  return res
}

function getTopicId (index) {
  let res = ''
  let obj = getObject(index)
  if (obj !== null) {
    res = obj.id
  }
  return res
}

function getTotal () {
  let res = 0
  // console.log('[getTotal] survey.bank: ', survey.bank)
  if (survey.bank.length !== 'undefined') {
    res = survey.bank.length
  }
  return res
}

module.exports = {
  init: init,
  has: isExist,
  getTopic: getTopic,
  getTips: getTips,
  setOption: setOption,
  getOption: getOption,
  getSurveyId: getSurveyId,
  getTopicId: getTopicId,
  getTotal: getTotal
}
