// 微信小程序通信封装

import {baseUrl} from '@/utils/comm/config'

// const loading = require('./loading')

// 服务标签
// var label = ''
// 资源标示后缀（完整URL也可以）
var url = ''
// 完整URL
var fullUrl = ''
// 调用方法
var method = 'GET'
// 头字段定义
var header = {'content-type': 'application/json'}
// 返回数据类型
var dataType = 'json'
// 传输数据
var data = {}
// 接口调用成功的回调函数
var success = function (res) {}
// 接口调用失败的回调函数
var fail = function (res) {}
// 接口调用完成的回调函数
var complete = function (res) {}
// 重复请求次数
// var retryLimit = 3
// 上报过程数据
// var profile = false
// 测试数据模式
// var mockJson = false
// var globalData = false

// 对外调用api
export function doRequest (params) {
  init(params)
  request()
}

// 初始化配置，包括交互协议, 后续操作, 异常处理
function init (params) {
  // console.log('[weRequest][init] called ')
  // 请求配置
  // label = params.label || ''
  url = params.url || ''
  method = params.method || 'GET'
  header = params.header || {'content-type': 'application/json'}
  dataType = params.dateType || 'json'
  data = params.data || {}
  // 回调函数
  success = params.success || false
  fail = params.fail || false
  complete = params.complete || false
  // 其它配置
  // 重复请求
  // retryLimit = params.retryLimit || 3
  // profile = params.profile || false
  // globalData = params.globalData || false
  // 预处理
  fullUrl = url.startsWith('http') ? url : (baseUrl + url)
  // 如果请求是GET，则在URL中自动加上登录态和全局参数
  if (method === 'GET') {
    for (var key in data) {
      if (fullUrl.indexOf('?') >= 0) {
        fullUrl += '&' + key + '=' + data[key]
      } else {
        fullUrl += '?' + key + '=' + data[key]
      }
    }
  }
}

// 执行请求
function request () {
  // 如果有上报字段配置，则记录请求发出前的时间戳
  // let startTime = new Date().getTime()
  // console.log('[request] url: ', url)
  wx.request({
    url: fullUrl,
    data: data,
    method: method,
    header: header || {},
    dataType: dataType || 'json',
    success: function (res) {
      // console.log('[weRequest][request] success res: ', res)
      // 如果有上报字段配置，则记录请求返回后的时间戳，并进行上报
      // if (typeof profile === 'function') {
      //  let endTime = new Date().getTime()
      //  profile(label, startTime, endTime)
      // }
      if (res.statusCode === 200) {
        typeof success === 'function' && success(res)
      } else {
        // 如果失败重新执行通信（有限次）
        // if (retryLimit > 0) {
        //   retryLimit = retryLimit - 1
        //   request()
        // }
      }
    },
    fail: function (res) {
      // console.log('[weRequest][request] fail res:', res)
      // fault.handle(obj, res)
      typeof fail === 'function' && fail(res)
    },
    complete: function (res) {
      // console.log('[weRequest][request] complete res: ', res)
      typeof complete === 'function' && complete(res)
    }
  })
}

// module.exports = {
//   init: init,
//   post: request,
//   push: push
// }

/* showLoading使用
function pack (obj) {
  // 请求配置
  console.log('[comm][init] pack begin')
  // URL定义
  if (typeof obj.url === 'undefined') {
    console.error('[comm][init] url must be set')
    throw '[requestWrapper] url must be set'
  }

  // 其它配置
  // 字段预处理
  typeof obj.beforeSend === 'function' && obj.beforeSend()
  // 请求失效，重复请求
  if (typeof obj.retryCount === 'undefined') {
    obj.retryLimit = 0
  }
  if (typeof obj.count === 'undefined') {
    obj.count = 0
  }
  if (obj.showLoading) {
    loading.show()
    obj.complete = (function (fn) {
      return function () {
        loading.hide()
        typeof fn === 'function' && fn.apply(this, arguments)
      }
    })(obj.complete)
  }
  // 待定
  obj.fields = obj.fields || {}
}
*/
