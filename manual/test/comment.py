# -*- coding: utf-8 -*-

"""
点评测试

TODO: 1. 问卷点评缓存服务(更新+查询) 
      2. 投票点评缓存服务(更新+查询)
      3. 利率计算缓存服务(更新+查询)

REMARK: 
"""

# 问卷点评缓存更新 
from puzzle.biz.survey import updateSurveyCommentStats 
#updateSurveyCommentStats({'Reload': True})

# 问卷点评后端系统查询
from milk.db.api import querySurveyCommentCacheTagStats
from milk.db.api import querySurveyCommentCacheDistinctStats
from milk.biz.comment import isSurveyCommentReady

pid = '8b70ef-00'
sid = 'sc001'
print(querySurveyCommentCacheTagStats(pid, sid))
print(querySurveyCommentCacheDistinctStats(pid, sid))
print(isSurveyCommentReady(pid, sid))

exit()

# 投票点评缓存更新
from puzzle.biz.vote import updateVoteCommentStats
updateVoteCommentStats({'Reload': True})

# 投票点评后端系统查询
from milk.db.api.comment import queryVoteCommentCachePollStats 
pid = 'e01cea-00'
vid = 'v001'
print(queryVoteCommentCachePollStats(pid, vid))

# 利率点评缓存更新
from puzzle.biz.counter import updateCounterCommentStats
updateCounterCommentStats()

# 利率点评后端系统查询
from milk.db.api.comment import queryCounterCommentCacheSegmentStats
from milk.db.api.comment import queryCounterCommentCacheDistinctStats
from milk.biz.comment import isCounterCommentReady
pid = 'e01cea-00'
print(queryCounterCommentCacheSegmentStats(pid))
print(queryCounterCommentCacheDistinctStats(pid))
print(isCounterCommentReady(pid))
