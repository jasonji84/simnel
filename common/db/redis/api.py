# -*- coding: utf-8 -*-

"""
Cache服务

TODO: 1. 初始化redis连接池对象

REMARK: 首个版本使用本地化redis
"""

from . import config
from .rds import RedisAgency

class CacheService(RedisAgency):
    """
    @summary: 缓存服务
    """
    def __init__(self, params={}):
        """
        @summary: 初始化
        @param params: dict, 配置参数
        @return: 
        """
        #
        host = config.DB_HOST
        port = config.DB_PORT
        index = config.DB_INDEX
        password = config.DB_PASSWORD
        super(CacheService, self).__init__({'Host':host,'Port':port,'Password':password,'DbIndex':index})
