# -*- coding: utf-8 -*-

"""
问卷点评
缓存数据服务

TODO: 1. 问卷点评缓存数据服务;
      2. 问卷点评统计缓存数据服务;

REMARK: 
"""

from common.api import BaseComponent
from common.api import CacheService

class SurveyCommentCache(BaseComponent):
    """
    @summary: 问卷缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.15  新增     完成
           2019.xx.xx
    """
    # key: 业务##问卷ID##产品ID##
    # value: [{'Tag': xxx, 'Count': xx}, ]
    _head = 'Survey'

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(SurveyCommentCache, self).__init__(params)
        #
        self.cs = CacheService()

    def query(self, pid, sid):
        """
        @summary: 查询记录
        @param pid: str, 产品id
        @param sid: str, 问卷id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.15  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##{}##'.format(self._head, sid, pid))

    def has(self, pid, sid):
        """
        @summary: 记录是否存在
        @param pid: str, 产品id
        @param sid: str, 问卷id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##{}##'.format(self._head, sid, pid)) is not None

    def set(self, pid, sid, data):
        """
        @summary: 新增记录
        @param pid: str, 产品id 
        @param sid: str, 问卷id
        @param data: dict, 数据对象
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.15  新增接口     完成
               2019.xx.xx
        """
        return self.cs.set('{}##{}##{}##'.format(self._head, sid, pid), data)

    def mset(self, data):
        """
        @summary: 批量导入数据
        @param data: list, 数据集合, 格式: (pid, sid, data)   注意 ！！！！！！ 
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.15  新增接口     完成
               2019.xx.xx
        """
        kvs = {'{}##{}##{}##'.format(self._head, da[1], da[0]):da[2] for da in data}
        return self.cs.mset(kvs)

    def remove(self, pid, sid):
        """
        @summary: 删除数据
        @param pid: str, 产品id
        @param sid: str, 问卷id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.15  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##{}##{}##'.format(self._head, sid, pid))

    def mdel(self):
        """
        @summary: 批量删除数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.15  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##*'.format(self._head))


class SurveyCommentStatsCache(SurveyCommentCache):
    """
    @summary: 问卷点评统计缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    # key: 业务##产品ID##
    # value: 有效点评数
    _head = 'SurveyStats'

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(SurveyCommentStatsCache, self).__init__(params)
