// 页面提示封装

var isHintShown = false

// 加载提示
export function showLoading (title = '加载中', callback = null) {
  // console.log('[showLoading] isHintShown ', isHintShown)
  if (isHintShown) {
    // 轮询等待
    setTimeout(function () {
      showLoading(title, callback)
    }, 300)
  } else {
    // 加载提示，操作完成关闭
    isHintShown = true
    // console.log('[showLoading] before wx.showLoading ', isHintShown)
    wx.showLoading({
      title: title,
      mask: true,
      complete: function () {
        typeof callback === 'function' && callback()
        wx.hideLoading()
        isHintShown = false
        // console.log('[showLoading] complete ', isHintShown)
      }
    })
  }
}

// 显示提交成功
export function showSuccess (title = '提交成功', callback = null) {
  if (isHintShown) {
    // 轮询等待
    setTimeout(function () {
      showSuccess(title, callback)
    }, 500)
  } else {
    // 加载提示，操作完成关闭
    isHintShown = true
    wx.showToast({
      title: title,
      icon: 'success',
      mask: true,
      duration: 750,
      complete: function () {
        typeof callback === 'function' && callback()
        isHintShown = false
      }
    })
  }
}

// 显示提交失败
export function showAlarm (title = '提交失败', callback = null) {
  if (isHintShown) {
    // 轮询等待
    setTimeout(function () {
      showSuccess(title, callback)
    }, 500)
  } else {
    // 加载提示，操作完成关闭
    isHintShown = true
    wx.showToast({
      title: title,
      icon: 'none',
      mask: true,
      duration: 2000,
      complete: function () {
        typeof callback === 'function' && callback()
        isHintShown = false
      }
    })
  }
}

export function showAlert (title = '错误提示', content = '服务器异常, 努力修复中', callback = null) {
  if (isHintShown) {
    wx.hideToast(null)
    wx.hideLoading()
    isHintShown = false
  }
  // 显示警示页面
  wx.showModal({
    title: title,
    content: content,
    showCancel: false,
    success: function (res) {
      if (res.confirm) {
        typeof callback === 'function' && callback()
      }
    }
  })
}
