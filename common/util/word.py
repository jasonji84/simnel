#coding=utf-8

"""
文本处理

TODO: 1. bytes转str
      2.
      3.
      4.

REMARK:
"""

def toString(text):
    """
    @summary: bytes转str
    @param text: str/bytes, 文本数据
    @return: str, 处理后文本  
    ---------------------------------------------
    @note: 2019.03.16   增加注释        完成
           2019.xx.xx 
    """
    res = text
    if isinstance(res, bytes):
        res = bytes.decode(res)
    return res


def hasChinese(text):
    """
    @summary: 是否包含汉字
    @param word: str, 本文数据
    @return: bool, 是否包含
    ---------------------------------------------
    @note: 2019.03.19   增加注释        完成
           2019.xx.xx 
    """
    #
    import re
    #
    zh_pattern = re.compile(u'[\u4e00-\u9fa5]+')
    match = zh_pattern.search(text)
    flag = (True if match else False)
    return flag


def hasDigit(text):
    """
    @summary: 是否包含数字 
    @param text: str，文本
    @return: bool, 是否包含 
    ---------------------------------------------
    @note: 2019.04.09   新增        完成
           2019.xx.xx 
    """
    import re
    #
    return bool(re.search(r'\d', text))


def digitToChinese(text):
    """
    @summary: 数字转汉字
    @param text: str，文本
    @return: str, 文本结果 
    ---------------------------------------------
    @note: 2019.04.09   新增        完成
           2019.xx.xx
    """
    rule = {
        '0':'零',
        '1':'一',
        '2':'二',
        '3':'三',
        '4':'四',
        '5':'五',
        '6':'六',
        '7':'七',
        '8':'八',
        '9':'九'
    }
    res = []
    for w in text:
        if w in rule:
            res.append(rule[w])
        else:
            res.append(w)
    return ''.join(res)
