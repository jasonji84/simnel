// 用户行为（微信）

// 应用启动
export function appLaunch (obj) {
  wx.reportAnalytics('basic_launch', {
    first_page: obj.path
  })
}

// 首页相关行为
// 榜单点击
export function channelClick (obj) {
  wx.reportAnalytics('billboard_sort_click', {
    sort_type: obj.channel
  })
}

// 榜单点击产品
export function channelProductClick (obj) {
  wx.reportAnalytics('billboard_product_click', {
    sort_type: obj.channel,
    product_rank: obj.product.rank,
    product_name: obj.product.name,
    product_id: obj.product.id,
    page_duration: obj.time
  })
}

// 分享产品
export function shareApp (obj) {
  // console.log('[shareApp] obj: ', obj)
  let posts = {
    action: obj.action,
    scene: obj.scene,
    product_name: '',
    product_id: ''
  }
  if (typeof obj.product !== 'undefined') {
    posts.product_name = obj.product.name
    posts.product_id = obj.product.id
  }
  wx.reportAnalytics('basic_share', posts)
}

// 用户搜索点击结果行为
export function searchProductClick (obj) {
  // console.log('[searchProductClick] obj: ', obj)
  wx.reportAnalytics('search_product_click', {
    search_source: obj.Search.Source,
    keyword_rank: obj.Search.KeywordRank,
    search_product_rank: obj.Select.Rank,
    product_name: obj.Select.ProductName,
    product_id: obj.Select.ProductId,
    page_duration: obj.Page.Time
  })
}

// 用户提交缺失产品
export function productSubmit (obj) {
  wx.reportAnalytics('missing_product_submit', {
    current_page: obj.path,
    product_name: obj.content
  })
}

// 利率计算结果
export function counterCommentResult (obj) {
  wx.reportAnalytics('counter_result_pageview', {
    product_name: obj.product.name,
    product_id: obj.product.id,
    interest: obj.interest
  })
}

// 利率点评 - 用户输入行为
// 对应两个事件：1. 利率计算提交；2. 离开利率点评页面
export function counterCommentOperation (obj, action = 'submit') {
  let posts = {
    loan: obj.loan,
    extra: obj.extra,
    repay_type: obj.repay.type,
    repay_value: obj.repay.value,
    refund: obj.refund,
    product_name: obj.product,
    page_duration: obj.time
  }
  let eventId = 'counter_submit'
  if (action === 'quit') {
    eventId = 'counter_quit'
    posts.action = obj.action
  }
  wx.reportAnalytics(eventId, posts)
}

// 问卷点评
export function surveyPageView (obj) {
  wx.reportAnalytics('survey_pageview', {
    last_page: obj.lastPage,
    product_id: obj.product.id,
    survey_id: obj.survey.id,
    topic_rank: obj.survey.topic.rank,
    topic_id: obj.survey.topic.id
  })
}

export function surveyAnswer (obj) {
  wx.reportAnalytics('survey_answer', {
    product_id: obj.product.id,
    survey_id: obj.survey.id,
    topic_rank: obj.survey.topic.rank,
    topic_id: obj.survey.topic.id,
    option_rank: obj.survey.option.rank,
    option: obj.survey.option.value,
    page_duration: obj.time
  })
}
