# coding=utf-8

"""
热词
手工导入

TODO: 热词导入 

REMARK: 第三个sheet 
"""


import xlrd
from common.api import getCurrentTime
from milk.db.api import getKeywordRank
from milk.db.service import KeywordRankDB


def check_keyword_rank(vals):
    """
    @summary: 关键词
              字段检查
    @param vals: list, 数据集合
    @return:
    """
    ranks = [v['Rank'] for v in vals]
    # 1. 从0开始连续排名
    for i in range(len(ranks)):
        if i not in ranks:
            print('Error! rank is not right  ', i)
            exit()
    # 2. 无重复项
    keys = [v['Keyword'] for v in vals]
    for k in keys:
        if keys.count(k) > 1:
            print('Error! {} is more than 1'.format(k))
            exit()
    # 3. 是否与最新榜单重复(排位+关键字)
    records = getKeywordRank()
    # 数量和内容完全一致
    if len(vals) == len(records):
        keys1 = ['' for i in range(len(vals))]
        for v in vals:
            keys1[v['Rank']] = v['Keyword']
        keys2 = ['' for i in range(len(vals))]
        for r in records:
            keys2[r['Rank']] = r['Keyword']
        flags = [(k1 == k2) for (k1,k2) in zip(keys1, keys2)]
        if sum(flags) == len(keys):
            print('Error! same as the latest hot search ranking')
            print(keys1)
            print(keys2)
            exit()
    return


# 数据文件
data_file = './data.xlsx'

# 字段配置
col_format = ['Rank', 'Keyword'] 
cmap = {k:v for (k,v) in zip(col_format, [0, 1])}
# 
sheet_idx = 2 
row_idx = 2 

# 导入数据
wb = xlrd.open_workbook(data_file)
sheet = wb.sheet_by_index(sheet_idx)
vals = []
for row in range(row_idx, sheet.nrows):
    val = {key:sheet.cell_value(rowx=row, colx=cmap[key]) for key in cmap}
    if isinstance(val['Rank'], str):
        break
    val['Rank'] = int(val['Rank'])
    vals.append(val)
vals =sorted(vals, key=lambda k:k['Rank'])

# 字段校验
check_keyword_rank(vals)

# 数据库
db = KeywordRankDB()
# Latest字段更新
conds = {'Latest': 1, 'Disabled': 0}
r_n = db.update(conds, {'Latest': 0})
print('update n: ', r_n)

# 新增热词记录
ts = getCurrentTime()
for (i,v) in enumerate(vals):
    vals[i]['Latest'] = 1
    vals[i]['Time'] = ts
res = db.insert(vals)
