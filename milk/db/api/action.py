# -*- coding: utf-8 -*-

"""
数据库访问
接口封装

TODO: 1. 登陆登出行为;
      2. 访问页面行为; 
      3. 搜索栏行为;
      4. 热搜榜单行为
      5. 用户提交产品名称行为;
      6. 用户点击分享行为;

REMARK:
"""

from common.util.date import getCurrentTime

"""
@summary: 用户行为
"""
def addLogin(oid, action=''):
    """
    @summary: 添加用户
              登录登出行为 
    @param oid: str, 微信openid
    @param action: str, 行为类型(登录-login, 登出-logout) 
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.14   代码升级    完成
           2019.xx.xx
    """
    #
    from ..service import LoginActionDB as LoginDB
    #
    vals = {'OpenId':oid, 'Action':action, 'Time': getCurrentTime()}
    res = LoginDB().insert([vals])
    return res!=-1


def addAccess(oid, page=''):
    """
    @summary: 添加用户
              访问页面行为
    @param oid: str, 微信openid
    @param page: str, 页面名称
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.14   代码升级    完成
           2019.xx.xx
    """
    # 
    from ..service import AccessActionDB
    #
    vals = {'OpenId':oid, 'Page':page, 'Time': getCurrentTime()}
    res = AccessActionDB().insert([vals])
    return res != -1

"""
@summary: 搜索行为
"""
def addSearch(vals, src='bar'):
    """
    @summary: 添加搜索行为
    @param vals: dict, 数据
    @param src: src, 来源{bar: 搜索栏, key: 热搜榜单}
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.29   搜索两个接口合并    完成
           2019.xx.xx
    """
    #
    from ..service import KeywordActionDB
    from ..service import SearchbarActionDB
    #
    res = False
    vals['Time'] = getCurrentTime()
    if src == 'bar':
        # 搜索栏搜索
        res = SearchbarActionDB().insert([vals])
    elif src == 'key':
        # 热搜榜单搜索
        res = KeywordActionDB().insert([vals])
    return res 


def addUploadProduct(oid, content='', source=''):
    """
    @summary: 添加自定义
              产品名称
    @param oid: str, 微信openid
    @param content: str, 用户提交产品名称 
    @param source: str, 来源(empty - 搜索为空, noempty - 搜索不为空)
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.13   代码升级    完成
           2019.xx.xx
    """
    #
    from ..service import UploadProductActionDB
    #
    vals = {'OpenId':oid, 'Content':content, 'Source':source, 'Time':getCurrentTime()}
    res = UploadProductActionDB().insert([vals])
    return res!=-1


def addShare(oid, otype, page, scode, pid='', bcode=''):
    """
    @summary: 添加用户
              点击分享行为
    @param oid:   str, 微信openid
    @param otype: str, 打开方式 
    @param page:  str, 页面类型
    @param scode: str, 分享码
    @param pid:   str, 网贷产品ID
    @param bcode: str, 利率计算业务ID
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.02.06   底层分享表单重置    完成
           2019.04.19   新增分享码          完成
           2019.xx.xx
    """
    # 
    from ..service import ShareActionDB
    #
    vals = {'OpenId':oid, 'OpenType':otype, 'Page':page, 'ShareCode': scode, 'ProductId':pid, 'BizCode':bcode, 'Time':getCurrentTime()}
    res = ShareActionDB().insert([vals])
    return res != -1


def addHitShare(uid, scode, gid=''):
    """
    @summary: 添加用户
              点击分享行为
    @param uid: str, 平台用户id(微信openid)
    @param scode: str, 分享码
    @param gid: str, 平台群id
    @return: bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.04.19   新增         完成
           2019.xx.xx
    """
    #
    from ..service import HitShareActionDB
    #
    vals = {'UserId':uid, 'ShareCode':scode, 'GroupId':gid, 'Time':getCurrentTime()}
    res = HitShareActionDB().insert([vals])
    return res != -1
