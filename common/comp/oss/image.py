# -*- coding: utf-8 -*-

"""
图片处理组件

TODO: 1. 图片处理对外接口封装;
      2. 上传服务集成阿里oss. 
"""

from ...base.comp import BaseComponent

class ImageModel(BaseComponent):
    """
    @summary: 图片处理组件
    """
    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict类型, 运行参数 
        """
        from .oss import AliOssHub
        #
        super(ImageModel, self).__init__(params)
        #
        oss_params = self._load_param(params, 'Oss', {})
        self.oss = AliOssHub(oss_params) 
        pass

    def upload_one(self, image):
        """
        @summary: 单个图片上传
        @param image: str, 图片文件
        @return: bool, 成功/失败
        ------------------------------------------------
        @note: 2019.03.11    新增接口      完成
               2019.xx.xx
        """
        import os
        #
        flag = False
        if os.path.isfile(image):
            name = image.split('/')[-1]
            print(self.oss.has(name))
            if self.oss.has(name) == False:
                flag = self.oss.upload_from_file(image, name)
                if flag == False:
                    self.log.warn('[ImageModel][upload_one] upload fails {}'.format(name))
            else:
                self.log.warn('[ImageModel][upload_one] oss has the same name {}'.format(name))
        else:
            self.log.warn('[ImageModel][upload_one] file does not exist')
        return flag

    def upload(self, image_files):
        """
        @summary: 上传服务器
        @param image_files: list, 图片文件
        @return: int, 返回成功上传数量 
        """
        res = 0 
        for f in image_files:
            if os.path.isfile(f):
                name = f.split('/')[-1]
                flag = self.oss.upload_from_file(f, name)
                if flag:
                    res += 1
                else:
                    self.log.warn('[ImageModel][upload] image upload fails ({})'.format(name))
        return res 

    def clean(self, names):
        """
        @summary: 远程图片清理
        @param names: list, 图片名称
        @return: 
        """
        return self.oss.delete(names)
