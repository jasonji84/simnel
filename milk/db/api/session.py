# -*- coding: utf-8 -*-

"""
用户会话
缓存数据接口

TODO: 1. 查询微信openid 
      2. 会话是否存在
      3. 添加会话缓存
      4. 删除会话缓存
      5. 用户SessionKey查询

REMARK:
"""

from ..cache.session import SessionCache


def querySessionCache(key):
    """
    @summary: 查询会话缓存
    @param key: str, 会话key
    @return: str, 成功返回微信openid,
                  失败返回None
    """
    return SessionCache.query(key)


def hasSessionCache(key):
    """
    @summary: 检查是否缓存会
    @param key: str, 会话key
    @return: bool, 成功/失败
    """
    return SessionCache.has(key)


def delSessionCache(key):
    """
    @summary: 删除会话
    @param key: str, 会话key
    @return: bool, 成功/失败
    """
    return SessionCache.remove(key)


def cleanSessionCache():
    """
    @summary: 清空全部会话
    @return: bool, 成功/失败
    """
    return SessionCache.mdel()


def addSessionCache(data):
    """
    @summary: 新增会话
    @param data: list, 数据结合{(会话key, openid)}
    @return: bool, 成功/失败
    """
    res = None
    if len(data) == 1:
        res = SessionCache.set(data[0][0], data[0][1])
    else:
        res = SessionCache.mset(data)
    return res


def querySessionKey(oid):
    """
    @summary: 查询SessionKey
    @param oid: str, 指定微信openId
    @return: str, SessionKey
    """
    from ..service import SessionDB
    #
    vals = SessionDB().query({'OpenId': oid, 'Disabled': 0}, ['SessionKey'])
    res = ''
    if len(vals) == 1:
        res = vals[0][0]
    return res
