# -*- coding: utf-8 -*-

"""
榜单统计
数据库sql模版

TODO: 1. 利率点评统计sql模版
      2. 热度点评统计sql模版
      3. 产品统计sql模版

REMARK: 
"""

def countCounterCommentRank(conditions = {'OpenState':1, 'RankFlag':1}):
    """
    @summary: 利率点评榜单统计
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    @remark: 规则1: 产品在架, 榜单权限开放
             规则2: 20条以上利率点评
             规则3: 平均利率排名前20个网贷产品 
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx
    """
    from common.api import ProductBaseDB
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #
    db0 = ProductBaseDB()
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    #
    table0 = db0.get_table()
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields0 = [db0.find_field(fd) for fd in ['ProductId','Name','Icon']]
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','RankFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenId','Air','Latest']]
    # 获取最新统计
    sql = """
          select {}
                ,{}
                ,avg(a.{}) 
          from {} as a
          join {} as b 
          on {}
          join {} as c
          on {}
          where a.Disabled = 0
                and b.Disabled = 0
                and c.Disabled = 0
                and a.{} = 1
                {}
          group by {}
                  ,{}
          having count(distinct a.{}) >= 20
          order by avg(a.{}) 
          limit 20 
          """.format(addSelectedField(fields2[:1], 'a'),
                     addSelectedField(fields0[1:], 'c'),
                     db2.find_field('Air'),
                     table2,
                     table1,
                     addEqualCondition(fields2[:1], 'b', fields1[:1], 'a'),
                     table0,
                     addEqualCondition(fields0[:1], 'c', fields1[:1], 'a'),
                     fields2[-1],
                     ''.join([addCondition(conditions[k], 'b.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]),
                     addSelectedField(fields2[:1], 'a'),
                     addSelectedField(fields0[1:], 'c'),
                     db2.find_field('OpenId'),
                     db2.find_field('Air'))
          #print(sql)
    res = db1.execute(sql)
    return res 


def countPopCommentRank(conditions = {'OpenState':1, 'RankFlag':1}):
    """
    @summary: 热度点评
              榜单统计
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    @remark: 规则1: 产品在架, 榜单权限开放
             规则2: 20条以上利率点评
             规则3: 平均利率排名前20个网贷产品 
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx
    """
    #
    from common.api import ProductBaseDB
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    from common.api import SurveyCommentDB
    from common.api import VoteCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #
    db0 = ProductBaseDB()
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    db3 = SurveyCommentDB()
    db4 = VoteCommentDB()
    #
    table0 = db0.get_table()
    table1 = db1.get_table()
    table2 = db2.get_table()
    table3 = db3.get_table()
    table4 = db4.get_table()
    #
    fields0 = [db0.find_field(fd) for fd in ['ProductId','Name','Icon']]
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','RankFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenId','Air']]
    fields3 = [db3.find_field(fd) for fd in ['ProductId','OpenId']]
    fields4 = [db4.find_field(fd) for fd in ['ProductId','OpenId']]
    # 
    union_sql = """
                (select {} from {} as a1
                 where a1.Disabled = 0 and a1.Latest = 1) 
                 UNION
                (select {} from {} as a2 
                 where a2.Disabled = 0 and a2.Latest = 1)
                 UNION
                (select {} from {} as a3 
                 where a3.Disabled = 0 and a3.Latest = 1)
                """.format(addSelectedField(fields2[:-1], 'a1'),
                           table2,
                           addSelectedField(fields3, 'a2'),
                           table3,
                           addSelectedField(fields4, 'a3'),
                           table4)
    #
    top_sql = """
              select {} 
                    ,count(distinct b1.{}) as Total
              from ({}) as b1 
              join {} as b2 
              on {}
              where b2.Disabled = 0
                    {}
              group by b1.{}
              having count(distinct b1.{}) >= 20
              order by count(distinct b1.{}) desc 
              limit 20 
              """.format(addSelectedField(fields2[:1], 'b1'),
                         fields2[1],
                         union_sql,
                         table1,
                         addEqualCondition([db1.find_field('ProductId')], 'b1', [db1.find_field('ProductId')], 'b2'),
                         ''.join([addCondition(conditions[k], 'b2.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]),
                         fields2[0],
                         fields2[1],
                         fields2[1])
    #
    sql = """
          select {}
                ,{}
                ,avg(c2.{}) 
          from ({}) as c1
          left join {} as c2
          on {}
          join {} as c3
          on {}
          where c2.Disabled = 0
                and c2.Latest = 1
                and c3.Disabled = 0
          group by {}
                  ,{}
          order by c1.Total desc
          """.format(addSelectedField(fields2[:1], 'c1'),
                     addSelectedField(fields0[1:], 'c3'),
                     fields2[-1],
                     top_sql,
                     table2,
                     addEqualCondition(fields2[:1], 'c1', fields2[:1], 'c2'),
                     table0,
                     addEqualCondition(fields2[:1], 'c1', fields0[:1], 'c3'),
                     addSelectedField(fields2[:1], 'c1'),
                     addSelectedField(fields0[1:], 'c3'))
    res = db1.execute(sql)
    return res


def countProductStats(conditions = {'OpenState':1}):
    """
    @summary: 网贷产品统计 
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    @remark: 规则1: 产品在架, 榜单权限开放
             规则2: 20条以上利率点评
             规则3: 平均利率排名前20个网贷产品 
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx
    """
    #
    from common.api import ProductBaseDB
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    from common.api import SurveyCommentDB
    from common.api import VoteCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #
    db0 = ProductBaseDB()
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    db3 = SurveyCommentDB()
    db4 = VoteCommentDB()
    #
    table0 = db0.get_table()
    table1 = db1.get_table()
    table2 = db2.get_table()
    table3 = db3.get_table()
    table4 = db4.get_table()
    #
    fields0 = [db0.find_field(fd) for fd in ['ProductId']]
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenId','Air']]
    fields3 = [db3.find_field(fd) for fd in ['ProductId','OpenId']]
    fields4 = [db4.find_field(fd) for fd in ['ProductId','OpenId']]
    #
    obj_sql = """
              select {} from {} as {}
              where {}.Disabled = 0 and {}.Latest = 1
              """
    #
    cnt_sql = """
              select {}.ProductId
                    ,count(distinct {}.OpenId) as Total
              from ({}) as {}
              group by {}.ProductId
              """
    #
    obj_table2 = obj_sql.format(addSelectedField(fields2[:-1], 'a1'), table2, 'a1', 'a1', 'a1') 
    obj_table3 = obj_sql.format(addSelectedField(fields3, 'a2'), table3, 'a2', 'a2', 'a2')
    obj_table4 = obj_sql.format(addSelectedField(fields4, 'a3'), table4, 'a3', 'a3', 'a3')
    #
    cnt_table2 = cnt_sql.format('b1', 'b1', obj_table2, 'b1', 'b1')
    cnt_table3 = cnt_sql.format('b2', 'b2', obj_table3, 'b2', 'b2')
    cnt_table4 = cnt_sql.format('b3', 'b3', obj_table4, 'b3', 'b3')
    cnt_union  = cnt_sql.format('b4', 'b4', "({}) UNION ({}) UNION ({})".format(obj_table2, obj_table3, obj_table4), 'b4', 'b4')
    #
    cnt_air_table2 = """
                     select b5.{}
                           ,count(distinct b5.{}) as Total
                           ,avg(b5.{}) as Air
                     from {} as b5 
                     where b5.Disabled = 0 and b5.Latest = 1 
                     group by b5.{}
                     """.format(fields2[0], fields2[1], fields2[-1], table2, fields2[0])
    #
    pid_sql = """
              select {}
              from {} as d1
              join {} as d2 
              on {}
              where d1.Disabled = 0 and d2.Disabled = 0
                    {}
              """.format(addSelectedField(fields0, 'd1'), 
                         table0,
                         table1,
                         addEqualCondition(fields0, 'd1', fields1[:1], 'd2'),
                         ''.join([addCondition(conditions[k], 'd2.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]))
    #
    sql = """
          select c1.ProductId
                ,CASE WHEN c2.Total is null THEN 0 ELSE c2.Total END
                ,CASE WHEN c3.Total is null THEN 0 ELSE c3.Total END
                ,CASE WHEN c4.Total is null THEN 0 ELSE c4.Total END
                ,CASE WHEN c5.Total is null THEN 0 ELSE c5.Total END
                ,CASE WHEN c2.Air is null THEN 0 ELSE c2.Air END
          from ({}) as c1
          left join ({}) as c2
          on c1.ProductId = c2.ProductId
          left join ({}) as c3
          on c1.ProductId = c3.ProductId
          left join ({}) as c4
          on c1.ProductId = c4.ProductId
          left join ({}) as c5
          on c1.ProductId = c5.ProductId
          order by c5.Total desc
          """.format(pid_sql,
                     cnt_air_table2,
                     cnt_table3,
                     cnt_table4,
                     cnt_union)
    res = db1.execute(sql)
    return res


if __name__ == "__main__":
    
    #vals = countCounterCommentRank()
    #vals = countPopCommentRank()
    vals = countProductStats()
    for v in vals:
        print(v)
