# -*- coding: utf-8 -*-

"""
产品统计
数据服务

TODO: 1. 产品统计

REMARK: 
"""


def updateProductStats(params={}):
    """
    @summary: 更新产品统计数据
    @param params: dict, 配置参数
    @return: bool, 成功/失败(判定不完备)
    --------------------------------------------
    @note: 2019.03.16    新增接口        完成
           2019.xx.xx
    """
    from ..db.stats import countProductStats 
    from common.api import ProductStatsDB 
    # 产品统计
    raw_vals = countProductStats()
    # 格式转换
    vals = transform(raw_vals)
    # 更新数据库
    db = ProductStatsDB()
    db.update({'Latest': 1, 'Disabled': 0}, {'Latest': 0})
    res = db.insert(vals)
    return res > 0  


def transform(data):
    """
    @summary: 格式转换
    @param data: list, 产品统计结果, 数据格式:
                                     - 产品Id, 利率点评数, 问卷点评数, 投票点评数, 交互数, 平均利率
                                     - byte, int, int, int, int, float
                                     - 注意, 统计字段不可能为None（sql中处理空值）
    @return: list, 统计结果, 数据格式: 
                             - ProductId: 产品Id
                             - TotalCounter: 利率点评数
                             - TotalSurvey: 问卷点评数 
                             - TotalVote: 投票点评数
                             - TotalUnion: 交互数
                             - Air: 平均利率(年化)
                             - Mir: 平均利率(月息)
                             - Dir: 平均利率(日息)
                             - Time: 时间戳
                             - Latest: 最近记录
    -----------------------------------------------------
    @note: 2019.03.16    新增接口       完成
           2019.xx.xx 
    """
    from common.api import converse 
    from common.api import toString
    from common.api import getCurrentTime
    #
    ts = getCurrentTime()
    cols = ['ProductId', 'TotalCounter', 'TotalSurvey', 'TotalVote', 'TotalUnion', 'Air', 'Mir', 'Dir']
    res = []
    for da in data:
        r = {k:v for (k,v) in zip(cols[1:-2], da[1:])}
        r['ProductId'] = toString(da[0])
        r['Mir'] = converse(r['Air'], option='y2m')
        r['Dir'] = converse(r['Air'], option='y2d')
        r['Time'] = ts
        r['Latest'] = 1 
        res.append(r)
    return res
