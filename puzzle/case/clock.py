# -*- coding: utf-8 -*-

"""
任务定义

TODO: 1. 产品统计
      2. 榜单统计 
      3. 点评统计

REMARK: 
"""

import logging
logging.basicConfig()


def updateProductStatsTask():
    """
    @summary: 产品统计任务
    @return: bool, 成功/失败
    --------------------------------------------
    @note: 2019.03.16   增加          完成
           2019.xx.xx
    """
    from puzzle.biz.stats import updateProductStats
    res = updateProductStats()
    #with open('f.txt','a') as f:
    #    f.write('[updateProductStatsTask] over \n')
    print('[updateProductStatsTask] over')
    return res 


def updateCommentRankTask():
    """
    @summary: 点评榜单统计
              任务 
    @return: bool, 成功/失败
    --------------------------------------------
    @note: 2019.03.15   增加          完成
           2019.xx.xx
    """
    #  
    from puzzle.biz.rank import updateCommentRank 
    # 更新点评榜单统计 
    res = updateCommentRank()
    print('[updateCommentRankTask] over')
    return res


def updateVoteCommentStatsTask():
    """
    @summary: 投票点评统计
              任务 
    @return: bool, 成功/失败
    --------------------------------------------
    @note: 2019.02.17   增加          完成
           2019.xx.xx
    """
    #  
    from puzzle.biz.vote import updateVoteCommentStats
    # 更新点评统计 
    res = updateVoteCommentStats()
    return res


def updateSurveyCommentStatsTask():
    """
    @summary: 问卷点评统计
              任务
    @return: bool, 成功/失败
    ---------------------------------------------
    @note: 2019.02.14   新增          完成
           2019.xx.xx
    """
    #
    from puzzle.biz.survey import updateSurveyCommentStats
    # 更新点评统计
    res = updateSurveyCommentStats()
    return res

def updateCounterCommentStatsTask():
    """
    @summary: 利率点评统计
              任务 
    @return: bool, 成功/失败
    ---------------------------------------------
    @note: 2019.02.17   新增          完成
           2019.xx.xx
    """
    #
    from puzzle.biz.counter import updateCounterCommentStats
    # 更新点评统计
    res = updateCounterCommentStats()
    return res

def updateCommentStatsTask(flag=True):
    """
    @summary: 点评统计任务
    @return:
    ---------------------------------------------
    @note: 2019.02.17   新增          完成
           2019.xx.xx
    """
    #
    from puzzle.biz.counter import updateCounterCommentStats
    from puzzle.biz.survey import updateSurveyCommentStats
    from puzzle.biz.vote import updateVoteCommentStats
    #
    counter_params = {'Reload': flag}
    updateCounterCommentStats(counter_params)
    #
    survey_params = {'Reload': flag, 'Num': 6}
    updateSurveyCommentStats(survey_params)
    #
    vote_params = {'Reload': flag}
    updateVoteCommentStats(vote_params)
    print('[updateCommentStatsTask] over')
    return


if __name__ == '__main__':
    
    # 后台线程, 线程不阻塞
    #from apscheduler.schedulers.background import BackgroundScheduler
    from apscheduler.schedulers.blocking import BlockingScheduler 

    def task():
        with open('f.txt','a') as f:
            f.write('a \n')
    
    # 创建调度器(后台执行模式)
    #scheduler = BackgroundScheduler()
    scheduler = BlockingScheduler()
    # 添加调度任务
    #scheduler.add_job(task, 'interval', seconds=10)
    scheduler.add_job(updateCommentStatsTask, 'interval', minutes=5)
    scheduler.add_job(updateProductStatsTask, 'interval', hours=12)
    scheduler.add_job(updateCommentRankTask, 'interval', hours=12)
    # 启动调度任务
    scheduler.start()
