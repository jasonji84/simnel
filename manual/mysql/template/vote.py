# -*- coding: utf-8 -*-

"""
投票点评统计
数据库服务

TODO: 1. 投票点评统计(使用Latest字段)
      2. 生成sql模版
  
REMARK: 目前暂不使用时间窗口
"""

def countVoteCommentByTimeInterval(conditions = {'OpenState':1, 'VoteFlag':1}, start='2019-01-01 00:00:00', end='2029-12-31 23:59:59'):
    """
    @summary: 投票点评
              按时间窗统计
    @param conditions: dict, 产品限定条件
    @param start: str, 起始时间, 默认2019-01-01 00:00:00
    @parma end: str, 结束时间, 默认2029-12-31 00:00:00
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.11  新增函数+调试     完成
           2019.xx.xx     
    """
    #
    from common.api import ProductFlagDB
    from common.api import VoteCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition  
    #
    db1 = ProductFlagDB()
    db2 = VoteCommentDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','VoteFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','VoteId','TopicId','Option','OpenId','Latest']]
    # 获取最新统计
    sql = """
          select {}
                ,count(distinct a.{})
          from {} as a
          join {} as b 
          on {}
          where a.{} = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                {}
          group by {}
          """.format(addSelectedField(fields2[:-2], 'a'),
                     fields2[-2],
                     table2,
                     table1,
                     addEqualCondition(fields2[:1], 'b', fields1[:1], 'a'),
                     fields2[-1],
                     ''.join([addCondition(conditions[k], 'b.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]),
                     addSelectedField(fields2[:-2], 'a'))
    #print(sql)
    res = db1.execute(sql)
    return res

if __name__ == "__main__":

    print(countVoteCommentByTimeInterval())

    #from puzzle.db import countVoteComment
    #for r in countVoteComment():
    #    print(r)
