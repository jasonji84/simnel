# -*- coding: utf-8 -*-

"""
统计
数据库服务

TODO: 1. 投票点评统计;
      2. 问卷点评统计; 

REMARK: 
"""

def countVoteComment():
    """
    @summary: 投票点评统计 
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.11  新增接口     完成
           2019.xx.xx     
    """
    #
    from common.api import MysqlDB
    #
    sql = """
          select a.ProductId,a.VoteId,a.TopicId,a.OptionTag
                ,count(distinct a.OpenId)
          from uc_vote_comment as a
          join op_product_flag as b 
          on b.ProductId=a.ProductId
          where a.Latest = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                and b.OpenState = 1 
                and b.VoteFlag = 1
          group by a.ProductId,a.VoteId,a.TopicId,a.OptionTag
          """
    res = MysqlDB().execute(sql)
    # print('[countVoteComment] res: ', res)
    return res


def countSurveyComment():
    """
    @summary: 问卷点评统计
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.15  新增接口     完成
           2019.xx.xx
    """
    #
    from common.api import MysqlDB
    #
    sql = """
          select a.ProductId,a.SurveyId,a.TopicId,a.Reply
                ,count(distinct a.OpenId)
          from uc_survey_comment as a
          join op_product_flag as b 
          on b.ProductId=a.ProductId
          where a.Latest = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                and b.OpenState = 1 and b.SurveyFlag = 1
          group by a.ProductId,a.SurveyId,a.TopicId,a.Reply
          """
    res = MysqlDB().execute(sql)
    return res


def countDistinctSurveyComment():
    """
    @summary: 问卷点评统计
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    #
    from common.api import MysqlDB
    sql = """
          select a.ProductId
                ,a.SurveyId
                ,count(distinct a.OpenId) 
          from uc_survey_comment as a
          join op_product_flag as b
          on b.ProductId=a.ProductId
          where a.Latest = 1
                and a.Disabled = 0
                and b.Disabled = 0
                and b.OpenState = 1 and b.SurveyFlag = 1
          group by a.ProductId, a.SurveyId 
          """
    res = MysqlDB().execute(sql)
    # print('[countDistinctSurveyComment] res: ', res)
    return res


def countCounterCommentBySegment():
    """
    @summary: 利率点评
              按区间统计
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.15  新增接口     完成
           2019.xx.xx
    """
    #
    from common.api import MysqlDB
    sql = """
          select pid
                ,segment
                ,count(air)
          from (
              select pid,air,
              (case
               when (air > 0 and air <=0.1) then 'Count0'
               when (air > 0.1 and air <= 0.1825) then 'Count1'
               when (air > 0.1825 and air <= 0.3) then 'Count2'
               when (air > 0.3 and air <= 0.36) then 'Count3'
               else 'Count4' 
               end) as segment
              from (
                  select a.ProductId as pid 
                        ,a.Air as air
                  from uc_counter_comment as a
                  join op_product_flag as b 
                  on b.ProductId=a.ProductId
                  where a.Latest = 1
                        and a.Disabled = 0 
                        and b.Disabled = 0
                        and b.OpenState = 1 and b.CounterFlag = 1
              ) as c) as d
          group by pid,segment
          """
    res = MysqlDB().execute(sql)
    # print('[countCounterCommentBySegment] res: ', res)
    return res


def countDistinctCounterComment():
    """
    @summary: 利率点评统计
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    #
    from common.api import MysqlDB
    sql = """
          select a.ProductId 
                ,count(distinct a.OpenId) 
          from uc_counter_comment as a
          join op_product_flag as b
          on b.ProductId=a.ProductId
          where a.Latest = 1
                and a.Disabled = 0
                and b.Disabled = 0
                and b.OpenState = 1 and b.CounterFlag = 1
          group by a.ProductId 
          """
    res = MysqlDB().execute(sql)
    # print('[countDistinctCounterComment] res: ', res)
    return res


def countCounterCommentRank(num1=20, num2=20):
    """
    @summary: 利率点评榜单统计
    @param num1: int, 利率点评量
    @param num2: int, 输出多少个
    @return: list, 结果集合
    @remark: 规则1: 网贷产品在架且榜单权限开放 
             规则2: 20条以上利率点评
             规则3: 平均利率排名前20个网贷产品
    --------------------------------------------
    @note: 2019.03.13   新增     完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select a.ProductId
                ,c.Name,c.Icon
                ,avg(a.Air) 
          from uc_counter_comment as a
          join op_product_flag as b 
          on b.ProductId=a.ProductId
          join eo_product_base as c
          on c.ProductId=a.ProductId
          where a.Disabled = 0
                and b.Disabled = 0
                and c.Disabled = 0
                and a.Latest = 1
                 and b.OpenState = 1 and b.RankFlag = 1
          group by a.ProductId
                  ,c.Name,c.Icon
          having count(distinct a.OpenId) >= {} 
          order by avg(a.Air) 
          limit {} 
          """.format(num1, num2)
    res = MysqlDB().execute(sql)
    return res


def countPopCommentRank(num1=20, num2=20):
    """
    @summary: 热度榜单统计
    @param num1: int, 最少交互数 
    @param num2: int, 输出多少个
    @return: list, 结果集合
    @remark: 规则1: 网贷产品在架且榜单权限开放
             规则2: 20条以上交互数(用户点评并集: 即用户A对产品A进行利率、投票、问卷点评, 按1个交互算)
             规则3: 交互数前20个网贷产品
             规则4: 输出产品Id, 平均利率 
    --------------------------------------------
    @note: 2019.03.13   新增     完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select c1.ProductId
                ,c3.Name,c3.Icon
                ,avg(c2.Air)
          from (
              select b1.ProductId 
                    ,count(distinct b1.OpenId) as Total
              from (
                (select a1.ProductId,a1.OpenId from uc_counter_comment as a1
                 where a1.Disabled = 0 and a1.Latest = 1) 
                 UNION
                (select a2.ProductId,a2.OpenId from uc_survey_comment as a2 
                 where a2.Disabled = 0 and a2.Latest = 1)
                 UNION
                (select a3.ProductId,a3.OpenId from uc_vote_comment as a3 
                 where a3.Disabled = 0 and a3.Latest = 1)
                ) as b1 
              join op_product_flag as b2 
              on b1.ProductId=b2.ProductId
              where b2.Disabled = 0
                     and b2.OpenState = 1 and b2.RankFlag = 1
              group by b1.ProductId
              having count(distinct b1.OpenId) >= {} 
              order by count(distinct b1.OpenId) desc 
              limit {} 
              ) as c1
          left join uc_counter_comment as c2
          on c1.ProductId=c2.ProductId
          join eo_product_base as c3
          on c1.ProductId=c3.ProductId
          where c2.Disabled = 0
                and c2.Latest = 1
                and c3.Disabled = 0
          group by c1.ProductId
                  ,c3.Name,c3.Icon
          order by c1.Total desc
          """.format(num1, num2)
    res = MysqlDB().execute(sql)
    return res


def countProductStats():
    """
    @summary: 产品统计 
    @return: list, 结果集合, 格式
                             - 产品Id, 利率点评数, 问卷点评数, 投票点评数, 交互数, 平均利率(年化)
    @remark: 规则1: 网贷产品在架
             规则2: 输出产品Id, 平均年化, 三种点评数, 交互数（三种点评并集）
    -----------------------------------------------------------
    @note: 2019.03.13   新增     完成
           2019.xx.xx
    """
    from common.api import MysqlDB
    #
    sql = """
          select c1.ProductId
                ,CASE WHEN c2.Total is null THEN 0 ELSE c2.Total END
                ,CASE WHEN c3.Total is null THEN 0 ELSE c3.Total END
                ,CASE WHEN c4.Total is null THEN 0 ELSE c4.Total END
                ,CASE WHEN c5.Total is null THEN 0 ELSE c5.Total END
                ,CASE WHEN c2.Air is null THEN 0 ELSE c2.Air END
          from (
              select d1.ProductId
              from eo_product_base as d1
              join op_product_flag as d2 
              on d1.ProductId=d2.ProductId
              where d1.Disabled = 0 and d2.Disabled = 0
                     and d2.OpenState = 1
              ) as c1
          left join (
                     select b5.ProductId
                           ,count(distinct b5.OpenId) as Total
                           ,avg(b5.Air) as Air
                     from uc_counter_comment as b5 
                     where b5.Disabled = 0 and b5.Latest = 1 
                     group by b5.ProductId
                     ) as c2
          on c1.ProductId = c2.ProductId
          left join (
              select b2.ProductId
                    ,count(distinct b2.OpenId) as Total
              from (
              select a2.ProductId,a2.OpenId from uc_survey_comment as a2
              where a2.Disabled = 0 and a2.Latest = 1
              ) as b2
              group by b2.ProductId
              ) as c3
          on c1.ProductId = c3.ProductId
          left join (
              select b3.ProductId
                    ,count(distinct b3.OpenId) as Total
              from (
              select a3.ProductId,a3.OpenId from uc_vote_comment as a3
              where a3.Disabled = 0 and a3.Latest = 1
              ) as b3
              group by b3.ProductId
              ) as c4
          on c1.ProductId = c4.ProductId
          left join (
              select b4.ProductId
                    ,count(distinct b4.OpenId) as Total
              from ((
              select a1.ProductId,a1.OpenId from uc_counter_comment as a1
              where a1.Disabled = 0 and a1.Latest = 1
              ) UNION (
              select a2.ProductId,a2.OpenId from uc_survey_comment as a2
              where a2.Disabled = 0 and a2.Latest = 1
              ) UNION (
              select a3.ProductId,a3.OpenId from uc_vote_comment as a3
              where a3.Disabled = 0 and a3.Latest = 1
              )) as b4
              group by b4.ProductId
              ) as c5
          on c1.ProductId = c5.ProductId
          """
    res = MysqlDB().execute(sql)
    return res

