# coding=utf-8

"""
网贷产品
手工导入

TODO: 1. 网贷产品基本信息
      2. 网贷产品状态数据 

REMARK: 1. OpenState设为关闭的, 不存数据库 
        2. 第一个sheet维护
"""

import xlrd
import pinyin
#
from common.api import hasDigit
from common.api import digitToChinese
from common.api import ProductBaseDB
from common.api import ProductFlagDB
from common.util.naming import hashNaming


def transform(vals):
    """
    @summary: 数值转换
    @param vals: list, 数据集合
    @return: list, 结果 
    """
    res = []
    for i,val in enumerate(vals):
        print(i, val)
        r = {} 
        # 1.  基本信息
        # 1.1 产品名称
        na1 = (str(int(val['Name'])) if isinstance(val['Name'], float) else val['Name']) 
        na2 = val['CorrectName']
        r['Name'] = (na2 if na2 else na1)
        # 1.2 名称拼音
        name = r['Name']
        if hasDigit(name):
            name = digitToChinese(name)
        r['Pinyin'] = (pinyin.get(name, format='strip') if has_chinese(name) else name)
        # 1.3 所属公司
        for col in ['Company', 'LoginLink', 'Icon', 'Source']:
            r[col] = val[col]
        # 1.4 产品编码
        # 规则: 根据名称, 6位hash编码 + 2位数字(防止同音字不同, 00-99默认00)
        hashstr = hashNaming(r['Name'])
        r['ProductId'] = '{}-{}'.format(hashstr[:6], hashstr[-6:])
        # 1.5 唯一名称(临时)
        r['UniqueName'] = r['Name']
        # 2.  状态信息
        for col in col_map:
            r[col] = col_map[col][val[col]]
        if r['OpenState']:
            res.append(r)
    return res


def check(vals):
    """
    @summary: 字段校验
    @param vals: list, 数据集合
    @return: 
    """
    # 筛选开放的网贷产品 
    data = []
    for val in vals:
        if val['OpenState'] == 1:
            data.append(val)
    data =sorted(data, key=lambda k:[k['Name'],k['Source']]) 
    # 名字校验
    names = [da['Name'] for da in data]
    chk1 = check_rename(names)
    chk2 = check_naming(names)
    # 编码校验
    pids = [da['ProductId'] for da in data]
    chk3 = check_rename(pids)
    if chk1 or chk2 or chk3:
        print('rename: ', chk1)
        print('naming: ', chk2)
        print('pids:   ', chk3)
    #  
    for (i,da) in enumerate(data):
        # 注册冲突校验
        if da['LoginFlag'] == 1 and da[i]['LoginLink'] != '':
            print('Error! Name: {} OpenState: {} LoginFlag: {} LoginLink: {}'.format(da['Name'], da['OpenState'], da['LoginFlag'], da['LoginLink']))
            exit()
    return  


def has_chinese(text):
    """
    @summary: 是否包含汉字
    @param word: str, 本文数据
    @return: bool, 是否
    """
    #
    import re
    #
    zh_pattern = re.compile(u'[\u4e00-\u9fa5]+')
    match = zh_pattern.search(text)
    flag = (True if match else False)
    return flag


def check_rename(names):
    """
    @summary: 检查是否重名 
    @param names: list, 名字列表
    @return: list, 重名列表
    """
    res = set() 
    ordered_names = sorted(names)
    for (i,na) in enumerate(ordered_names[:-2]):
        if na in ordered_names[i+1:]:
            res.add(na)
    return list(res)


def check_naming(names):
    """
    @summary: 检查名字
    @param names: list, 名字列表
    @return: list, 异常名字列表
    @remark: a. 不包含汉字; 
             b. 名字长度;
    """
    res1 = set()
    # 是否包含中文
    for na in names:
        flag = has_chinese(na)
        if flag is False:
            res1.add(na)
    # 检查长度
    res2 = set()
    for na in names:
        if len(na) >= 6:
            res2.add(na)
    return list(res1), list(res2)


# 运维文件
data_file = './data.xlsx'

# 字段定义
col_format = ['Name', 'CorrectName', 'Company', 'LoginLink',
              'CheckState', 'OpenState',
              'LoginFlag', 'SearchFlag', 'RankFlag', 'CounterFlag', 'SurveyFlag', 'VoteFlag',
              'Icon', 'Source']
# 字段映射
col_map = {
    'CheckState':  {'未审核': 0, '通过': 1, '未通过': -1},
    'OpenState':   {'关闭': 0, '开放': 1},
    'LoginFlag':   {'否': 0, '是': 1},
    'SearchFlag':  {'否': 0, '是': 1},
    'RankFlag':    {'否': 0, '是': 1},
    'CounterFlag': {'否': 0, '是': 1},
    'SurveyFlag':  {'否': 0, '是': 1},
    'VoteFlag':    {'否': 0, '是': 1}
}
# 字段映射表
cmap = {key:idx for (idx, key) in enumerate(col_format)}


# 导入数据
wb = xlrd.open_workbook(data_file)
# 第一个sheet为产品运维
sheet = wb.sheet_by_index(0)
vals = []
for row in range(1, sheet.nrows):
    val = {key: sheet.cell_value(rowx=row, colx=cmap[key]) for key in cmap}
    vals.append(val)

# 字段处理
vals = transform(vals)

# 字段校验
check(vals)

# 数据库校验
keys = ['ProductId', 'Name', 'Pinyin', 'UniqueName', 'Icon', 'Company', 'LoginLink']
db = ProductBaseDB()
# 网贷产品基础信息一致性校验
# 离线校验
cols = ['ProductId', 'UniqueName']
records = []
for rd in db.query({'Disabled':0}, cols):
    records.append({k:str(r) for (k,r) in zip(cols,rd)})
# 校验: 产品编码+唯一命名 
for val in vals:
    for rd in records:
        if (rd['ProductId'] == val['ProductId']) != (rd['UniqueName'] == val['UniqueName']):
            print('Error ! new: {} old: {} '.format(val, rd))
            exit() 

# 数据库保存
# 网贷产品基础信息
for (i,val) in enumerate(vals):
    conditions = {'ProductId': val['ProductId'], 'Disabled': 0}
    # 若存在, 数据覆盖 
    if db.has(conditions):
        # 无效原来记录
        res = db.update(conditions, updated={'Disabled': 1})
    else:
        print('[new] index {} {} {}'.format(i, val['Name'], val['ProductId']))
    # 添加网贷产品数据
    res = db.insert([{k:val[k] for k in keys}])

# 网贷产品状态
keys = ['ProductId', 'CheckState', 'OpenState', 'LoginFlag', 'SearchFlag', 'RankFlag', 'CounterFlag', 'SurveyFlag', 'VoteFlag'] 
db = ProductFlagDB() 
for val in vals:
    conditions = {'ProductId': val['ProductId'], 'Disabled': 0}
    # 若存在, 数据覆盖  
    # 否则添加
    if db.has(conditions):
        # 无效原来记录
        res = db.update(conditions, updated={'Disabled': 1})
    # 添加网贷产品状态
    res = db.insert([{k:val[k] for k in keys}])
