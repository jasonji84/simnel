# coding=utf-8

"""
mysql数据库
操作封装

TODO: 1. 集成表单对象 
      2. 操作应用 

REMARK: 针对应用
"""

#
import copy
from .mysql import MysqlAgency 
from ... import tc 


class MysqlServiceDB(MysqlAgency):
    """
    @summary: mysql数据库
              操作封装 
    """
    # 表单标签(唯一指定某张表)  
    _tag = 'TableTag' 
    # 表单名称  
    _table = 'TableName'
    # 表单字段列表 (有序)
    _fields = []
    # 字段映射 
    _fieldMap = {}
    # 类型映射
    _typeMap = {'str': '', 'int': 0, 'float': 0.0}
    # 操作映射
    _optMap = {'lt':'<','gt':'>','lte':'<=','gte':'>=','ne':'<>'}

    def __init__(self, params={}):
        """
        @summary: 构造器 
        @param params: dict, 配置参数 
        @return:
        """
        super(MysqlServiceDB, self).__init__(params)
        # 载入配置 
        vals = tc.find_table(self._tag) 
        if vals:
            self._table = vals['Table']
            fields = vals['Field']
            self._fieldMap = {}
            for fd in fields:
                self._fieldMap[fd['Name']] = (fd['Field'], fd['Type'])
            self._fields = list(self._fieldMap.keys())
        pass

    def query(self, conditions, fields1=[], fields2=[], num=-1):
        """
        @summary: 表单查询
        @param conditions: dict, 查询条件
                           e.g. conditions={'title':'标题','author':'作者'}
        @param fields1: list, 指定字段输出
        @param fields2: list, 默认字段(不映射)
        @return: list, 返回结果列表
        """
        # 构造查询条件
        cond, vals = self._set_condition(conditions)
        statement = 'select {} from {}{}'.format(','.join([self._fieldMap[fd][0] for fd in fields1] + fields2),
                                                 self._table,
                                                 (' where {}'.format(cond) if cond else ''))
        if num > 0:
            statement += ' limit {}'.format(num)
        res = self.execute(statement, vals) 
        return list(res)    

    def insert(self, data):
        """
        @summary: 添加记录
        @param data: list, 数据集合 
        @return: 成功返回记录数量, 
                 否则返回-1
        """
        statement = "insert into {}({}) values({})".format(self._table,
                                                           ','.join([self._fieldMap[fd][0] for fd in self._fields]),
                                                           ','.join(['%s' for i in range(len(self._fields))]))
        v = [self._typeMap[self._fieldMap[fd][1]] for fd in self._fields]
        vals = []
        for da in data:
            val = copy.copy(v)
            for fd in da:
                val[self._fields.index(fd)] = da[fd]
            vals.append(tuple(val))
        res = self.execute(statement, vals)
        return res

    def update(self, conditions={}, updated={}):
        """ 
        @summary: 更新记录
        @param conditions: dict, 查询条件
                           e.g. condition={'title':'标题','author':'作者'}
        @param updated: dict, 更新内容 
        @return: 返回更新条数 
        """
        statement = "update {} set {} where {}".format(self._table,
                                                       ','.join([self._fieldMap[fd][0]+'=%s' for fd in updated.keys()]),
                                                       ' and '.join([self._fieldMap[fd][0]+'=%s' for fd in conditions.keys()]))
        vals = tuple([updated[key] for key in updated.keys()] + list(conditions.values()))
        res = self.execute(statement,vals)
        return res

    def delete(self, conditions):
        """
        @summary: 删除记录
        @param conditions: dict, 查询条件
                           e.g. condition={'title':'标题','author':'作者'}
        @return: 返回删除条数         
        """
        statement = 'delete from {} where {}'.format(self._table,
                                                     ' and '.join([self._fieldMap[fd][0]+'=%s' for fd in conditions.keys()]))
        res = self.execute(statement, tuple(conditions.values()))
        return res

    def has(self, conditions):
        """
        @summary: 判断标签是否存在
        @param conditions: dict, 查询条件
                           e.g. condition={'title':'标题','author':'作者'}
        @return: 若存在返回True, 否则返回False
        """
        res = self.query(conditions, fields2=['Id'])
        return len(res)>0

    def _set_condition(self, conditions):
        """
        @summary: 查询条件解析
        @param conditions: dict类型, 查询条件
        @return:
        @remark: 支持两种格式
        """
        # 支持逻辑关系及不等式
        if conditions and list(conditions.keys())[0] in ['$and','$or']:
            return self._resolve(conditions[key], key)
        # 简单查询条件
        statement = ' and '.join([self._fieldMap[fd][0]+'=%s' for fd in conditions])
        return statement,list(conditions.values())

    def _resolve(self, conds, lr='$and'):
        """
        @summary: 迭代构造条件
        @param conds: list类型, 查询条件列表
        @param lr: str类型, 逻辑关系
        @return: 表达式+数值列表
        """
        stats,values = [],[]
        for cond in conds:
            if isinstance(cond, dict): 
                key = cond.keys()[0] 
                if key in ['$and','$or']:
                    stat,vals = self._resolve(cond[key],key)
                    stats.append(stat)
                    values +=vals
            else:
                opt = (self._optMap[cond['opt']] if cond.has_key('opt') else '=')
                stats.append('{}{}%s'.format(self._fieldMap[cond['col']][0],opt))
                values.append(cond['val'])
        statement = '({})'.format((' {} '.format(lr[1:])).join(stats))
        return statement, values

    def get_table(self):
        """
        @summary: 获取表单名称
        @return: str, 返回表单名称
        """
        return self._table

    def find_field(self, key):
        """
        @summary: 查询字段
        @param key: str, 字段名称
        @return: dict, 查询结果
        """
        res = '' 
        if key in self._fieldMap:
            res = self._fieldMap[key][0] 
        return res

    def get_fields(self):
        """
        @summary: 获取字段列表
        @return: list, 字段列表
        """
        return self._fields
