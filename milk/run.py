# coding=utf-8

"""
系统运行

TODO: 

REMARK: 模式调整
"""

#import ssl
#
from milk import app
from milk.biz.cache import reloadCache

#from lime.config import host, port

if __name__ == '__main__':
    # 重新加载缓存
    reloadCache()

    #app.run()
    #app.run(host="192.168.3.5", port=5052)
    app.run(host="127.0.0.1", port=5000, debug=False)
    #app.run(host=host, port=port)

#context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
#context.load_cert_chain('./.ssl/name_of_cert_file.crt', keyfile='./.ssl/name_of_key_file.key')

#app.go_fast(host='0.0.0.0', port=5000, ssl=context, workers=os.cpu_count(), debug=True)
