// 通讯配置
// 本地开发
// export const baseUrl = 'http://127.0.0.1:5000/'
// export const baseUrl = 'http://192.168.3.5:5052'
// 测试环境
// export const baseUrl = 'http://94.191.70.15:5000/'
// 生产环境IP
// export const baseUrl = 'http://47.92.232.89:5000/'
// 生产环境https
export const baseUrl = 'https://comment.finartwork.com/'
// oss图片
export const imageUrl = 'https://image.finartwork.com/'
//
// 用户相关操作接口
// 用户登录
export const userLoginUrl = baseUrl + 'user/login'
// 校验会话
export const validateSessionUrl = baseUrl + 'user/validate'
//
// 图片获取服务接口
// 产品图标
// export const productImageUrl = baseUrl + 'image/product'
export const productImageUrl = imageUrl + 'icon'
// 素材图片
export const elementImageUrl = imageUrl + 'adorn'
// export const elementImageUrl = baseUrl + 'image/element'
// 群二维码
export const qrcodeImageUrl = imageUrl + 'contact'
// export const qrcodeImageUrl = baseUrl + 'image/qrcode'
//
// 搜索数据服务
// 产品榜单
export const productRankUrl = baseUrl + 'search/rank/product'
// 热词榜单
export const keywordRankUrl = baseUrl + 'search/rank/keyword'
// 输入搜索
export const searchProductUrl = baseUrl + 'search/product'
// 热词搜索
export const searchKeywordUrl = baseUrl + 'search/keyword'
// 产品详情
export const productDetailUrl = baseUrl + 'search/detail'
// 根据名称查询网贷产品
export const queryProductByNameUrl = baseUrl + 'search/productname'
//
// 点评数据服务
// 增加利率点评
export const addCounterCommentUrl = baseUrl + 'comment/counter/add'
// 统计利率点评
export const countCounterCommentUrl = baseUrl + 'comment/counter/stats'
// 增加问卷点评
export const addSurveyCommentUrl = baseUrl + 'comment/survey/add'
// 统计问卷点评
export const countSurveyCommentUrl = baseUrl + 'comment/survey/stats'
// 获取问卷题库
export const getSurveyBankUrl = baseUrl + 'comment/survey/bank'
// 增加投票点评
export const addVoteCommentUrl = baseUrl + 'comment/vote/add'
// 统计投票点评
export const countVoteCommentUrl = baseUrl + 'comment/vote/stats'
//
// 行为数据服务
// 用户登录和离开app
export const actionLoginUrl = baseUrl + 'action/app'
// 用户上传产品名称
export const actionUploadProductUrl = baseUrl + 'action/upload/product'
// 用户分享
export const actionShareUrl = baseUrl + 'action/share'
// 用户点击分享
export const actionHitShareUrl = baseUrl + 'action/share/hit'
//
// 其它数据服务
//
// 统计数据服务
// 产品模块 - 利率统计
// export const productInterestUrl = baseUrl + 'product/interest'
// 产品模块 - 轻点评统计
// export const productCommentUrl = baseUrl + 'product/comment'
// 计算模块 - 提交计算结果
// export const counterResultUrl = baseUrl + 'counter/result'
//
// export const productnameImageUrl = baseUrl + 'image/productname'
//
// 添加轻点评
// export const bitCommentAddUrl = baseUrl + 'comment/bitcomment/add'
// 轻点评统计
// export const bitCommentStatsUrl = baseUrl + 'comment/bitcomment/stats'
