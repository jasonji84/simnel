# coding=utf-8

"""
微信通信

TODO: 1. 校验登陆凭证  

REMARK:
"""

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#
from common.api import BaseComponent


class WechatRequest(BaseComponent):
    """
    @summary: 微信请求组件
    ------------------------------------------------------------
    @note: 2019.01.05  微信校验临时凭证获取登录信息     完成
           2019.04.20  移至平台目录 + 代码整理          完成 
           2019.xx.xx 
    """
    __header = {'Content-Type': 'application/json'}
    
    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始化参数 
        @return:
        """
        super(WechatRequest, self).__init__(params)
        # 连接池
        self.http = urllib3.PoolManager()

    def checkout(self, code):
        """
        @summary: 微信校验
                  登录临时凭证
        @param code: str, 微信临时凭证
        @return: dict, 返回结果
        """
        import json
        from .config import appId, appSecret, urlDict
        #
        grantType = 'authorization_code'
        url = urlDict['LoginCertificate'].format(appId, appSecret, code, grantType)
        #
        res = {} 
        try:
            r = self.http.request("GET", url, headers=self.__header)  
            res = json.loads(r.data)
        except Exception as e:
            self.log.error("[loginCheckout] fails {} {}".format(r.status, url))
        return res
