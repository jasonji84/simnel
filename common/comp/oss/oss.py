# -*- coding: utf-8 -*-

"""
阿里云oss服务

TODO: 阿里oss接口封装
"""

from ...base.comp import BaseComponent


class AliOssHub(BaseComponent):
    """
    @summary: 阿里云oss服务
    """
    def __init__(self, params={}): 
        """
        @summary: 构造器
        @param params: dict, 配置参数
        @return 
        -------------------------------------------------
        @note: 2019.02.24   新增          完成
               2019.xx.xx
        """
        import oss2
        from .config import access_key_id, access_key_secret
        from .config import bucket, endpoint, path
        #
        super(AliOssHub, self).__init__(params)
        #
        self.__auth = oss2.Auth(access_key_id, access_key_secret)
        self.__bucket = oss2.Bucket(self.__auth, endpoint, bucket)
        #
        self.__path = self._load_param(params, 'Path', path)
        pass

    def has(self, name):
        """
        @summary: 检查文件
                  是否存在
        @param name: str, 对象名称
        @return: bool, 是否存在
        """
        flag = self.__bucket.object_exists(self.__path + "/" + name)
        return flag 

    def upload(self, name, data):
        """
        @summary: 上传操作
        @param name: str, 文件名称 
        @param data: (图片)数据
        @return: bool, 成功返回True, 否则False
        """
        flag = False
        try:
            self.__bucket.put_object(self.__path + "/" + name, data)
            flag = True
        except Exception as e:
            self.log.exception('[oss][upload]  err: ', e)
        return True

    def upload_from_file(self, src_fname, dst_fname):
        """
        @summary: 发送操作
        @param src_fname: str, 源文件路径和文件名 
        @param dst_fname: str, 目标文件名
        @return: bool, 上传成功返回True, 否则False
        """
        flag = False
        try:
            self.__bucket.put_object_from_file('{}/{}'.format(self.__path, dst_fname), src_fname)
            flag = True
        except Exception as e:
            self.log.exception('[oss][upload_from_file]  err: ', e)
        return True

    def delete(self, namelist):
        """ 
        @summary: 删除操作
        @param namelist: list, 文件列表 
        @return: bool, 删除成功返回True, 否则False
        """
        flag = False
        try:
            self.__bucket.batch_delete_objects(['{}/{}'.format(self.__path, na) for na in namelist])
            flag = True
        except Exception as e:
            self.log.exception('[oss][delete]  err: ', e)
        return True
