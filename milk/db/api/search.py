# -*- coding: utf-8 -*-

"""
搜索相关
数据访问接口

TODO: 1. 模糊搜索产品 
      2. 获取产品明细
      3. 获取产品榜单
      4. 获取热词榜单

REMARK: 
"""

def queryProductWithRank(content="", pinyin="", num=20):
    """
    @summary: 搜索产品
              模糊搜索 + 结果带Rank
    @param content: str, 查询内容
    @param pinyin: str, 相应拼音
    @param num: int, 输出多少个结果
    @return: list, 查询结果, 数据格式:
                             - ProductId: 产品Id 
                             - Name: 产品名称
                             - Icon: 产品图标 
                             - TotalComment: 交互数
                             - Rank: 排位
    ----------------------------------------------------
    @note: 2019.04.09    新增     完成
           2019.xx.xx
    """
    from ..service import queryProductWithRank as doSearch
    from common.api import toString
    #
    res = []
    if content and pinyin:
        for v in doSearch(content, pinyin, num):
            r = {}
            r['ProductId'] = toString(v[0])
            r['Name'] = toString(v[1])
            r['Icon'] = toString(v[2])
            r['TotalComment'] = int(v[3])
            r['Rank'] = int(v[-1])
            res.append(r)
    return res 


def queryProductDetail(pid):
    """
    @summary: 网贷产品明细
    @param pid: str, 网贷产品id
    @return: dict, 产品明细, 数据格式:
                             - ProductId: 产品Id
                             - Name: 产品名称
                             - Icon: 产品图标
                             - Company: 所属公司
                             - LoginLink: 注册链接
                             - CounterFlag: 利率权限
                             - SurveyFlag: 问卷权限
                             - VoteFlag: 投票权限
                             - LoginFlag: 注册权限
    --------------------------------------------------------------------
    @note: 2019.01.17   代码升级           完成
           2019.03.16   底层取数升级       完成
                        新增注释
                        数据处理升级
           2019.xx.xx
    """
    #
    from ..service import queryProductDetail
    from common.api import toString
    # 
    res = {} 
    vals = queryProductDetail(pid)
    if vals and len(vals) == 1:
        fields = ['ProductId','Name','Icon','Company','LoginLink', 'CounterFlag','SurveyFlag','VoteFlag','LoginFlag']
        res = {k:toString(v) for (k,v) in zip(fields[:5], vals[0][:5])}
        res.update({k:v for (k,v) in zip(fields[5:], vals[0][5:])}) 
    return res


def getCommentRank(tag=''):
    """
    @summary: 获取榜单数据 
    @param tag: str, 榜单标签
    @return: list, 结果列表, 数据格式:
                             - Rank: 排位
                             - ProductId: 产品Id
                             - Name: 产品名称
                             - Icon: 产品图标
                             - Air: 平均利率 
                             - Index: 利率指数 
                   失败为None 
    ----------------------------------------------------------------------
    @note: 2019.01.06   名字变更(ProductRanking到ProductRank)    完成
           2019.03.15   切换为缓存                               完成
                        名字变更(ProductRank到CommentRank)
                        新增注释
           2019.xx.xx   
    """
    #
    import json
    from ..cache.rank import CommentRankCache
    #
    res = [] 
    val = CommentRankCache.query(tag)
    if val is not None:
        for v in json.loads(val):
            res.append({k:v[k] for k in ['Rank', 'ProductId', 'Name', 'Icon', 'Air', 'Index']})
    return res


def getKeywordRank():
    """
    @summary: 获取(最新)热门榜单
    @return: list, 热词列表, 数据格式:
                             - Rank: 排位
                             - Keyword: 热搜词
    --------------------------------------------------------------------
    @note: 2019.01.06   名字变更（SearchRanking到KeywordRank）    完成
           2019.03.17   底层升级                                  完成
                        新增注释
           2019.xx.xx
    """
    from ..service import queryKeywordRank
    from common.api import toString
    #
    res = []
    for v in queryKeywordRank():
        res.append({'Rank': int(v[0]), 'Keyword': toString(v[1])})
    return res
