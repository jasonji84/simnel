// const fault = require('../fault')

// 缓存相关组件
import {sessionKeyLocalStorage as sessionKey} from '@/utils/cache/config'
import {setStorageSync, getStorageSync, removeStorage} from '@/utils/cache/storage'
// 通信相关组件
import {request} from '../fly'
import {userLoginUrl as loginUrl, validateSessionUrl as sessionUrl} from '@/utils/comm/config'
import {showAlert} from '@/utils/hint'

// session数据
var session = ''
// session微信确认有效
var sessionIsWechatConfirmed = false
// 过程状态变量
// 是否正在微信登录，其他请求轮询稍后，避免重复调用登录接口
var isLogining = false
// 是否正在远程登录服务
var isRemoteLogining = false
// 是否已登陆
var hasLogined = false
// 是否正在校验微信会话，避免重叠调用
var isCheckingWechatSession = false
// 是否正在校验远程会话Key，避免重叠调用
var isCheckingRemoteSession = false
//
// 注意: localStorage和后端通信使用统一名称
// session名称
// var sessionKey = 'session'
// 微信code名称
var codeKey = 'Code'
// 后端校验是否有效名称
var isValidKey = 'Valid'
// 其它项目
// 重复请求次数
var retryLimit = 3
// var profile = false
// var mockJson = false
// var globalData = false

function init (params) {
  // console.log('[login][init] called')
  // 参数配置
  hasLogined = false
  isLogining = false
  isRemoteLogining = false
  isCheckingWechatSession = false
  isCheckingRemoteSession = false
  sessionIsWechatConfirmed = params.doNotCheckSession || false
  codeKey = params.codeKey || 'Code'
  isValidKey = params.isValidKey || 'Valid'
  retryLimit = params.retryLimit || 3
  // profile = params.profile || (require('../profile.js')).reportTime
}

function checkWechatSession () {
  // 检查session有效性
  // console.log('[checkWechatSession] called')
  // 检查session是否有效
  if (isCheckingWechatSession) {
    // 是否正在校验session, 轮询等待, 避免重复调用接口
    setTimeout(function () {
      checkWechatSession()
    }, 500)
  } else if (session) {
    // console.log('[checkWechatSession] session exists')
    // session存在
    if (!sessionIsWechatConfirmed) {
      // console.log('[checkWechatSession] wx.checkSession')
      // wx.checkSession未校验
      isCheckingWechatSession = true
      // let startTime = new Date().getTime()
      wx.checkSession({
        // 校验session是否过期
        success: function (res) {
          // 返回值处理待定
          // console.log('[checkWechatSession] succeed res', res)
          // let endTime = new Date().getTime()
          // if (typeof profile === 'function') {
          //   profile('wx.checkSession', startTime, endTime)
          // }
          // 确认session有效，在本生命周期内无须再检验了
          sessionIsWechatConfirmed = true
          checkRemoteSession()
        },
        fail: function (res) {
          // console.log('[checkWechatSession] fail (session is invalid)')
          // 无效化会话
          invalidateSession()
          // 重新登录
          mainLogin()
        },
        complete: function (res) {
          // console.log('[checkWechatSession] complete')
          // 微信会话校验结束
          isCheckingWechatSession = false
        }
      })
    } else {
      // console.log('[checkWechatSession] checkRemoteSession')
      // 不校验过期（直接远程验证会话）
      // 注意：sessionIsWechatConfirmed需要手动关掉才能走这部分
      checkRemoteSession()
    }
  } else {
    // console.log('[checkWechatSession] mainLogin')
    // session不存在, 运行登录
    mainLogin()
  }
}

function mainLogin () {
  // console.log('[mainLogin] called')
  // 条件：本地session不存在，或无效
  // 过程：微信获取临时凭证，发送后端校验, 返回本地Key
  // 微信登录标识
  let wechatLoginFlag = true
  if (isLogining) {
    // 是否正在登陆中, 轮询等待, 避免重复调用接口
    setTimeout(function () {
      mainLogin()
    }, 500)
  } else {
    // 登陆流程
    isLogining = true
    // 记录调用wx.login前的时间戳
    // let startTime = new Date().getTime()
    // 微信登陆服务
    wx.login({
      success: function (res) {
        // console.log('[login][wx.login] success')
        // 微信获取临时凭证，发送后端校验, 返回本地Key
        if (res.code) {
          // 记录wx.login返回数据后的时间戳，用于上报
          // if (typeof profile === 'function') {
          //  let endTime = new Date().getTime()
          //  profile('wx.login', startTime, endTime)
          // }
          // 远程登录服务
          remoteLogin(res.code)
        } else {
          // 微信登录失败（成功通信，结果异常）
          wechatLoginFlag = false
        }
      },
      fail: function (res) {
        // console.log('[login][wx.login] fails res: ', res)
        // 微信登录失败
        wechatLoginFlag = false
      },
      complete: function (res) {
        // console.log('[login][wx.login] complete')
        // 登录失败，解除锁，防止死锁
        isLogining = false
        if (wechatLoginFlag === false) {
          retryLogin()
        }
      }
    })
  }
}

function remoteLogin (code) {
  // 远程登录服务
  // 过程: 校验临时code有效性，返回本地会话Key
  // console.log('[remoteLogin] called  code: ', code)
  if (isRemoteLogining) {
    // 是否正在校验session, 轮询等待, 避免重复调用接口
    setTimeout(function () {
      remoteLogin()
    }, 500)
  } else {
    isRemoteLogining = true
    var posts = {}
    posts[codeKey] = code
    // 用户登录服务（校验临时code有效性，返回本地会话Key）
    request(loginUrl, 'get', posts,
      function success (res) {
        // console.log('[remoteLogin] success: ', res)
        // 远程登录服务成功
        // 临时凭证有效，则返回会话key
        if (sessionKey in res.data) {
          setStorageSync(sessionKey, res.data[sessionKey])
          sessionIsWechatConfirmed = true
          hasLogined = true
        } else {
          // 临时凭证无效, 重新尝试
          retryLogin()
        }
        isRemoteLogining = false
      },
      function fail (err) {
        // 重新尝试
        retryLogin()
        isRemoteLogining = false
      }
    )
  }
}

function retryLogin () {
  // console.log('[retryLogin] called')
  if (retryLimit > 0) {
    // 延迟等待，尝试登陆
    setTimeout(function () {
      retryLimit = retryLimit - 1
      mainLogin()
    }, 500)
  } else {
    // 增加容错提示页面
    showAlert('错误提示', '用户登录失败')
  }
}

function checkRemoteSession () {
  // 远程会话校验
  // console.log('[checkRemoteSession] called')
  if (isCheckingRemoteSession) {
    // 是否正在校验session, 轮询等待, 避免重复调用接口
    setTimeout(function () {
      checkRemoteSession()
    }, 500)
  } else {
    isCheckingRemoteSession = true
    let posts = {}
    posts[sessionKey] = session
    request(sessionUrl, 'get', posts,
      function success (res) {
        // 校验会话通信成功后处理事件
        // 备注：状态为200，通信已校验
        // console.log('[checkRemoteSession] success')
        if (res.data[isValidKey]) {
          // 会话有效
          hasLogined = true
        } else {
          // 如果本地会话无效, 删除本地会话
          invalidateSession()
          // 执行登录
          mainLogin()
        }
        isCheckingRemoteSession = false
      },
      function fail (err) {
        isCheckingRemoteSession = false
      }
    )
  }
}

function invalidateSession () {
  // console.log('[invalidateSession] called')
  // 无效化Session
  // 场景1: 第三方session校验失败，返回无效
  // 场景2: 发送code至后端，返回数据异常
  removeStorage(sessionKey)
  session = ''
}

export function doLogin (params) {
  // 外部调用登录接口
  // 初始化参数
  init(params)
  // 获取本地会话
  session = getStorageSync(sessionKey)
  // 主流程入口
  checkWechatSession()
}

export function getStatus () {
  // 获取登录状态
  return hasLogined
}
