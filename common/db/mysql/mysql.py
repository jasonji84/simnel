# -*- coding: UTF-8 -*-

"""
mysql数据库
操作类

TODO: 1. 数据池连接独立实现 
      2. mysql基础操作封装 

REMARK: 1. 执行带参数sql时, 请先用sql语句指定需要输入的条件列表，然后再用tuple/list进行条件批配
        2. 在格式sql中不需要使用引号指定数据类型，系统会根据输入参数自动识别
        3. 在输入的值中不需要使用转意函数，系统会自动处理
      
"""

#
from ...base.comp import BaseComponent
#
from .pool import getConnectionPool


class MysqlAgency(BaseComponent):
    """
    @summary: mysql数据库封装
    """
    _opt_map = {'query':'select','insert':'insert','update':'update','delete':'delete'} 

    def __init__(self, params={}):
        """
        @summary: 数据库构造函数,
                  从连接池中取出连接, 并生成操作游标
        @param params: dict类型, 配置参数
        """
        super(MysqlAgency, self).__init__(params)
        pass

    def execute(self, statement, values=None):
        """
        @summary: 执行sql语句
        @param statement: str, sql表达式
        @param values: list/tuple, 数值
        @return: 执行结果
        """
        res = None
        opt = self.__extract(statement)
        #print(statement)
        try:
            with getConnectionPool() as db:
                if opt == 'query':
                    db.cursor.execute(statement, values)
                    res = db.cursor.fetchall() 
                elif opt == 'insert':
                    res = db.cursor.executemany(statement, values)
                    db.conn.commit()
                elif opt in ['update', 'delete']:
                    res = db.cursor.execute(statement, values)
                    db.conn.commit()
        except Exception as e:
            self.log.exception('connection fails  e: ', e)
            self.log.exception('statement: ', statement)
            self.log.exception('values: ', values)
        return res

    def __extract(self, statement):
        """
        @summary: 解析表达式
        @param statement: str, 表达式
        @return: str, 操作类型 
        """
        ret = None
        for key in self._opt_map:
            optstr = self._opt_map[key]
            if statement.strip().startswith(optstr):
                ret = key
        return ret 
