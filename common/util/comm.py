# coding=utf-8

"""
微信端通讯组件

TODO: 1. 获取openid 

REMARK:
"""

import json
import traceback
import urllib3
#
from ..base.comp import BaseComponent 
from ..data.config import appId, appSecret, urlDict


class WechatRequest(BaseComponent):
    """
    @summary: 微信请求组件
    ---------------------------------------------
    @note: 2019.03.16   增加注释        完成
           2019.xx.xx 
    """
    __header = {'Content-Type': 'application/json'}
    
    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始化参数 
        @return:
        ---------------------------------------------
        @note: 2019.03.16   增加注释        完成
               2019.xx.xx
        """
        super(WechatRequest, self).__init__(params)
        #
        self.http = urllib3.PoolManager()

    def loginCheckout(self, code):
        """
        @summary: 登陆凭证校验
        @param data: json打包数据
                     若是json数据, 自动打包
        @param asdict: bool类型, 是否dict输出
        @return: 返回结果
        ---------------------------------------------
        @note: 2019.03.16   增加注释        完成
               2019.xx.xx 
        """
        grantType = 'authorization_code'
        url = urlDict['LoginCertificate'].format(appId, appSecret, code, grantType)
        #
        status = None 
        res = None 
        try:
            r = self.http.request("GET", url, headers=self.__header)
            res = json.loads(r.data)
        except Exception as e:
            print("[loginCheckout] fails ",traceback.print_exc())
            print("[loginCheckout] fails {} {}".format(r.status, url))
        return res
