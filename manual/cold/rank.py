# coding=utf-8

"""
点评榜单
冷启动数据

TODO: 1. 随机用户
      2. 给定日期, 给定数量, 给定平均利率
      3. 利率点评增加mock标记
      4. 满足正态分布

REMARK: 第二个sheet维护(首页两个榜单)
"""

import xlrd
import random
import numpy as np
# 
from common.api import VoteCommentDB
from common.api import CounterCommentDB
#
from common.api import toString
from common.api import getCurrentTime
from common.api import randomSeries
#
from offline import queryProduct

# 时间戳
ts = getCurrentTime()


def load(data_file):
    """
    @summary: 数据文件载入
    @param data_file: str, 数据文件
    @return: 
    """
    # 字段映射
    cmap = {'Name':0, 'CounterNum': 2, 'Avg': 3, 'Min': 4, 'Max': 5, 'VoteNum': 6, 'Deny': 7, 'Pass': 8}
    # 
    sheet_idx = 1
    row_idx = 2
    # 
    vals = []
    wb = xlrd.open_workbook(data_file)
    sheet = wb.sheet_by_index(sheet_idx)
    for row in range(row_idx, sheet.nrows):
        val = {key: sheet.cell_value(rowx=row, colx=cmap[key]) for key in cmap}
        if val['Name'] is '':
            break
        vals.append(val)
    #
    return process(vals) 


def process(data):
    """
    @summary: 预处理
    @param data:
    @return:
    """
    res = []
    for da in data:
        obj = {'ProductId': '', 'Name': '', 'Counter': {'Num': 0, 'Expect': 0, 'Bound': [0,0]}, 'Vote': {'Num': 0, 'Rate': 0.0}}
        # 名字检查
        r = queryProduct([{'Name':da['Name']}, {'OpenState':1, 'CounterFlag':1, 'VoteFlag': 1, 'Disabled':0}], ['ProductId'])
        if r is None or len(r) == 0:
            print('product not exists! {}'.format(da['Name']))
            exit()
        # 产品信息 
        obj['ProductId'] = toString(r[0][0])
        obj['Name'] = da['Name']
        #
        # 投票点评配置
        obj['Vote']['Num'] = (da['VoteNum'] if da['VoteNum'] else random.randint(20, 50)) 
        # 拒绝/通过
        # 如果未配置, 按0.7和0.3配置
        obj['Vote']['Rate'] = (round(float(da['Deny']), 2) if da['Deny'] and da['Pass'] else 0.7)
        # 利率点评配置
        obj['Counter']['Num'] = (da['CounterNum'] if da['CounterNum'] else random.randint(20, obj['Vote']['Num']))
        if da['Avg']:
            obj['Counter']['Expect'] = float(da['Avg'])
        else:
            print('Avg cannot be empty ', da)
            exit()
        # 边界(非必要项: 如果为空, 按Avg -/+ 0.2处理) 
        obj['Counter']['Bound'] = [(float(da['Min']) if da['Min'] else max(da['Avg']-0.2, 0.05)), (float(da['Max']) if da['Max'] else min(da['Avg']+0.2, 1.0))] 
        if obj['Counter']['Bound'][0] >= obj['Counter']['Bound'][1] or obj['Counter']['Bound'][0] >= da['Avg'] or obj['Counter']['Bound'][1] <= da['Avg']:
            print('min and max is error! ', da)
            exit()
        res.append(obj)
    return res


def mock(obj):
    """
    @summary: 批量生成
              冷启动数据(利率点评和投票点评)
    @param obj: dict, 冷启动配置(by网贷产品)
    @return: list, 冷启动数据
    """
    # obj = {'ProductId': '', 'Name': '', 'Counter': {'Num': 0, 'Expect': 0, 'Bound': [0,0]}, 'Vote': {'Num': 0, 'Rate': 0.0}}
    # 基本信息
    pid = obj['ProductId']
    name = obj['Name']
    #
    oids = []
    # 投票点评
    info = {'VoteId': 'v001', 'TopicId': 'vt01'}
    options = ['no', 'yes']
    #
    num  = int(obj['Vote']['Num'])
    rate = obj['Vote']['Rate']
    vc_vals =[]
    for i in range(int(num*rate)):
        oids.append(randomSeries(36))
        vc_vals.append({'ProductId':pid, 'OpenId':oids[-1],
                        'VoteId': info['VoteId'], 'TopicId': info['TopicId'], 'Option':options[0],
                        'Time':ts, 'Latest': 1, 'Remark': 'mock'})
    for i in range(num - int(num*rate)):
        oids.append(randomSeries(36))
        vc_vals.append({'ProductId':pid, 'OpenId':oids[-1],
                        'VoteId': info['VoteId'], 'TopicId': info['TopicId'], 'Option':options[1],
                        'Time':ts, 'Latest': 1, 'Remark': 'mock'})
    # 利率点评
    mock_info = {'ProductId':pid, 'ProductName':name, 
                 'Loan':0.0, 'Extra':0.0, 'Refund':0.0,
                 'RepayType':1, 'RepayPeriod':9, 'RepayPeriodUnit':'month', 'RepayCustom':0,
                 'Time': ts, 'Latest':1, 'Remark':'mock'}
    #
    num = obj['Counter']['Num']
    exp = obj['Counter']['Expect']
    bnd = obj['Counter']['Bound']
    # 
    airs = createBatchAir(exp, bnd, num)
    print(obj['Name'], np.array(airs).mean())
    cc_vals = []
    for i in range(num):
        # 随机利率
        air = airs[i]  # createAir(exp, bnd) 
        irs = {'Air': round(air, 6), 'Mir': round(air/12, 6), 'Dir': round(air/365, 6)}
        #
        val = {'BizCode': randomSeries(36), 'OpenId': oids[i]}
        val.update(mock_info)
        val.update(irs)
        cc_vals.append(val)
    return cc_vals, vc_vals 


def createAir(exp, bnd):
    """
    """
    for i in range(1000): 
        air = np.random.normal(exp, random.uniform(0.05, 0.15))
        if air >= bnd[0] and air <= bnd[1]:
            return air
    return None 


def createBatchAir(exp, bnd, num):
    """
    """
    vals = []
    while len(vals) < num:
        air = np.random.normal(exp, random.uniform(0.05, 0.15))
        if air >= bnd[0] and air <= bnd[1]:
            vals.append(air)
    #
    cnt = 99999999
    while cnt > 0:
        vals = sorted(vals)
        residual = exp - np.array(vals).mean()
        if residual >= 0:
            if residual <= 0.0015:
                return vals
            else:
                vals[0] = random.uniform(vals[0], bnd[1])
        else:
            vals[-1] = random.uniform(bnd[0], vals[-1])
        cnt = cnt - 1
    return None


# 配置文件
data_file = './data.xlsx'
#
samples = load(data_file)
# 
cc_vals, vc_vals = [], []
for sa in samples:
    (a,b) = mock(sa)
    cc_vals += a
    vc_vals += b

# 数据库操作
ccdb = CounterCommentDB()
vcdb = VoteCommentDB()
# 无效化
conds = {'Latest':1,'Disabled':0,'Remark':'mock'}
updates = {'Disabled':1}
ccdb.update(conds, updates)
vcdb.update(conds, updates)
# 新增记录
ccdb.insert(cc_vals)
vcdb.insert(vc_vals)
