# coding=utf-8

"""
通用组件

TODO: 1. 基类对象 
      2. 数据库组件(redis, mysql, mongo待定)
      3. 表单定义
      4. 通用工具
      5. 文档及第三方依赖

REMARK:
"""

import random

import logbook
logbook.set_datetime_format('local')

import os
project_path = os.path.dirname(__file__)

from .db.table import TableConfig
#
# 载入数据库表单定义 
tc_files = []
tc_path = "{}/data/table".format(project_path)
for f in os.listdir(tc_path):
    fname = '{}/{}'.format(tc_path, f)
    if f.endswith('.json') and os.path.isfile(fname):
        tc_files.append(fname)
# 
tc_params = {}
tc_params['TableConfigFileList'] = tc_files
tc = TableConfig(tc_params)
