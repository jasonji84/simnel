// 图片

let imagePath = '/static/images/'
//
// 搜索图标
export const searchImage = imagePath + 'public/icon_search.png'
//
export const cleanImage = imagePath + 'public/icon_clean.png'
//
export const openImage = imagePath + 'public/icon_extra_open.png'
//
export const closeImage = imagePath + 'public/icon_extra_close.png'
//
export const homeImage = imagePath + 'public/icon_home.png'
//
export const shareImage = imagePath + 'public/icon_share.png'
//
// 问卷点评相关图片
/* 背景图
export const surveyBackgroundImage = imagePath + 'element/background.png'
// 问题抬头背景
export const surveyTopicHeaderImage = imagePath + 'element/header.png'
// 小经验抬头背景
export const surveyTipsHeaderImage = imagePath + 'element/header.png'
//
// 利率点评相关图片
// 结果背景
export const counterResultBackgroundImage = imagePath + 'element/background.png'
// 结果页脚背景
export const counterResultFooterImage = imagePath + 'element/footer.png'
*/
