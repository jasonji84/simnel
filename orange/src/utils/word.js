// 文本处理组件

// 文字截断
// 如果文字超出最长字数限制，截断增加后缀
export function maxSlice (text, maxLength = 16, suffix = '...') {
  // console.log('[maxSlice] text: ', text, text.length)
  return text.length > maxLength ? text.slice(0, maxLength) + '...' : text
}

// 校验输入是否无效
// 规则：1. 多个小数点; 2. 小数点前缀; 3. 输入是否为空; 其它待定
export function isAmountInvalid (text) {
  // 预处理
  let value = text.replace(/\s+/g, '')
  // 判定过程
  let res = false
  if (value) {
    if (value.split('.').length > 2) {
      // 多个小数点
      res = true
    } else if ((/^(\.\d*)$/.test(value)) === true) {
      // 小数点为前缀
      res = true
    }
  } else {
    // 输入为空
    res = true
  }
  return res
}

// 数字输入校验及处理
// 规则：1.有效小数不校验；2.非零开头；3.增加千分位；
export function correctNumberInput (text) {
  // 数字输入处理
  let value = text.replace(/,/g, '')
  // 计算器数字输入规则（非零开头）
  if ((/^(0|([1-9]\d*))(\.\d*)?$/.test(value)) === false) {
    value = value.substring(0, value.length - 1)
  }
  //
  let res = addThousandSeparator(value)
  return res
}

// 金额输入校验及处理
// 规则：1.两位有效小数；2.非零开头；3.增加千分位；
export function correctAmountInput (text) {
  // 数字输入处理
  let value = text.replace(/,/g, '')
  // 计算器数字输入规则（非零开头，小数最多2位）
  if ((/^(0|([1-9]\d*))(\.[0-9]{0,2})?$/.test(value)) === false) {
    value = value.substring(0, value.length - 1)
  }
  //
  let res = addThousandSeparator(value)
  return res
}

// 数字转金额表达式
// 规则：1.增加千分位；2.两位有效数；
export function toAmountExpression (val) {
  // 数字转文本
  let value = val.toFixed(2).toString()
  //
  let res = addThousandSeparator(value)
  return res
}

// 数字表达式
// 规则：增加千分位；
export function toNumberExpression (val) {
  let value = val.toString()
  let res = addThousandSeparator(value)
  return res
}

function addThousandSeparator (value) {
  // 增加分位符
  let digits = value.split('.')
  // 整钱部分
  let head = digits[0].split('').reverse()
  // 零钱部分
  let t = ''
  for (let i = 0; i < head.length; i++) {
    t = t + head[i] + ((i + 1) % 3 === 0 && (i + 1) !== head.length ? ',' : '')
  }
  let res = t.split('').reverse().join('')
  if (digits.length > 1) {
    res = res + '.' + digits[1]
  }
  return res
}
