# -*- coding: utf-8 -*-

"""
热词单相关数据
数据库服务

TODO: 1. 热词搜索榜单数据;

REMARK: 
"""

from ..mysql.api import MysqlServiceDB


class KeywordRankDB(MysqlServiceDB):
    """
    @summary: 热词榜单数据 
              表单及数据服务封装
    """
    _tag = 'KeywordRank'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(KeywordRankDB, self).__init__(params)
        pass
