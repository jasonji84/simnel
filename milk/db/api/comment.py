# -*- coding: utf-8 -*-

"""
用户点评
数据业务接口

TODO: 1. mysql服务
         - 添加利率点评;
         - 添加问卷点评;
         - 添加投票点评;
      2. redis服务
         - 利率点评分段统计;
         - 利率点评有效数;
         - 问卷点评标签统计;
         - 问卷点评有效数;
         - 投票点评投票统计; 

REMARK: 
"""

"""
@summary: mysql服务
"""

def addCounterComment(data):
    """
    @summary: 添加利率点评
    @param data: dict, 点评数据
    @return bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.14   代码升级           完成
           2019.02.19   增加Latest字段     完成 
           2019.xx.xx
    """
    #
    from ..service import CounterCommentDB
    from common.util.date import getCurrentTime
    #
    vals = data
    vals['Latest'] = 1
    vals['Time'] = getCurrentTime()
    conds = {'ProductId':vals['ProductId'], 'OpenId':vals['OpenId'], 'Latest': 1, 'Disabled': 0}
    #
    db = CounterCommentDB()
    # 更新原记录(latest字段设为0)
    db.update(conds, {'Latest': 0})
    # 添加记录
    res = db.insert([vals])
    return res != -1


def addSurveyComment(data):
    """
    @summary: 新增问卷点评
    @param data: dict, 点评数据
    @return bool, 是否成功
    --------------------------------------------------------------------------------
    @note: 2019.01.14   代码升级                完成
           2019.02.16   执行逻辑升级            完成
                        - 增加latest字段
                        - 第一题更新历史全部latest字段     by产品by用户by问卷
                        - 非第一题更新历史对应latest字段   by产品by用户by问卷by问题
           2019.xx.xx
    """
    #
    from ..service import SurveyCommentDB
    from common.util.date import getCurrentTime
    #
    vals = data
    vals['Latest'] = 1
    vals['Time'] = getCurrentTime()
    #
    conds = {'ProductId':vals['ProductId'], 'OpenId':vals['OpenId'], 'SurveyId':vals['SurveyId'],
             'Latest':1, 'Disabled':0} 
    if vals['Rank'] > 0: 
        conds['Rank'] = vals['Rank']
    db = SurveyCommentDB()
    # 更新原记录(latest字段设为0)
    db.update(conds, {'Latest': 0})
    # 添加记录
    res = db.insert([vals])
    return res != -1


def addVoteComment(data):
    """
    @summary: 新增投票点评
    @param data: dict, 点评数据
    @return bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.01.19   代码完成           完成
           2019.02.14   增加Latest字段     完成
           2019.xx.xx   
    """
    #
    from ..service import VoteCommentDB
    from common.util.date import getCurrentTime
    #
    vals = data
    vals['Latest'] = 1
    vals['Time'] = getCurrentTime()
    #
    conds = {'ProductId':vals['ProductId'], 'OpenId':vals['OpenId'], 'VoteId': vals['VoteId'],
             'TopicId': vals['TopicId'], 'Latest': 1, 'Disabled': 0}
    db = VoteCommentDB()
    # 更新原记录(latest字段设为0)
    db.update(conds, {'Latest': 0})
    # 添加记录
    res = db.insert([vals])
    return res != -1


def countSurveyCommentToTag(pid, num=6):
    """
    @summary: 网贷产品
              问卷点评标签统计
    @param pid: str, 网贷产品id
    @param num: int, 输出数量
    @return: dict, 结果
    --------------------------------------------------------------------
    @note: 2019.01.18   代码升级    完成
           2019.xx.xx
    """
    #
    from ..service.online import querySurveyCommentStats
    from common.util.word import toString
    #
    res = [] 
    for v in querySurveyCommentStats(pid, num):
        r = {}
        r['Tag'] = toString(v[0])
        r['Count'] = int(v[-1]) 
        res.append(r)
    return res


def querySurveyCommentStatsCache(pid, sid):
    """
    @summary: 获取问卷点评
              统计缓存服务
    @param pid: str, 网贷产品id
    @param sid: str, 问卷id
    @return: dict, 结果
    --------------------------------------------------------------------
    @note: 2019.02.16   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import SurveyCommentCache
    #
    res = []
    val = SurveyCommentCache.query(pid, sid)
    if val is not None:
        for v in json.loads(val):
            r = {}
            r['Tag'] = v[0]
            r['Count'] = v[-1]
            res.append(r)
    return res


"""
@summary: redis服务 
"""
def queryCounterCommentCacheSegmentStats(pid):
    """
    @summary: 利率点评
              分段利率统计
    @param pid: str, 网贷产品id
    @return: list, 利率统计结果
    --------------------------------------------------------------------
    @note: 2019.02.16   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import CounterCommentCache
    #
    res = []
    val = CounterCommentCache.query(pid)
    if val is not None:
        res = json.loads(val)
    return res

def queryCounterCommentCacheDistinctStats(pid):
    """
    @summary: 利率点评
              有效数统计
    @param pid: str, 网贷产品id
    @return: int, 返回有效数, 否则为0 
    --------------------------------------------------------------------
    @note: 2019.02.25   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import CounterCommentStatsCache
    #
    res = 0
    val = CounterCommentStatsCache.query(pid)
    if val is not None:
        res = json.loads(val)
    return res

def querySurveyCommentCacheTagStats(pid, sid):
    """
    @summary: 问卷点评
              热门标签统计
    @param pid: str, 网贷产品id
    @param sid: str, 问卷id
    @return: dict, 结果
    --------------------------------------------------------------------
    @note: 2019.02.16   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import SurveyCommentCache
    #
    res = []
    val = SurveyCommentCache.query(pid, sid)
    if val is not None:
        for v in json.loads(val):
            r = {}
            r['Tag'] = v[0]
            r['Count'] = v[-1]
            res.append(r)
    return res


def querySurveyCommentCacheDistinctStats(pid, sid):
    """
    @summary: 问卷点评
              有效数统计
    @param pid: str, 网贷产品id
    @param sid: str, 问卷id
    @return: int, 返回有效数, 否则为0
    --------------------------------------------------------------------
    @note: 2019.02.25   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import SurveyCommentStatsCache
    #
    res = 0
    val = SurveyCommentStatsCache.query(pid, sid)
    if val is not None:
        res = json.loads(val)
    return res


def queryVoteCommentCachePollStats(pid, vid):
    """
    @summary: 投票点评
              投票数统计
    @param pid: str, 网贷产品id
    @param vid: str, 投票项目id
    @return: dict, 结果
    --------------------------------------------------------------------
    @note: 2019.02.13   新增接口    完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import VoteCommentCache 
    #
    res = []
    val = VoteCommentCache.query(pid,vid)
    if val is not None:
        for v in json.loads(val):
            tid = v['TopicId']
            for tag in v['Option']:
                res.append({'TopicId': tid, 'Tag': tag, 'Count': v['Option'][tag]})
    return res


def updateSingleVoteCommentCache(pid, vid):
    """
    @summary: 更新指定网贷产品
              投票点评缓存
    @param pid: str, 网贷产品ID 
    @param vid: str, 投票项目ID
    @return bool, 是否成功
    --------------------------------------------------------------------
    @note: 2019.06.03   新增           完成
           2019.xx.xx
    """
    #
    import json
    from ..cache.comment import VoteCommentCache
    from ..service import countSingleVoteComment  
    from common.util.word import toString
    #
    # 获取产品投票点评数据
    vals = {} 
    for da in countSingleVoteComment(pid, vid):
        tid = toString(da[2])
        opt = toString(da[3])
        cnt = int(da[-1])
        if tid not in vals:
            vals[tid] = {}
        vals[tid][opt] = cnt
    # 更新缓存
    if vals:
        value = json.dumps([{'TopicId': tid, 'Option': vals[tid]} for tid in vals])
        # print('[updateSingleVoteCommentCache] update: ', pid, vid, value)
        VoteCommentCache.update(pid, vid, value)
    return 
