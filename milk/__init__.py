# coding=utf-8

"""
系统初始化

TODO: 1. sanic初始化
      2. 服务资源

REMARK: 1. 数据架构升级
        2. 离线在线功能分离
"""
#
from common.api import BaseComponent

#
import os
projectPath = os.path.dirname(__file__)

from sanic import Sanic
from sanic import Blueprint
from sanic_compress import Compress

app = Sanic('milk')
Compress(app)

# 缓存服务
from common.api import CacheService 

cs = CacheService()

# Blueprint注册
# 用户服务
from .rsrc.user   import user_bp
# 图片服务
#from .rsrc.image  import image_bp
# 搜索服务
from .rsrc.search import search_bp
# 点评服务
from .rsrc.comment import comment_bp
# 行为服务
from .rsrc.action import action_bp

app.blueprint(user_bp)
#app.blueprint(image_bp)
app.blueprint(search_bp)
app.blueprint(comment_bp)
app.blueprint(action_bp)

# 
@app.route('/')
async def hello(request):
    """
    @summary: 连接测试
    @param request: Request, 请求对象
    @return: 200, 返回hello world
    """
    from sanic import response
    #
    return response.json({'hello': 'world'})


@app.route("/robots.txt", methods=["GET"])
async def robotHandler(request):
    """
    @summary: 获取robot协议
    @param request: Request, 请求对象
    @return: 200, 成功, 返回图片
             400, 失败
    ---------------------------------------
    @note: 2019.02.10  新增接口     完成
           2019.xx.xx  
    """
    import os
    from sanic import response
    from sanic.exceptions import abort
    from .config import robotFile
    #
    if os.path.isfile(robotFile) == False:
        abort(400)
    return await response.file(robotFile)
