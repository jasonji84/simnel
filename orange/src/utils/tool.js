// 公共函数

export function getLastPage (offset = 2) {
  // 获取上一页ID
  let pages = getCurrentPages()
  var page = pages[pages.length - offset]
  return page.route
}
