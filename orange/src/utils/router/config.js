// 路由配置

export const routing = [
  {
    name: 'home',
    path: '/pages/home/main',
    config: {
      navigationBarTitleText: '首页'
    },
    remark: '首页'
  },
  {
    name: 'counter',
    path: '/pages/counter/main',
    config: {
      navigationBarTitleText: '利率照妖镜'
    },
    remark: '计算器tab页'
  },
  {
    name: 'counterbro',
    path: '/pages/counter/bro/main',
    config: {
      navigationBarTitleText: '利率照妖镜'
    },
    remark: '计算器内置页'
  },
  {
    name: 'result',
    path: '/pages/result/main',
    config: {
      navigationBarTitleText: '利率结果'
    },
    remark: '利率计算结果'
  },
  {
    name: 'search',
    path: '/pages/search/main',
    config: {
      navigationBarTitleText: '搜索'
    }
  },
  {
    name: 'detail',
    path: '/pages/detail/main',
    config: {
      navigationBarTitleText: '网贷产品'
    }
  },
  {
    name: 'survey_topic0',
    path: '/pages/survey/topic0/main',
    config: {
      navigationBarTitleText: '轻点评'
    }
  },
  {
    name: 'survey_topic1',
    path: '/pages/survey/topic1/main',
    config: {
      navigationBarTitleText: '轻点评'
    }
  },
  {
    name: 'survey_topic2',
    path: '/pages/survey/topic2/main',
    config: {
      navigationBarTitleText: '轻点评'
    }
  },
  {
    name: 'survey_topic3',
    path: '/pages/survey/topic3/main',
    config: {
      navigationBarTitleText: '轻点评'
    }
  },
  {
    name: 'aboutus',
    path: '/pages/aboutus/main',
    config: {
      navigationBarTitleText: '关于我们'
    },
    remark: '关于我们'
  }
]
