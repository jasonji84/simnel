# coding=utf-8

"""
用户登录行为
相关操作

TODO: 1. 微信用户登录
      2. 微信临时凭证校验
      3. 校验本地会话
      4. 生成本地会话 
 
REMARK:
"""

def wechatLogin(scrip):
    """
    @summary: 微信用户登陆
    @param scrip: str, 微信临时凭证
    @return: (str, str) 返回本地会话key, 
                        否则返回None
    -----------------------------------------------
    @note: 2019.01.05  用户登录流程集成         完成
           2019.01.29  返回本地会话key和OpenId  完成
           2019.xx.xx
    """
    from ...db.service import UserBaseDB
    from ...db.service import SessionDB
    from ...db.api import addSessionCache as addSession
    from ...db.api import delSessionCache as delSession
    from common.api import toString
    #
    res = None
    # 校验临时凭证
    vals = wechatCheckScrip(scrip)
    if vals:
        # 1. 微信openid; 2. 微信会话key
        oid, session = vals
        # 生成本地会话
        local_session = generateSession()
        # 数据库更新
        db = UserBaseDB()
        # 若为新用户, 添加数据库
        if db.has({'OpenId': oid, 'Disabled': 0}) is False:
            # 保存用户信息
            db.insert([{'OpenId': oid}])
        # 用户已存在, 无效化已有会话记录 
        else:
            # 更新会话
            db = SessionDB()
            for r in db.query({'OpenId':oid, 'Disabled': 0}, ['Key']):
                # 获取本地会话key
                old_session = toString(r[0])
                # 删除缓存会话
                delSession(old_session)
            # 无效化记录
            db.update({'OpenId':oid, 'Disabled': 0}, {'Disabled':1})
        #
        # 添加会话记录
        SessionDB().insert([{'Key':local_session, 'SessionKey':session, 'OpenId':oid}])
        # 添加缓存会话
        addSession([(local_session, oid)])
        # 返回结果
        res = (local_session, oid)
    return res


def wechatCheckScrip(scrip):
    """
    @summary: 检查临时凭证
    @param scrip: str, 微信临时凭证
    @return: tuple, 成功返回二元组, (微信openid, 会话key）
                    失败返回None
    ---------------------------------------
    @note: 2019.01.05  发送到微信端校验     完成
           2019.xx.xx
    """
    from ...mp.wechat import WechatRequest
    from common.api import find_value
    #
    res = None
    if scrip:
        vals = WechatRequest().checkout(scrip)
        oid = find_value(vals, ['openid'])
        session = find_value(vals, ['session_key'])
        if oid and session:
            res = (oid, session) 
    return res


def checkSession(session):
    """
    @summary: 校验会话
    @param session: str, 会话
    @return: str, 微信oid, 
                  否则输出None
    ---------------------------------------
    @note: 2019.01.05  存在性校验     完成
           2019.xx.xx  有效期限校验
    """
    from ...db.api import querySessionCache as querySession
    #
    res = None
    if session:
        res = querySession(session)
    return res


def generateSession():
    """
    @summary: 生成会话
    @return: str, 会话序列
    ---------------------------------------
    @note: 2019.01.05  128位随机序列     完成
           2019.xx.xx  加入时间期限 
    """
    from common.api import randomSeries
    #
    res = randomSeries(size=128)
    return res
