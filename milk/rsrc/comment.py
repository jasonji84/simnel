# coding=utf-8

"""
点评相关服务

TODO: 1. 查看利率点评统计
      2. 查看问卷点评统计
      3. 查看投票点评统计
      4. 上传利率点评
      5. 上传问卷点评
      6. 上传投票点评
      7. 获取问卷题库
      
REMARK: 
"""

from . import *


comment_bp = Blueprint('comment_blueprint', url_prefix='/comment')


@comment_bp.route("/counter/stats", methods=["POST"])
async def countCounterCommentHandler(request):
    """
    @summary: 产品分段利率统计
              数据服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - ProductId: str, 网贷产品ID
    @return: 200, 运行成功, dict, 结果集合,
                  数据格式: - Result: dict, 统计结果
                              - ProductId: str, 产品ID
                              - Count: list, 利率分段统计列表
             400, 校验无效
             404, 运行异常
    -----------------------------------------------------
    @note: 2019.01.18   接口代码升级            完成
           2019.02.19   接口升级                完成
                        - 统计切换为缓存服务
                        - 若查询为空处理
           2019.02.26   增加评显示状态          完成
                        接口升级
                        - 若State为False, 则字段Count为[]
                        - 若State为True,  则字段Count为[x,x,x,x,x]
           2019.xx.xx
    """
    from ..biz.user import checkSession
    from ..biz.comment import isCounterCommentReady
    from ..db.api import queryCounterCommentCacheSegmentStats
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    pid = find_value(data, ['Data', 'ProductId'])
    # 对话校验
    oid = checkSession(session)
    if oid is None or pid is None:
        abort(400)
    #
    res = {}
    try:
        # 是否满足显示条件  
        is_ready = isCounterCommentReady(pid)
        res['State'] = is_ready
        # 分段利率统计
        vals = []
        if is_ready: 
            vals = queryCounterCommentCacheSegmentStats(pid)
        res['Count'] = vals
        # 其它信息
        res['ProductId'] = pid
    except Exception as e:
        print(e)
        abort(404)
    #print('[countCounterCommentHandler] res: ', res)
    return response.json({'Result': res}, status=200)


@comment_bp.route("/survey/stats", methods=["POST"])
async def countSurveyCommentHandler(request):
    """
    @summary: 问卷点评统计
              数据服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - ProductId: str, 产品ID
                                - SurveyId: str, 问卷ID
    @return: 200, 运行成功, dict, 结果集合, 
                  数据格式: - Result: dict, 结果 
                              - ProductId: str, 产品ID
                              - Data: list, 统计列表(目前输出6个标签)
                                - Tag: str, 标签
                                - Count: int, 点评数 
             400, 校验无效 
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.18   接口代码升级          完成
           2019.02.16   接口升级              完成
                        - 请求增加问卷ID
                        - 统计切换为缓存服务
           2019.02.26   增加显示状态          完成
                        接口升级
                        - 若State为False, 则字段Count为[]
                        - 若State为True,  则字段Count为[x,x,x,x,x]
           2019.xx.xx
    """
    from ..biz.user import checkSession
    from ..biz.comment import isSurveyCommentReady
    from ..db.api import querySurveyCommentCacheTagStats
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    pid = find_value(data, ['Data', 'ProductId'])
    sid = find_value(data, ['Data', 'SurveyId'], 'sc001')
    # 对话校验
    oid = checkSession(session)
    if oid is None or pid is None:
        abort(400)
    #  
    res = {}
    try:
        # 是否满足显示条件 
        is_ready = isSurveyCommentReady(pid, sid)
        res['State'] = is_ready
        vals = []
        if is_ready:
            # 结果存在, 输出格式[{'Tag':xx, 'Count':x}]
            # 否则输出[]
            vals = querySurveyCommentCacheTagStats(pid, sid) 
        res['Data'] = vals 
        # 其它信息
        res['ProductId'] = pid
        res['SurveyId'] = sid 
    except Exception as e:
        print(e)
        abort(404)
    #print('[countSurveyCommentHandler] res: ', res)
    return response.json({'Result':res}, status=200)


@comment_bp.route("/vote/stats", methods=["POST"])
async def countVoteCommentHandler(request):
    """
    @summary: 投票点评统计
              数据服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - ProductId: str, 网贷产品ID 
                                - VoteId: str, 投票项目ID
    @return: 200, 运行成功, dict, 结果集合, 
                  数据格式: - Result: dict, 统计结果 
                              - ProductId: str, 产品ID 
                              - Count: list, 利率分段统计列表 
             400, 校验无效 
             404, 运行异常
    ----------------------------------------------------
    @note: 2019.01.28   初版 - 临时输出方案     完成
           2019.02.16   接口升级                完成
                        - 请求增加问卷ID
                        - 统计切换为缓存服务
           2019.xx.xx
    """
    from ..biz.user import checkSession
    from ..db.api import queryVoteCommentCachePollStats
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    pid = find_value(data, ['Data', 'ProductId'])
    vid = find_value(data, ['Data', 'VoteId'], 'v001')
    # 对话校验
    oid = checkSession(session)
    if oid is None or pid is None:
        abort(400)
    #  
    res = {}
    try:
        # 若存在, 输出格式[{'TopicId': xxx, 'Tag': xx, 'Count': x}]
        # 若不存在, 输出[]
        vals = queryVoteCommentCachePollStats(pid, vid)
        res['Data'] = vals
        # 其它信息
        res['ProductId'] = pid
        res['VoteId'] =vid
    except Exception as e:
        print(e)
        abort(404)
    #print('[countVoteCommentHandler] res: ', res)
    return response.json({'Result':res}, status=200)


@comment_bp.route("/vote/add", methods=["POST"])
async def addVoteCommentHandler(request):
    """
    @summary: 用户提交投票点评
              数据服务
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 投票点评数据集
                                          - ProductId: str, 产品id
                                          - VoteId: str, 问卷id
                                          - TopicId: str, 问题id
                                          - Option: str, 问题回复
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.27   新增接口            完成
           2019.06.03   新增实时更新缓存    完成
           2019.xx.xx
    """
    from ..biz.user import checkSession
    from ..db.api import addVoteComment
    from ..db.api import updateSingleVoteCommentCache
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    vals = find_value(data, ['Data'])
    # 对话校验
    oid = checkSession(session)
    print(session)
    print(oid)
    print(vals)
    if oid is None or vals is None:
        abort(400)
    # 利率点评数据格式转换
    vals = transform_vc(vals)
    vals['OpenId'] = oid
    #
    res = {}
    try:
        flag = addVoteComment(vals)
        print(flag)
        # 更新缓存
        if flag:
            pid = find_value(vals, ['ProductId'], '')
            vid = find_value(vals, ['VoteId'], '')
            if pid and vid:
                updateSingleVoteCommentCache(pid, vid)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@comment_bp.route("/survey/add", methods=["POST"])
async def addSurveyCommentHandler(request):
    """
    @summary: 用户提交问卷点评
              数据服务
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 问卷点评数据集
                                          - ProductId: str, 产品id 
                                          - SurveyId: str, 问卷id
                                          - TopicId: str, 问题id
                                          - Reply: str, 问题回复
                                          - Rank: int, 第几个问题
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常 
    -------------------------------------------------
    @note: 2019.01.14   接口升级     完成
           2019.xx.xx
    """ 
    from ..biz.user import checkSession
    from ..db.api import addSurveyComment
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    vals = find_value(data, ['Data'])
    # 对话校验
    oid = checkSession(session)
    if oid is None or vals is None:
        abort(400)
    # 利率点评数据格式转换
    vals = transform_sc(vals)
    vals['OpenId'] = oid
    #
    res = {}
    try:
        flag = addSurveyComment(vals)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@comment_bp.route("/counter/add", methods=["POST"])
async def addCounterCommentHandler(request):
    """
    @summary: 用户提交利率点评
              数据服务
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 利率点评数据集（格式参见transform）
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常 
    -------------------------------------------------
    @note: 2019.01.14   接口升级     完成
           2019.xx.xx
    """  
    from ..biz.user import checkSession
    from ..db.api import addCounterComment
    from common.util.naming import randomNaming
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    vals = find_value(data, ['Data'])
    # 对话校验
    oid = checkSession(session)
    if oid is None or vals is None:
        abort(400)
    # 利率点评数据格式转换
    vals = transform_cc(vals)
    vals['OpenId'] = oid
    vals['BizCode'] = randomNaming()
    # 临时填充方案
    if vals['ProductId'] is '':
        vals['ProductId'] = replenish(vals['ProductName'])
    #
    res = {}
    try:
        flag = addCounterComment(vals)
        res['Result'] = {'BizCode': vals['BizCode']}
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@comment_bp.route("/survey/bank", methods=["GET"])
async def surveyBankHandler(request):
    """
    @summary: 获取问卷题目
              数据服务
    @param request: Request, 请求对象
    @return: 200, 运行成功, dict, 结果集合,
                  数据格式: - Result: dict, 问卷题库 
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.25    新增接口, 暂时不用通信     完成
           2019.xx.xx
    """
    import os
    import json
    from common.config import survey_file
    #
    res = {}
    try:
        if os.path.isfile(survey_file):
            with open(survey_file, 'r') as f:
                vals = json.load(f)
                res['Result'] = {'Survey': vals[0]}
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


def transform_sc(data):
    """
    @summary: 问卷点评数据
              格式转换
    @param data: dict, 原数据
    @return: dict, 导入数据库格式数据
    -------------------------------------------------
    @note: 2019.01.26    新增函数     完成
           2019.xx.xx
    """
    res = {}
    for key in ['ProductId','SurveyId','TopicId','Reply']:
        res[key] = find_value(data, [key], '')
    res['Rank'] = find_value(data, ['Rank'], -1)
    #print('transform  data: ', data)
    #print('transform  res:  ', res)
    return res


def transform_vc(data):
    """
    @summary: 投票点评数据
              格式转换
    @param data: dict, 原数据
    @return: dict, 导入数据库格式数据
    -------------------------------------------------
    @note: 2019.01.27    新增函数     完成
           2019.xx.xx
    """
    res = {}
    for key in ['ProductId','VoteId','TopicId','Option']:
        res[key] = find_value(data, [key], '')
    return res


def transform_cc(data):
    """
    @summary: 利率计算点评数据
              格式转换
    @param data: dict, 原数据
    @return: dict, 导入数据库格式数据
    -------------------------------------------------
    @note: 2019.01.19    代码升级     完成
           2019.xx.xx
    """
    res = {}
    # 借款金额
    res['Loan'] = find_value(data, ['Loan'], 0.0)
    # 额外费用
    res['Extra'] = find_value(data, ['Extra'], 0.0)
    # 还款金额
    res['Refund'] = find_value(data, ['Refund'], 0.0)
    # 还款方式
    rt = find_value(data, ['Repay', 'Type'], 'none')
    rtmap = {'none':-1, 'single':0, 'multiple': 1}
    res['RepayType']   = rtmap[rt]
    res['RepayPeriod'] = find_value(data, ['Repay', 'Value'], -1)
    res['RepayPeriodUnit']   = find_value(data, ['Repay', 'Unit'], '')
    res['RepayCustom'] = int(find_value(data, ['Repay', 'Custom'], False))
    # product
    res['ProductId']   = find_value(data, ['Product', 'Id'], '')
    res['ProductName'] = find_value(data, ['Product', 'Name'], '')
    # result
    res['Air'] = find_value(data, ['Result', 'Year'], 0.0)
    res['Mir'] = find_value(data, ['Result', 'Month'], 0.0)
    res['Dir'] = find_value(data, ['Result', 'Date'], 0.0)
    # other
    #res['Source'] = find_value(data, ['Source'], '')
    #print('transform  data: ', data)
    #print('transform  res:  ', res)
    return res


def replenish(name):
    """
    @summary: 根据产品名称
              补全产品ID
    @param name: str, 网贷产品名称 
    @return: str, 网贷产品id, 否则为''
    -------------------------------------------------
    @note: 2019.02.19    临时方案补全产品id     完成
           2019.xx.xx
    """
    #
    from ..db.api import queryProductId
    #
    res = ''
    if name:
        res = queryProductId(name)
    return res 
