// 产品明细数据批量加载

import {prepare} from '@/utils/comm/fly'
import {productDetailUrl, countCounterCommentUrl, countSurveyCommentUrl, countVoteCommentUrl} from '@/utils/comm/config'

var Fly = require('flyio/dist/npm/wx')
var fly = new Fly()

// 批量获取商品信息
export function getProductInfo (posts, successList, fail, callback) {
  fly.all([getProductDetail(posts, successList[0], null),
    getCounterComment(posts, successList[1], null),
    getSurveyComment(posts, successList[2], null),
    getVoteComment(posts, successList[3], null)]).then(fly.spread(function (records, projects) {
    // 两个请求都完成
    typeof callback === 'function' && callback()
  })).catch(function (error) {
    typeof fail === 'function' && fail(error)
    typeof callback === 'function' && callback()
  })
}

// 获取点评统计
export function getProductDetail (posts, success, fail) {
  return request(productDetailUrl, 'get', posts, false, success, fail)
}

// 获取利率点评统计
export function getCounterComment (posts, success, fail) {
  return request(countCounterCommentUrl, 'post', posts, true, success, fail)
}

// 获取问卷点评统计
export function getSurveyComment (posts, success, fail) {
  return request(countSurveyCommentUrl, 'post', posts, true, success, fail)
}

// 获取投票点评统计
export function getVoteComment (posts, success, fail) {
  return request(countVoteCommentUrl, 'post', posts, true, success, fail)
}

// 通信（for 批量使用）
function request (url, method, data, withSession, success, fail) {
  // 预处理
  let obj = prepare(url, method, data, withSession)
  // 执行请求
  return fly.request(obj.url, obj.data, {
    method: obj.method,
    timeout: 5000 // 超时设置为5s
  }).then(function (res) {
    typeof success === 'function' && success(res)
  }).catch(function (error) {
    typeof fail === 'function' && fail(error)
  })
}
