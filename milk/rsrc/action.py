# coding=utf-8

"""
用户行为记录

TODO: 1. 用户登录和离开app
      2. 用户上传自定义产品名称 
      3. 

REMARK:
"""

from . import *


action_bp = Blueprint('action_blueprint', url_prefix='/action')


@action_bp.route('/app', methods=['POST'])
async def loginHandler(request):
    """
    @summary: 登录和离开app
              行为记录
    @summary: 用户提交自定义
              产品名称行为记录
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 参数
                                          - Action: str, 行为类型{login: 登录, logout: 离开}
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.29   接口升级     完成
           2019.xx.xx
    """
    # 对话校验
    from ..biz.user import checkSession
    # 添加数据库
    from ..db.api import addLogin
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    action = find_value(data, ['Data','Action'], '')
    # 字段验证
    oid = checkSession(session)
    if oid is None or action not in ['login', 'logout']:
        abort(400)
    #
    res = {}
    try:
        addLogin(oid, action)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@action_bp.route('/upload/product', methods=['POST'])
async def uploadProductHandler(request):
    """
    @summary: 用户提交自定义
              产品名称行为记录
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 参数
                                          - Content: str, 自定义产品名称 
                                          - Source': str, 来源(empty: 搜索为空情况, noempty: 搜索不为空情况)
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常 
    -------------------------------------------------
    @note: 2019.01.13   接口规范升级     完成
           2019.xx.xx
    """
    # 对话校验
    from ..biz.user import checkSession
    # 添加数据库
    from ..db.api import addUploadProduct
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    content = find_value(data, ['Data','Content'])
    source  = find_value(data, ['Data','Source'])
    # 字段验证
    oid = checkSession(session)
    if oid is None or content is None or source not in ['empty','noempty']:
        abort(400)
    #
    res = {}
    try:
        flag = addUploadProduct(oid, content, source)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@action_bp.route('/share', methods=['POST'])
async def shareHandler(request):
    """
    @summary: 用户分享 
              行为记录
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 参数
                                          - OpenType:  str, 打开类型{menu: 左上角, button: 页内按钮}
                                          - Page:      str, 页面名称
                                          - ProductId: str, 网贷产品id(非必填项)
                                          - BizCode:   str, 利率计算业务(非必填项)
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常
    -------------------------------------------------
    @note: 2019.02.06   接口重置         完成
           2019.04.19   返回分享标记     完成
           2019.xx.xx   
    """
    # 对话校验
    from ..biz.user import checkSession
    # 添加数据库
    from ..db.api import addShare
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    otype = find_value(data, ['Data','OpenType'], '')
    page  = find_value(data, ['Data','Page'], '')
    scode = find_value(data, ['Data','ShareCode'], '')
    pid   = find_value(data, ['Data','ProductId'], '')
    bcode = find_value(data, ['Data','BizCode'], '')
    # 字段验证
    oid = checkSession(session)
    if oid is None or otype not in ['menu', 'button'] or page is '':
        abort(400)
    #
    res = {}
    try:
        # 保存分享行为
        addShare(oid, otype, page, scode, pid, bcode)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@action_bp.route('/share/hit', methods=['POST'])
async def hitShareHandler(request):
    """
    @summary: 用户点击分享
              行为记录
    @param request: 请求对象, 数据格式: - Session: str, 会话key
                                        - Data: dict, 参数
                                          - ShareCode: str, 分享码(必填项)
                                          - Encryption: str, 加密信息(非必填项)
                                          - Iv: str, 加密算法的初始向量
    @return: 200, 运行成功
             400, 校验无效
             404, 运行异常
    -------------------------------------------------
    @note: 2019.04.19   新增     完成
           2019.xx.xx
    """
    # 对话校验
    from ..biz.user import checkSession
    # 微信解密
    from ..mp.wechat import WechatDataCrypt
    # 查询SessionKey
    from ..db.api import querySessionKey
    # 添加数据库
    from ..db.api import addHitShare
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    scode = find_value(data, ['Data','ShareCode'], '')
    einfo = find_value(data, ['Data','Encryption'], '')
    iv = find_value(data, ['Data', 'Iv'], '')
    # 字段验证
    oid = checkSession(session)
    if oid is None:
        abort(400)
    res = {}
    try:
        vals = {}
        if einfo and iv:
            skey = querySessionKey(oid)
            vals = WechatDataCrypt().decrypt(skey, einfo, iv)
        # resolve
        gid = (find_value(vals, ['openGId'], '') if vals else '')
        # 保存分享行为
        addHitShare(oid, scode, gid)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)
