# coding=utf-8

"""
时间相关组件

TODO: 1. 获取当前时间 
      2. 随机时间戳 

REMARK: 
"""

import datetime as dt


def getCurrentTime(tag='time'):
    """
    @summary: 获得当前时间
    @param tag: str, 类型, 时间 - 2019-03-01 12:10:19
                           日期 - 2019-03-01
    @return: str, 返回指定格式时间, 
                  否则返回None
    -------------------------------------------------
    @note: 2019.03.16   时间和日期合并    完成
           2019.xx.xx
    """
    import datetime as dt     
    #
    fmap = {'time': '%Y-%m-%d %H:%M:%S', 'date': '%Y-%m-%d'}
    res = None
    if tag in fmap:
        res = dt.datetime.now().strftime(fmap[tag])
    return res


def randomTimeBatch(start='2018-01-01 00:00:00', end='2029-01-01 00:00:00', num=1):
    """
    @summary: 批量随机时间
    @param start: str, 起始时间
    @param end: str, 结束时间
    @return: list, 随机时间列表
    @remark: 日期格式 2019-01-01 00:00:00
    -------------------------------------------------
    @note: 2019.02.11   新建函数    完成
           2019.xx.xx
    """
    import time
    import random
    from datetime import datetime
    #
    t1 = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    t2 = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
    min_ts = int(time.mktime(min(t1, t2).timetuple()))
    max_ts = int(time.mktime(max(t1, t2).timetuple()))
    #
    res = []
    for i in range(num):
        rand_ts = random.randint(min_ts, max_ts)
        rand_time = datetime.fromtimestamp(rand_ts)
        res.append(datetime.strftime(rand_time, '%Y-%m-%d %H:%M:%S'))
    return res
