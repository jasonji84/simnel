# coding=utf-8

"""
搜索相关服务

TODO: 1. 产品搜索服务
      2. 热词搜索服务
      3. 产品榜单服务
      4. 热词榜单服务
      5. 产品明细服务
      6. 产品名称查询服务

REMARK: 
"""

from . import *

search_bp = Blueprint('search_blueprint', url_prefix='/search')


@search_bp.route("/product", methods=["POST"])
async def searchProductHandler(request):
    """
    @summary: 产品搜索服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - Content: str, 查询短语
    @return: 跳转搜索接口
             400, 校验错误
    -----------------------------------------------------
    @note: 2019.01.07   业务接口和搜索接口分离     完成
           2019.03.29   跳转接口改为调用函数       完成
           2019.xx.xx
    """
    #
    from ..db.api import addSearch
    from ..biz.user import checkSession
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    content = find_value(data, ['Data','Content'], '').strip()
    # 字段验证
    oid = checkSession(session)
    if oid is None or content is '':
        abort(400)
    # 搜索行为记录
    addSearch({'OpenId':oid, 'Content':content}, 'bar')
    # 搜索
    res = {'Result': searchHandler(content)}
    return response.json(res, status=200) 


@search_bp.route("/keyword", methods=["POST"])
async def searchKeywordHandler(request):
    """
    @summary: 热词搜索服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - Keyword: str, 热词
                                - Rank: int, 排位
    @return: 跳转搜索接口
             400, 校验错误
    -------------------------------------------------
    @note: 2019.01.07   业务接口和搜索接口分离     完成
           2019.xx.xx   加入行为记录
    """
    #
    from ..db.api import addSearch
    from ..biz.user import checkSession
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    keyword = find_value(data, ['Data','Keyword'], '')
    rank = find_value(data, ['Data', 'Rank'], -1)
    # 字段验证
    oid = checkSession(session)
    if oid is None or keyword is '' or rank < 0:
        abort(400)
    # 搜索行为记录
    addSearch({'OpenId':oid, 'Keyword':keyword, 'Rank':rank}, 'key')
    # 搜索
    res = {'Result': searchHandler(keyword)}
    return response.json(res, status=200) 


def searchHandler(content):
    """
    @summary: 模糊搜索服务
    @param content: str, 搜索短语           
    @return: list, 结果集合, 数据格式: 
                             - ProductId: str, 产品ID 
                             - Name: str, 产品名称 
                             - Icon: str, 产品图标
                             - TotalComment: int, 交互数
                             - Rank: int, 排位
             否则为[]
    ------------------------------------------------------
    @note: 2019.01.07   搜索接口后置          完成
           2019.03.29   接口改为函数          完成
           2019.04.09   升级mysql查询         完成
                        - 文本+拼音搜索
                        - 根据匹配结果排序 
           2019.xx.xx
    """
    import pinyin
    from common.api import hasDigit
    from common.api import digitToChinese
    from ..db.api import queryProductWithRank
    #
    tval = content
    if hasDigit(tval):
        tval = digitToChinese(tval)
    pval = pinyin.get(digitToChinese(tval), format='strip')
    #
    res = []
    try:
        # 查询产品相关信息
        res = queryProductWithRank(content, pval, 20)
    except Exception as e:
        print(traceback.print_exc())
    return res


@search_bp.route("/rank/product", methods=["POST"])
async def productRankHandler(request):
    """
    @summary: 产品榜单服务
    @param request: Request, 请求对象
                    数据格式: - Session: str, 会话key
                              - Data: dict, 参数
                                - Tag: 榜单类型, {Counter, Pop}
    @return: 200, 运行成功, dict, 结果集合, 
                    数据格式: - Result: list, 结果列表
                                - ProductId: str, 产品ID 
                                - Name: str, 产品名称 
                                - Icon: str, 产品图标
                                - Air: float, 平均年化利率
                                - Index: int, 点评指数
                                - Rank: int, 排位
             400, 校验无效
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.07   体验版v2数据接口调试     完成
           2019.03.15   切换为缓存榜单           完成
                        榜单类型改为Counter, Pop
           2019.xx.xx  
    """
    #
    from ..db.api import getCommentRank  # getProductRank
    from ..biz.user import checkSession
    #
    # 请求解析
    data = request.json
    session = find_value(data, ['Session'])
    tag = find_value(data, ['Data','Tag'])
    #
    # 字段校验
    oid = checkSession(session)
    if oid is None or tag not in ['Counter','Pop']:
        abort(400)
    #
    res = {}
    try:
        # 获取最新产品榜单
        res['Result'] = getCommentRank(tag)    # getProductRank(tag)
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@search_bp.route("/rank/keyword", methods=["GET"])
async def keywordRankHandler(request):
    """
    @summary: 热词榜单服务
    @param request: Request, 请求对象
    @return: 200, 运行成功, dict, 结果集合
                            数据格式: 
                            - Result: list, 列表
                              - Rank: int, 排位
                              - Keyword: str, 热词
             404, 运行异常
    -------------------------------------------------
    @note: 2019.01.07   体验版v2数据接口调试     完成
           2019.xx.xx
    """
    #
    from ..db.api import getKeywordRank
    #
    res = {}
    try:
        res['Result'] = getKeywordRank()
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@search_bp.route("/detail", methods=["GET"])
async def productDetailHandler(request):
    """
    @summary: 产品明细服务
    @param request: Request, 请求对象
    @return: 200, 运行成功, 数据格式: 
                            - Result: dict, 返回数据 
                              - Info: dict, 产品信息
                                - ProductId: str, 产品ID
                                - Name:      str, 产品名称
                                - Icon:      str, 产品图标
                                - Company:   str, 所属公司
                                - LoginLink: str, 注册链接(跟权限关联)
                              - State: dict, 权限状态
                                - Counter:   int, 利率权限
                                - Survey:    int, 问卷权限
                                - Vote:      int, 投票权限
                                - Login:     int, 注册权限
             400, 参数异常
             404, 运行异常
    --------------------------------------------------------
    @note: 2019.01.17   代码升级, 对话权限关闭     完成
           2019.xx.xx   
    """
    #
    from ..db.api import queryProductDetail
    #
    # 参数解析
    # 产品ID 
    pid = request.args.get('ProductId')
    if pid is None:
        abort(400)
    #
    res = {}
    try:
        val = queryProductDetail(pid)
        if val:
            info = {k:val[k] for k in ['ProductId', 'Name', 'Icon', 'Company', 'LoginLink']}
            state = {k1:val[k2] for (k1,k2) in zip(['Counter', 'Survey', 'Vote', 'Login'], 
                                                   ['CounterFlag', 'SurveyFlag', 'VoteFlag', 'LoginFlag'])}
            if state['Login'] == 0:
                info['LoginLink'] = ''
            res['Result'] = {}
            res['Result']['Info'] = info
            res['Result']['State'] = state 
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)


@search_bp.route("/productname", methods=["GET"])
async def productnameHandler(request):
    """
    @summary: 产品名称查询服务
    @param request: Request, 请求对象
    @return: 200, 运行成功, 数据格式:
                            - Result: dict, 返回数据
                              - State: bool, 状态位(唯一记录返回True, 否则返回False)
                              - Info: dict, 产品信息
                                - ProductId: str, 产品ID
                                - Name:      str, 产品名称
                                - Icon:      str, 产品图标 
             400, 参数异常
             404, 运行异常
    --------------------------------------------------------
    @note: 2019.04.17   新增     完成
           2019.xx.xx
    """
    #
    from ..db.api import queryProductByName
    #
    # 参数解析
    # 产品名称 
    name = request.args.get('name')
    if name is None:
        abort(400)
    # 查询
    res = {'Result': {'State': False, 'Info': {}}}
    try:
        vals = queryProductByName(name.strip())
        if len(vals) == 1:
            res['Result']['State'] = True
            res['Result']['Info'] = {k: vals[0][k] for k in ['ProductId', 'Name', 'Icon']}
    except Exception as e:
        print(e)
        abort(404)
    return response.json(res, status=200)
