# coding=utf-8

"""
计算器

TODO: 1. 利率换算 
      2.  

REMARK: 
"""

def converse(meta, option=''):
    """
    @summary: 利率换算 
    @param meta: float, 利率
    @param option: str, 换算类型{'y2m, y2d, m2d, d2m, d2y'}
    @return: float, 计算结果, 异常输出-1
    ---------------------------------------------
    @note: 2019.03.16   增加注释        完成
           2019.xx.xx 
    """
    res = -1 
    if option == 'y2m':
        res = meta / 12
    elif option == 'y2d':
        res = meta / 365
    elif option == 'm2d':
        res = meta / 30
    elif option == 'd2m':
        res = meta * 30
    elif option == 'd2y':
        res = meta * 365
    return round(res, 4) 
