# -*- coding: utf-8 -*-

"""
离线
数据库服务

TODO: 1. 网贷产品基本信息(带功能权限);
      2. 网贷产品统计数据(带功能权限);

REMARK: rank脚本使用 
"""

def queryProduct(conditions=[{}, {'OpenState':1}], fields=['ProductId']):
    """
    @summary: 产品查询 
    @param conditions: list, 基本信息条件
                             权限条件 
    @param fields: list, 输出字段 
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.01.06  产品开放        完成
           2019.03.03  完善查询接口    完成
           2019.xx.xx
    """
    from common.api import ProductBaseDB
    from common.api import ProductFlagDB
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    #    
    db1 = ProductBaseDB()
    db2 = ProductFlagDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in fields]
    fields2 = [db2.find_field(fd) for fd in ['ProductId']]
    #
    sql = """
          select {}
          from {} as a
          join {} as b
          on {}
             and b.Disabled = 0
          where a.Disabled = 0
                {}
                {}
          """.format(addSelectedField(fields1, 'a'),
                     table1,
                     table2,
                     addEqualCondition([db1.find_field('ProductId')], 'a', [db2.find_field('ProductId')], 'b'),
                     ''.join([addCondition(conditions[0][k], 'a.{}'.format(db1.find_field(k)), conditions[0][k]) for k in conditions[0]]),
                     ''.join([addCondition(conditions[1][k], 'b.{}'.format(db2.find_field(k)), conditions[1][k]) for k in conditions[1]]))
    res = db1.execute(sql)
    return res


def queryProductStats(conditions={'OpenState':1}, fields=['ProductId']):
    """
    @summary: 查询网贷统计数据
    @param conditions: dict, 权限条件
    @param fields: list, 输出字段
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.01.06  产品开放     完成
           2019.xx.xx     
    """
    #
    from common.api import ProductFlagDB
    from common.api import ProductStatsDB
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition  
    #
    db1 = ProductStatsDB()
    db2 = ProductFlagDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in fields + ['Time']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenState']]
    # 获取最新统计
    latest_sql = """
                 select {}
                 from {} as a 
                 where a.Disabled = 0
                       and {}
                 """.format(addSelectedField(fields1[:-1], 'a'),
                            table1, 
                            addLatestCondition(fields1[-1], table1, 'a', 'b', fields1[:1]))
    #
    sql = """
          select {}
          from ({}) as c
          join {} as d 
          on {}
          where d.Disabled = 0 
                {}
          """.format(addSelectedField(fields1[:-1], 'c'),
                     latest_sql,
                     table2,
                     addEqualCondition(fields1[:1], 'c', fields2[:1], 'd'),
                     ''.join([addCondition(conditions[k], 'd.{}'.format(db2.find_field(k)), conditions[k])  for k in conditions]))
    res = db1.execute(sql)
    return res
