# -*- coding: utf-8 -*-

"""
用户表单
数据库服务

TODO: 1. 用户基本信息;

REMARK: 
"""

from common.api import MysqlDB 

class UserBaseDB(MysqlDB):
    """
    @summary: 用户基本信息
              表单及数据服务封装
    """
    _tag = 'UserBase'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return 
        """
        super(UserBaseDB, self).__init__(params)
        pass
