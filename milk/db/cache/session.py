# -*- coding: utf-8 -*-


"""
对话表单
数据库服务

TODO: 1. 对话数据库服务(历史数据存留)

REMARK: 
"""

from ... import cs

class SessionCache(object):
    """
    @summary: 对话缓存服务
              操作封装
    """
    # key前缀(格式: 业务##会话key)
    _head = 'Session'

    @staticmethod
    def query(sessionKey):
        """
        @summary: 根据session键查询openId
        @param sessionKey: str, 会话key
        @return: 成功返回微信openid, 失败返回None
        """
        return cs.get('{}##{}'.format(SessionCache._head, sessionKey))

    @staticmethod
    def has(sessionKey):
        """
        @summary: 会话是否存在
        @param sessionKey: str, 会话key
        @return: bool, 成功/失败
        """
        return cs.get('{}##{}'.format(SessionCache._head, sessionKey)) is not None

    @staticmethod
    def set(sessionKey, openId):
        """
        @summary: 新增会话记录
        @param sessionKey: str, 会话key
        @param openId: str, 微信openid
        @return: bool, 成功/失败
        """
        return cs.set('{}##{}'.format(SessionCache._head, sessionKey), openId)

    @staticmethod
    def mset(data):
        """
        @summary: 批量新增会话
        @param data: list, 会话集合
        @return: bool, 成功/失败
        """
        # key是第三方会话code, value目前是oponid
        kvs = {'{}##{}'.format(SessionCache._head, da[0]):da[1] for da in data}
        return cs.mset(kvs)

    @staticmethod
    def remove(sessionKey):
        """
        @summary: 删除会话记录
        @param sessionKey: str, 会话key
        @return: bool, 成功/失败
        """
        return cs.remove('{}##{}'.format(SessionCache._head, sessionKey))
    
    @staticmethod
    def mdel():
        """
        @summary: 删除全部会话记录
        @param sessionKey: str, 会话key
        @return: bool, 成功/失败
        """
        return cs.remove('{}##*'.format(SessionCache._head))
