# -*- coding: utf-8 -*-

"""
mongo数据库
基本操作封装

TODO: 1. 数据库连接; 
      2. 基本操作(查询、插入、更新);

REMARK:
"""

import datetime as dt
import traceback
from pymongo import MongoClient
#
from ..base.comp import BaseComponent


class MongoAgency(BaseComponent):
    """
    @summary: mongo数据库
              基本操作封装
    """
    _database    = 'test'
    _collection  = 'test' 
    _fields = []

    def __init__(self, params={}):
        """
        @summary: 构造器
        """
        super(MongoAgency, self).__init__(params)
        # 数据库连接 
        client = self.connect()
        db = client[self._database]
        self.agency = db[self._collection]
        pass

    def connect(self, params={}):
        """
        @summary: 数据库链接 
        @param params: dict, 配置参数 
        @return: 数据库代理对象 
        """
        db = MongoClient('localhost', 27017)
        return db

    def query(self, conditions={}, fields=[], bans=[]):
        """
        @summary: 查询记录
        @param conditions: dict, 查询条件
        @param fields: list, 指定字段
        @param bans: list, 不获取字段 
        @return: list, 结果集合
        """
        res = [] 
        try:
            fds1 = {fd:1 for fd in fields}
            fds2 = {fd:0 for fd in bans}
            fds1.update(fds2)
            res = self.agency.find(conditions, fds1)
        except Exception as e:
            print("[query] fails ",traceback.print_exc())
            print("[query] fails {} ".format(res))
        return list(res)

    def has(self, conditions):
        """
        @summary: 记录是否存在
        @param conditions: dict, 条件集合
        @return: bool, 是或否
        """
        res = -1 
        try:
            res = self.agency.count(conditions)
        except Exception as e:
            print("[has] fails ",traceback.print_exc())
            print("[has] fails {} ".format(res))
        return res > 0

    def insert(self, data):
        """
        @summary: 添加记录
        @param data: [], 数据集合 
        @return: 返回插入记录数, 否则返回0 
        """
        ts = dt.datetime.now()
        suffix = {'Disabled': 0,
                  'CreateTime': ts,
                  'UpdateTime': ts}
        docs = []
        for da in data:
            docs.append(self.__pack(da, suffix))
        res = 0 
        try:
            #if len(docs) == 1:
            #    res = self.agency.insert_one(docs[0]).inserted_id
            #else: 
            res = self.agency.insert_many(docs)
            #res = 1
        except Exception as e:
            print("[insert] fails ",traceback.print_exc())
            print("[insert] fails {} ".format(res))
        return res

    def update(self, conditions={}, values={}):
        """
        @summary: 更新记录
        @param conditions: dict, 查询条件
        @param values: dict, 字段值
        @return: dict, 更新结果(状态, 更新量, 等) 
        """
        updates = {'UpdateTime': dt.datetime.now()}
        updates.update(values)
        res = -1
        try:
            res = self.agency.update(conditions, {"$set": updates}, multi=True)
        except Exception as e:
            print("[update] fails ",traceback.print_exc())
            print("[update] fails {} ".format(res))
        return res

    def __pack(self, data, suffix={}):
        """
        @summary: 打包文档 
                  字段校验, 补齐字段
        @param data: dict, 数据 
        @param suffix: dict, 后缀字段
        @return: dict, 完整文档
        """
        vals = {fd:'' for fd in self._fields}
        for key in data:
            if key in vals:
                vals[key] = data[key]
        return {**vals, **suffix}

    def aggregate(self, pipeline):
        """
        @summary: 聚合操作
        @param pipeline: list, pipeline参数(聚合操作定义)
        @return: 
        """
        try:
            res = self.agency.aggregate(pipeline)
        except Exception as e:
            print("[aggregate] fails ",traceback.print_exc())
            print("[aggregate] fails {} ".format(res))
        return res
