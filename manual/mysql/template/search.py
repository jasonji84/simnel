# -*- coding: utf-8 -*-

"""
定制化线上服务
数据库服务

TODO: 1. 产品搜索;
      2. 热词榜单;

REMARK: 构造多表查询
"""

def queryProductWithRank(content, pinyin, num = 20):
    """
    @summary: 搜索产品
              模糊搜索 + 结果带Rank
    @param content: str, 查询内容
    @param pinyin: str, 相应拼音
    @param num: int, 输出多少个结果
    @return: list, 查询结果, 数据格式:
                             - 字段: (产品Id, 产品名称, 产品图标, 交互数, 排位)
                             - 类型: (byte, byte, byte, int, float) 
    ----------------------------------------------------
    @note: 2019.04.09    新增     完成
           2019.xx.xx
    """
    from milk.db.service import ProductBaseDB
    #    
    db = ProductBaseDB()
    sql = """
          select t.ProductId
                ,t.Name
                ,t.Icon
                ,t.TotalComment
                ,t.Score
                ,t.RankFlag
                ,@curRank := @curRank + 1 as rank
          from (select a.ProductId
                      ,a.Name
                      ,a.Icon
                      ,b.RankFlag
                      ,(case when c.TotalUnionComment is null then 0 else c.TotalUnionComment end) as TotalComment
                      ,(case
                        when a.Name = '{}' then 10
                        when a.Name like '{}%' then 8 
                        when a.Name like '%{}' then 6 
                        when a.Name like '%{}%' then 7 
                        when a.Pinyin = '{}' then 5 
                        when a.Pinyin like '{}%' then 4 
                        when a.Pinyin like '%{}' then 2 
                        when a.Pinyin like '%{}%' then 3 
                        else 0
                        end) as Score
                from eo_product_base as a
                join op_product_flag as b
                on a.ProductId = b.ProductId
                   and b.Disabled = 0
                   and b.OpenState = 1 and b.SearchFlag = 1
                left join da_product_stats as c
                on a.ProductId = c.ProductId
                   and c.Latest = 1
                   and c.Disabled = 0
                where a.Disabled = 0
                      and (a.Name like '%{}%' and a.Pinyin like '%{}%')) as t, (select @curRank := 0) as r
          order by t.Score desc,t.TotalComment desc,t.RankFlag desc
          limit {}
          """.format(content, content, content, content, pinyin, pinyin, pinyin, pinyin, content, pinyin, num)
    #
    print(sql) 
    res = db.execute(sql)
    return res


if __name__ == "__main__":

    from common.api import toString
    import pinyin
    from common.api import hasDigit
    from common.api import digitToChinese
    
    text = '呗'
    val = text
    if hasDigit(val):
        val = digitToChinese(val)
    pval = pinyin.get(digitToChinese(val), format='strip')
    #
    for r in queryProductWithRank(text, pval):
        print(toString(r[1]), r[-3], r[-2], int(r[-1]))
