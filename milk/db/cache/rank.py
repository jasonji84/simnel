# -*- coding: utf-8 -*-

"""
榜单
缓存服务

TODO: 1. 点评榜单缓存服务;

REMARK: 
"""

from ... import cs

class CommentRankCache(object):
    """
    @summary: 点评榜单
              缓存服务操作封装
    --------------------------------------------
    @note: 2019.03.15   新增     完成
           2019.xx.xx
    """
    # 利率点评榜单
    # key: CommentRank##Counter
    # value: [{}, {}], 格式
    #        - Rank: int, 排名
    #        - ProductId: str, 产品ID
    #        - Rate: float, 平均利率
    #        - Score': int, 利率指数 
    # 投票点评榜单
    # key: CommentRank##Vote
    # value: [{}, {}], 格式
    #        - Rank: int, 排名
    #        - ProductId: str, 产品ID
    #        - Rate: float, 平均利率
    #        - Score': int, 点评热度
    _head = 'CommentRank'
    _tags = ['Counter', 'Pop']

    @staticmethod
    def query(tag):
        """
        @summary: 查询
        @param tag: str, 榜单tag
        @return: 成功返回list, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##'.format(CommentRankCache._head, tag))
