// 热词榜单缓存管理
//
// 远程链接
import {keywordRankUrl as keywordUrl} from '@/utils/comm/config.js'
// 远程通信
import {request} from '@/utils/comm/fly'
// 时间函数
import {getCurrentTime, calTimeSpan} from '@/utils/clock'
// 本地缓存
import {keywordRankLocalStorage as keywordKey} from '@/utils/cache/config'
import {setStorage, getStorageSync} from '@/utils/cache/storage'

// 限定条件：2个小时
var threshold = 2
var unit = 'hr'
// 数据仓库
// 格式：- timestamp: 时间戳
//      - keywords: 已排序热词字列表
var databox = {}
// 后处理回调函数
var postCallback = null

export function getRanking (force = false, callback = null) {
  // 获取热门关键字列表
  postCallback = callback
  // 1. 强制刷新; 2. 数据没有缓存或数据过期
  if (force === true || !check()) {
    // 获取远程数据
    let posts = {}
    request(keywordUrl, 'get', posts,
    function success (res) {
      // 成功获取数据后处理
      // 备注：状态为200，通信已校验
      try {
        let objs = res.data.Result
        let array = Array(objs.length)
        for (var i in objs) {
          let obj = objs[i]
          array[obj.Rank] = obj.Keyword
        }
        setStorage(keywordKey, array)
        databox = {time: getCurrentTime(), keywords: array}
        // 后处理
        typeof postCallback === 'function' && postCallback(array)
      } catch (e) {
        console.warn('[successHandler] e: ', e)
      }
    })
  } else {
    // 否则，载入缓存数据
    typeof postCallback === 'function' && postCallback(databox.keywords)
  }
}

function check () {
  // 有效性判定
  var res = false
  // 条件1: 数据是否初始化
  if (Object.keys(databox).length === 0) {
    databox = getStorageSync(keywordKey, {})
  }
  // 条件2: 数据是否存在
  if ('keywords' in databox && databox.keywords.length > 0) {
    // 条件2: 是否过期
    res = isExpired(databox.time)
  }
  return res
}

function isExpired (timestamp) {
  // 判定是否过期
  var res = true
  if (timestamp instanceof Date) {
    let currentTime = getCurrentTime()
    let val = calTimeSpan(timestamp, currentTime, unit)
    res = (val >= threshold)
  }
  return res
}
