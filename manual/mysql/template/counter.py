# -*- coding: utf-8 -*-

"""
利率点评统计
数据库服务

TODO: 1. 利率点评统计(使用Latest字段)
      2. 生成sql模版
  
REMARK: 目前暂不使用时间窗口
"""

def countCounterCommentRank(conditions = {'OpenState':1, 'CounterFlag':1}):
    """
    @summary: 利率点评榜单统计
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx
    """
    #
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','CounterFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','OpenId','Air','Latest']]
    # 获取最新统计
    sql = """
          select {}
                ,avg(a.{}) 
          from {} as a
          join {} as b 
          on {}
          where a.{} = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                {}
          group by {}
          having count(distinct a.{}) >= 20
          order by avg(a.{}) 
          limit 20 
          """.format(addSelectedField(fields2[:1], 'a'),
                     db2.find_field('Air'),
                     table2,
                     table1,
                     addEqualCondition(fields2[:1], 'b', fields1[:1], 'a'),
                     fields2[-1],
                     ''.join([addCondition(conditions[k], 'b.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]),
                     addSelectedField(fields2[:1], 'a'),
                     db2.find_field('OpenId'),
                     db2.find_field('Air'))
    #print(sql)
    res = db1.execute(sql)
    return res 


def countCounterComment(conditions = {'OpenState':1, 'CounterFlag':1}):
    """
    @summary: 利率点评统计
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx     
    """
    #
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition  
    #
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','CounterFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','Air','OpenId','Latest']]
    # 获取最新统计
    sql = """
          select {}
                ,count(distinct a.{})
          from {} as a
          join {} as b 
          on {}
          where a.{} = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                {}
          group by {}
          """.format(addSelectedField(fields2[:-2], 'a'),
                     table2,
                     table1,
                     addEqualCondition(fields2[:1], 'b', fields1[:1], 'a'),
                     fields2[-1],
                     ''.join([addCondition(conditions[k], 'b.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]),
                     addSelectedField(fields2[:-2], 'a'))
    #print(sql)
    res = db1.execute(sql)
    return res


def countCounterCommentBySegment(conditions = {'OpenState':1, 'CounterFlag':1}):
    """
    @summary: 分段利率统计
    @param conditions: dict, 产品限定条件
    @return: list, 结果集合
    --------------------------------------------
    @note: 2019.02.15  新增函数+调试     完成
           2019.xx.xx
    """
    #
    from common.api import ProductFlagDB
    from common.api import CounterCommentDB
    #
    from common.db.mysql.template import addCondition
    from common.db.mysql.template import addSelectedField
    from common.db.mysql.template import addEqualCondition
    from common.db.mysql.template import addLatestCondition
    #
    db1 = ProductFlagDB()
    db2 = CounterCommentDB()
    #
    table1 = db1.get_table()
    table2 = db2.get_table()
    #
    fields1 = [db1.find_field(fd) for fd in ['ProductId','OpenState','CounterFlag']]
    fields2 = [db2.find_field(fd) for fd in ['ProductId','Air','OpenId','Latest']]
    # 获取最新统计
    sql = """
          select a.{} as pid 
                ,a.{} as air
          from {} as a
          join {} as b 
          on {}
          where a.{} = 1
                and a.Disabled = 0 
                and b.Disabled = 0
                {}
          """.format(fields2[0], fields2[1],
                     table2,
                     table1,
                     addEqualCondition(fields2[:1], 'b', fields1[:1], 'a'),
                     fields2[-1],
                     ''.join([addCondition(conditions[k], 'b.{}'.format(db1.find_field(k)), conditions[k])  for k in conditions]))
    #
    cal_sql = """
              select pid
                    ,segment
                    ,count(air)
              from ( 
              select pid,air,
              (case
               when (air > 0 and air <=0.1) then 'Count0'
               when (air > 0.1 and air <= 0.1825) then 'Count1'
               when (air > 0.1825 and air <= 0.3) then 'Count2'
               when (air > 0.3 and air <= 0.36) then 'Count3'
               else 'Count4' 
               end) as segment
               from ({}) as c) as d
               group by pid, segment 
               """.format(sql) 
    res = db1.execute(cal_sql)
    return res


if __name__ == "__main__":

    vals = countCounterCommentRank()
    #vals = countCounterCommentBySegment()
    print(vals)

    #from puzzle.db import countVoteComment
    #for r in countVoteComment():
    #    print(r)
