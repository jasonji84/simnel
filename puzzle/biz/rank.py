# -*- coding: utf-8 -*-

"""
产品榜单
数据服务

TODO: 1. 利率点评榜单
         - 满足榜单条件: 20条以上点评
         - 前20个网贷产品缓存
      3. 热度点评榜单
         - 满足榜单条件: 20个互动（利率、投票、问卷, 并集）
         - 前20个网贷产品缓存

REMARK: 
"""


def updateCommentRank(params={}):
    """
    @summary: 更新点评榜单数据
    @param params: dict, 配置参数
    @return:
    --------------------------------------------
    @note: 2019.03.14    新增接口        完成
           2019.xx.xx
    """
    from ..db.stats import countCounterCommentRank
    from ..db.stats import countPopCommentRank
    from ..db.cache.rank import CommentRankCache
    #
    from common.api import find_value
    # 榜单统计
    raw_vals1 = countCounterCommentRank()
    raw_vals2 = countPopCommentRank()
    # 格式转换
    vals1 = transform(raw_vals1)
    vals2 = transform(raw_vals2)
    # 更新缓存 
    cache = CommentRankCache()
    # 需要重置, 则重置点评缓存
    cache.mdel()
    # 更新点评缓存
    res1 = cache.set('Counter', vals1)
    res2 = cache.set('Pop', vals2)
    return res1 and res2 


def transform(data):
    """
    @summary: 格式转换
    @param data: list, 榜单统计结果(已排序), 数据格式:
                                             - (产品Id, 名称, 图标, 平均利率)
                                             - (byte, byte, byte, float)
    @return: json, 榜单列表, 数据格式: 
                             - {'Rank': 排名, 'ProductId': 产品Id, 'Name': 名称, 'Icon': 图片, 'Index': 指数, 'Air': 平均利率} 
    --------------------------------------------
    @note: 2019.03.14    新增接口       完成
           2019.xx.xx 
    """
    #
    import json
    from common.api import toString
    #
    cols = ['ProductId', 'Name', 'Icon', 'Air']
    res = []
    for (i,da) in enumerate(data):
        r = {'Rank':i}
        r['ProductId'] = toString(da[0])
        r['Name'] = toString(da[1])
        r['Icon'] = toString(da[2])
        r['Air'] = (round(float(da[-1]), 4) if da[-1] else -1)
        r['Index'] = cal(r['Air']) 
        res.append(r)
    return json.dumps(res)


def cal(val):
    """
    @summary: 指数计算
    @param val: float, 利率
    @return: int, 指数(0, 5)
    """
    res = -1 
    if val and val > 0: 
        if val > 0 and val <= 0.1:
            res = 5
        elif val > 0.1 and val <= 0.1825:
            res = 4
        elif val > 0.1825 and val <= 0.3:
            res = 3
        elif val > 0.3 and val <= 0.36:
            res = 2 
        else: 
            res = 1
    return res
