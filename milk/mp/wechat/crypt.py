# coding=utf-8

"""
微信解密数据

TODO: 
 
REMARK:
"""

from common.api import BaseComponent

class WechatDataCrypt(BaseComponent):
    """
    @summary: 微信解密数据
    """
    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 配置参数
        @return 
        -------------------------------------------------
        @note: 2019.04.20   新增          完成
               2019.xx.xx
        """
        super(WechatDataCrypt, self).__init__(params) 
        #
        #self.__appId = self._load_param(params, 'AppId', '')
        #self.__sessionKey = self._load_param(params, 'SessionKey', '')
        self._unpad = lambda s: s[:-ord(s[len(s)-1:])]
        pass

    def decrypt(self, sessionKey, data, iv):
        """
        @summary: 解码
        @param sessionKey: str, 用户会话
        @param data: str, 加密数据
        @param iv: str, 加密数据初始向量
        @return: dict, 结果集合 
        """
        import base64
        import json
        from Crypto.Cipher import AES
        #
        from .config import appId
        # base64 decode
        dsession = base64.b64decode(sessionKey)
        ddata = base64.b64decode(data)
        dvector = base64.b64decode(iv)
        #
        res = {}
        try:
            cipher = AES.new(dsession, AES.MODE_CBC, dvector)
            decrypted = json.loads(self._unpad(cipher.decrypt(ddata)))
            # 检查水印
            if decrypted['watermark']['appid'] == appId:
                res = decrypted
        except Exception as e:
            print(e)
        return res 
