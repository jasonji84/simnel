# -*- coding: utf-8 -*-

"""
点评
缓存服务

TODO: 1. 投票点评缓存服务;
      2. 利率点评缓存服务;
      3. 问卷点评缓存服务;
      4. 网贷产品点评状态缓存服务;

REMARK: 
"""

from ... import cs


class VoteCommentCache(object):
    """
    @summary: 利率点评缓存服务 
              操作封装
    --------------------------------------------
    @note: 2019.02.13  新增接口     完成
           2019.xx.xx
    """
    # 格式: 业务##产品ID##投票Id##问题Id##
    # 数据: {'Yes': xx, 'No': xx}
    _head = 'Vote'

    @staticmethod
    def query(pid, vid):
        """
        @summary: 查询
        @param pid: str, 产品id 
        @param vid: str, 投票id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##{}##'.format(VoteCommentCache._head, pid, vid))

    @staticmethod
    def update(pid, vid, value):
        """
        @summary: 更新
        @param pid: str, 产品id 
        @param vid: str, 投票id
        @param value: str, 值
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.06.03  新增接口     完成
               2019.xx.xx
        """
        key = '{}##{}##{}##'.format(VoteCommentCache._head, pid, vid)
        return cs.set(key, value)


class SurveyCommentCache(object):
    """
    @summary: 问卷缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.16  新增     完成
           2019.xx.xx
    """
    # key: 业务##问卷ID##产品ID##
    # value: [{'Tag': xxx, 'Count': xx}, ]
    _head = 'Survey'

    @staticmethod
    def query(pid, sid):
        """
        @summary: 查询记录
        @param pid: str, 产品id  
        @param sid: str, 问卷id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##{}##'.format(SurveyCommentCache._head, sid, pid))


class CounterCommentCache(object):
    """
    @summary: 利率点评缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.16  新增     完成
           2019.xx.xx
    """
    # key: 业务##产品ID##
    # value: [count1, count2, count3, count4, count5]
    _head = 'Counter'

    @staticmethod
    def query(pid):
        """
        @summary: 查询记录
        @param pid: str, 产品id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.16  新增接口     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##'.format(CounterCommentCache._head, pid))


class CounterCommentStatsCache(object):
    """
    @summary: 利率点评统计
              缓存服务操作封装
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    # key: 业务##产品ID##
    # value: 有效点评数
    _head = 'CounterStats'

    @staticmethod
    def query(pid):
        """
        @summary: 查询记录
        @param pid: str, 产品id 
        @return: int, 成功返回数量 
                      失败返回None 
        --------------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##'.format(CounterCommentStatsCache._head, pid))


class SurveyCommentStatsCache(object):
    """
    @summary: 问卷点评统计
              缓存服务操作封装
    --------------------------------------------
    @note: 2019.02.25  新增     完成
           2019.xx.xx
    """
    # key: 业务##问卷ID##产品ID##
    # value: 有效点评数
    _head = 'SurveyStats'

    @staticmethod
    def query(pid, sid):
        """
        @summary: 查询记录
        @param pid: str, 产品id
        @param sid: str, 问卷id
        @return: 成功返回dict,
                 失败返回None
        ----------------------------------------
        @note: 2019.02.25  新增     完成
               2019.xx.xx
        """
        return cs.get('{}##{}##{}##'.format(SurveyCommentStatsCache._head, sid, pid))
