# coding=utf-8

"""
redis数据库
基本操作封装

TODO: 1. 数据库连接; 
      2. 基本操作(查询、插入、更新);

REMARK: 目前仅支持读写字符串
"""

import redis
#
from ...base.comp import BaseComponent


class RedisAgency(BaseComponent):
    """
    @summary: redis数据库
              基本操作封装
    """
    __host = '' 
    __port = '' 
    __db = 0
    __password = '' 

    def __init__(self, params={}):
        """
        @summary: 构造器
        """
        super(RedisAgency, self).__init__(params)
        # 数据库参数
        self.__host = self._load_param(params, 'Host', '') 
        self.__port = self._load_param(params, 'Port', '')
        self.__passowrd = self._load_param(params, 'Password', '')
        self.__db = self._load_param(params, 'DbIndex', 0)
        # 初始化连接池
        self.__conn = self.__connect()
        pass

    def __connect(self):
        """
        @summary: redis连接
        @return: StrictRedis对象, 返回redis协议对象,
                 连接失败返回None
        @remark: redis目前不设置连接持续, 
        """
        conn = None
        try:
            conn = redis.ConnectionPool(host=self.__host, 
                                        port=self.__port, 
                                        db=self.__db)#,
                                        #password=self.__password)
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.ConnectionError) as e:
            self.log.error('[connect] fails ', repr(e))
        except Exception as e: 
            self.log.error('[connect] other fails ', repr(e))
        return conn

    def get(self, key):
        """
        @summary: get操作
        @param key: str: 键
        @return: str, 查询结果, 否则返回None 
        """
        res = None 
        try:
            rds = redis.StrictRedis(connection_pool=self.__conn)
            res = rds.get(key)
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.BusyLoadingError,
                redis.exceptions.ConnectionError,
                redis.exceptions.TimeoutError) as e:
            self.log.error('[get] fails ', repr(e))
        except Exception as e: 
            self.log.error('[get] other fails ', repr(e))
        #print('redis get: ', res)
        return res

    def set(self, key, value):
        """
        @summary: get操作
        @param key: str, 键
        @param value: str, 值 
        @return: bool, 成功/失败 
        """
        res = None
        try:
            rds = redis.StrictRedis(connection_pool=self.__conn) 
            res = rds.set(key, value)
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.BusyLoadingError,
                redis.exceptions.ConnectionError,
                redis.exceptions.TimeoutError) as e:
            self.log.error('[set] fails ', repr(e))
        except Exception as e: 
            self.log.error('[set] other fails ', repr(e))
        return res

    def mget(self, keys):
        """
        @summary: mget操作
        @param keys: list: 键列表
        @return: list, 查询结果, 
                       否则对应key位置返回None 
        """
        res = [] 
        try:
            db = redis.StrictRedis(connection_pool=self.__conn)
            res = db.mget(keys)
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.BusyLoadingError,
                redis.exceptions.ConnectionError,
                redis.exceptions.TimeoutError) as e:
            self.log.error('[mget] fails ', repr(e))
        except Exception as e: 
            self.log.error('[mget] other fails ', repr(e)) 
        return res

    def mset(self, kvs):
        """
        @summary: mset操作
        @param kvs: dict, 键值集合 
        @return: bool, 成功/失败 
        """
        res = False 
        try:
            rds = redis.StrictRedis(connection_pool=self.__conn)
            res = rds.mset(kvs)
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.BusyLoadingError,
                redis.exceptions.ConnectionError,
                redis.exceptions.TimeoutError) as e:
            self.log.error('[mset] fails ', repr(e))
        except Exception as e:
            self.log.error('[mset] other fails ', repr(e))
        return res

    def remove(self, key):
        """
        @summary: 删除操作
        @param key: str, 单个key值
        @return: bool, 成功/失败
        -------------------------------------------
        @note: 2019.02.25   bug修正      完成
               2019.xx.xx 
        """
        res = False
        try:
            rds = redis.StrictRedis(connection_pool=self.__conn)
            rds.delete(*rds.keys(key))
            res = True
        except (redis.exceptions.AuthenticationError,
                redis.exceptions.BusyLoadingError,
                redis.exceptions.ConnectionError,
                redis.exceptions.TimeoutError) as e:
            self.log.error('[del] fails ', repr(e))
        except Exception as e:
            self.log.error('[del] other fails ', repr(e))
        return res

    def clean(self):
        """
        @summary: 清空redis
        @return: bool, 成功/失败  
        """
        res = False
        try:
            rds = redis.StrictRedis(connection_pool=self.__conn)
            rds.flushall()
            res = True
        except Exception as e:
            self.log.error('[clean] fails ', repr(e))
        return res
