# -*- coding: utf-8 -*-

"""
(高频数据)缓存服务

TODO: 1. 重置缓存数据

REMARK: 1. 目前为用户会话缓存(key是本地会话, value是微信oponid)
"""

def reloadCache():
    """
    @summary: 重置缓存数据 
    @return: 无
    -------------------------------------------
    @note: 2019.01.05  会话缓存数据     完成
           2019.02.12  增加投票缓存     完成
    """
    # 全局缓存对象
    from ... import cs
    # 
    # 重载会话缓存
    reloadSession()
    return 

def reloadSession():
    """
    @summary: 重载会话
              缓存服务
    @return: bool, 成功/失败
    --------------------------------------------------
    @note: 2019.02.12  加载会话缓存             完成
           2019.02.14  增加清空会话缓存,        完成
                       加载会话变成重载会话
           2019.xx.xx
    """
    # 用户会话数据
    from ...db.service import SessionDB
    # 添加会话缓存
    from ...db.api import addSessionCache as addSession
    # 清空会话缓存
    from ...db.api import cleanSessionCache as cleanSession
    # byte转str
    from common.util.word import toString
    #
    # 清空会话缓存
    cleanSession() 
    # 获取会话记录
    vals = SessionDB().query({'Disabled': 0},['Key','OpenId'])
    # 打包数据[(key,value)]: key是第三方会话code, value目前是oponid
    kvs = [(toString(v[0]), toString(v[1])) for v in vals]
    # 批量导入会话
    res = addSession(kvs)
    return res
