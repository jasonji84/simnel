// 投票题库
const data = {
  id: 'v001',
  topics: [
    {
      id: 'vt01',
      options: [
        {
          tag: 'yes',
          label: '我下款了',
          count: 0
        },
        {
          tag: 'no',
          label: '下款失败',
          count: 0
        }
      ]
    }
  ]
}

function getTopic (index) {
  let res = null
  if (isExist(index)) {
    res = data.topics[index]
  }
  return res
}

function isExist (index) {
  return index < data.topics.length
}

function getVoteId () {
  return data.id
}

module.exports = {
  getVoteId: getVoteId,
  getTopic: getTopic
}
