# coding=utf-8

"""
对外访问接口

TODO: 1. 
      2. 

REMARK: 重要
"""

# 组件基类
from .base.comp import BaseComponent

# ---------------------------------------------
#
# @summary: 数据库 
#
# ---------------------------------------------

# Mysql代理
from .db.mysql.api import MysqlServiceDB as MysqlDB
# Redis代理
from .db.redis.api import CacheService

# ---------------------------------------------
#
# @summary: 基础数据 
#
# ---------------------------------------------

# 产品基本信息
from .db.service.product import ProductBaseDB


# ---------------------------------------------
#
# @summary: 运维数据
#
# ---------------------------------------------

# 产品权限
from .db.service.product import ProductFlagDB
# 热词榜单
from .db.service.keyword import KeywordRankDB


# ---------------------------------------------
#
# @summary: 点评数据
#
# ---------------------------------------------

# 利率点评
from .db.service.comment import CounterCommentDB
# 问卷点评
from .db.service.comment import SurveyCommentDB
# 投票点评
from .db.service.comment import VoteCommentDB


# ---------------------------------------------
#
# @summary: 统计数据
#
# ---------------------------------------------

# 网贷产品统计
from .db.service.product import ProductStatsDB


# ---------------------------------------------
#
# @summary: 工具组件 
#
# ---------------------------------------------
#
# 图片组件
from .comp.oss.image import ImageModel
#
from .util.counter import converse
#
from .util.word import toString
#
from .util.word import hasDigit
#
from .util.word import digitToChinese
#
from .util.find import find_value
# 当前时间
from .util.date import getCurrentTime
# 命名组件
# 随机序列
from .util.naming import randomSeries
# hash命名
from .util.naming import hashNaming
