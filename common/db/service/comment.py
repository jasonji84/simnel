# -*- coding: utf-8 -*-

"""
点评数据表单
数据库服务

TODO: 1. 问卷点评数据;
      2. 投票点评数据;
      3. 利率点评数据;

REMARK: 
"""

from ..mysql.api import MysqlServiceDB


class SurveyCommentDB(MysqlServiceDB):
    """
    @summary: 问卷点评数据 
              表单及数据服务封装
    """
    _tag = 'SurveyComment'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(SurveyCommentDB, self).__init__(params)
        pass


class VoteCommentDB(MysqlServiceDB):
    """
    @summary: 投票点评数据
              表单及数据服务封装
    """
    _tag = 'VoteComment'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(VoteCommentDB, self).__init__(params)
        pass


class CounterCommentDB(MysqlServiceDB):
    """
    @summary: 利率点评数据 
              表单及数据服务封装
    """
    _tag = 'CounterComment'

    def __init__(self, params={}):
        """
        @summary: 构造器
        @param params: dict, 初始参数
        @return:
        """
        super(CounterCommentDB, self).__init__(params)
        pass
