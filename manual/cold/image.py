# coding=utf-8

"""
图片手动上传

TODO: 1. 图片校验
      2. 上传oss

REMARK: Flag有问题  
"""


from offline import queryProduct
#
from milk.config import imagePath
from common.api import ImageModel
from common.api import toString
#
vals = queryProduct([{}, {'OpenState':1, 'Disabled':0}], ['Icon'])
vals = [toString(v[0]) for v in vals]
#
model = ImageModel({'Oss':{'Path':'icon'}})
for va in vals:
    f = '{}/icon/{}'.format(imagePath, va)
    flag = model.upload_one(f)
    print(flag)
