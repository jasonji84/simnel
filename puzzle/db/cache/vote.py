# -*- coding: utf-8 -*-

"""
投票点评
数据服务

TODO: 1. 投票点评缓存服务;

REMARK: 
"""

from common.api import BaseComponent
from common.api import CacheService

class VoteCommentCache(BaseComponent):
    """
    @summary: 投票点评缓存服务
              操作封装
    --------------------------------------------
    @note: 2019.02.13  新增接口     完成
           2019.xx.xx
    """
    # 格式: 业务##产品ID##投票Id##
    # 数据: {'Yes': xx, 'No': xx}
    _head = 'Vote'

    def __init__(self, params={}):
        """
        @summary: 构造函数,
        @param params: dict类型, 配置参数
        @return: 
        """
        super(VoteCommentCache, self).__init__(params)
        #
        self.cs = CacheService()

    def query(self, pid, vid):
        """
        @summary: 查询记录
        @param pid: str, 产品id 
        @param vid: str, 投票id
        @return: 成功返回dict, 
                 失败返回None 
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##{}##'.format(self._head, pid, vid))

    def has(self, pid, vid):
        """
        @summary: 记录是否存在
        @param pid: str, 产品id 
        @param vid: str, 投票id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.get('{}##{}##{}##'.format(self._head, pid, vid)) is not None

    def set(self, pid, vid, data):
        """
        @summary: 新增记录
        @param pid: str, 产品id 
        @param vid: str, 投票id
        @param data: dict, 数据对象
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.set('{}##{}##{}##'.format(self._head, pid, vid), data)

    def mset(self, data):
        """
        @summary: 批量导入数据
        @param data: list, 数据集合, 格式: (pid, vid, data) 
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        kvs = {'{}##{}##{}##'.format(self._head, da[0], da[1]):da[2] for da in data}
        return self.cs.mset(kvs)

    def remove(self, pid, vid):
        """
        @summary: 删除数据
        @param pid: str, 产品id
        @param vid: str, 投票id
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##{}##{}##'.format(self._head, pid, vid))

    def mdel(self):
        """
        @summary: 批量删除数据
        @return: bool, 成功/失败
        --------------------------------------------
        @note: 2019.02.13  新增接口     完成
               2019.xx.xx
        """
        return self.cs.remove('{}##*'.format(self._head))
