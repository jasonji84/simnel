# coding=utf-8

"""
sql宏定义 

TODO: 生成sql语句 

REMARK: 
"""


addSelectedField = lambda fields=[], label='': ','.join(["{}{}".format(addLabel(label=label), fd) for fd in fields])

addLabel = lambda label='', sqlstr="{}.": (sqlstr.format(label) if label else '')

addEqualCondition = lambda fields1=[], label1='', fields2=[], label2='', relation='and': \
                    ' {} '.format(relation).join(['{}{}={}{}'.format(addLabel(label=label1),
                                                                     fd1,
                                                                     addLabel(label=label2),
                                                                     fd2) for (fd1,fd2) in zip(fields1,fields2)])

addLatestCondition = lambda field='', table='', outer_label='', inner_label='', condition_fields=[]: \
                     """
                     {} = (select max({}) 
                           from {} {} 
                           where {})
                           """.format("{}{}".format(addLabel(label=outer_label), field),
                                      "{}{}".format(addLabel(label=inner_label), field),
                                      table,
                                      addLabel(label=inner_label,sqlstr='as {}'),
                                      addEqualCondition(fields1=condition_fields, label1=outer_label, fields2=condition_fields,label2=inner_label))


def addCondition(condition, field, value, relation=None, default_value='', prefix = 'and'):
    """
    @summary: 生成查询条件
    @param condition:   
    @param field:
    @param value:
    @param relation:
    @param default_value:
    @param prefix:
    @return: 
    """
    ret = default_value 
    if condition:
        if isinstance(value, list):
            ret = "{} in ({})".format(field, ','.join(["'{}'".format(v) for v in value]))
        else:
            val = ("'{}'".format(value) if isinstance(value, str) else value)
            rel = ("=" if relation is None else relation)
            ret = "{} {} {}".format(field, rel, val)
    return (' {} {}'.format(prefix, ret) if ret else ret)
