# coding=utf-8

"""
图片资源服务

TODO: 1. 获取图片资源
      2. 获取网贷产品图标 
      3. 获取小程序素材
      4. 获取微信群二维码
  

REMARK: 1. 本地图片资源用于测试
        2. 产品上线使用腾讯云图片服务
        3. 暂不加用户身份校验

-------------------------------------------
NOTE:  2019.03.01   切换为阿里云图片服务 
       2019.xx.xx
"""

import os
from . import *
from ..config import imagePath as image_path


image_bp = Blueprint('image_blueprint', url_prefix='/image')


@image_bp.route('/product/<image>', methods=['GET'])
async def imageHandler(request, image):
    """
    @summary: 获取网贷产品
              图标服务 
    @param request: Request对象
    @param image: str, 图片名
    @return: 跳转获取图片
    """
    # 生成跳转链接
    uri = '/image/lookup?Biz=icon&Image={}'.format(image)
    return response.redirect(uri)


@image_bp.route('/element/<image>', methods=["GET"])
async def elementImageHandler(request, image):
    """
    @summary: 获取小程序素材
              图片服务 
    @param request: Request, 请求对象
    @param image: str, 图片名
    @return: 跳转获取图片
    """
    # 生成跳转链接
    uri = '/image/lookup?Biz=cell&Image={}'.format(image)
    return response.redirect(uri) 


@image_bp.route("/qrcode", methods=["GET"])
async def groupImageHandler(request):
    """
    @summary: 获取微信群二维码
              图片服务
    @param request: Request, 请求对象
    @return: 跳转获取图片
    """
    import glob
    #
    # 获取最新的二维码图片
    files = glob.glob('{}/group/*.png'.format(image_path))
    latest = max(files, key=os.path.getctime)
    # 生成跳转链接
    uri = '/image/lookup?Biz=group&Image={}'.format(latest.split('/')[-1])
    return response.redirect(uri)


@image_bp.route("/productname/<name>", methods=["GET"])
async def productname2ImageHandler(request, name):
    """
    @summary: 通过产品名称
              查询获取图片资源
    @param request: Request, 请求对象
    @param name: str, 网贷产品名称 
    @return: 200, 成功, 跳转获取图片 
             400, 失败
    ---------------------------------------
    @note: 2019.01.15  临时接口    完成
           2019.xx.xx   
    """
    #
    from ..db.service import ProductBaseDB 
    from common.util.word import toString
    #
    res = ProductBaseDB().query({'Name': name.strip(), 'Disabled': 0}, ['Icon']) 
    if len(res) != 1:
        abort(400)
    image = toString(res[0][0]) 
    # 生成跳转链接
    uri = '/image/lookup?Biz=icon&Image={}'.format(image)
    return response.redirect(uri)


@image_bp.route("/lookup", methods=["GET"])
async def imageHandler(request):
    """
    @summary: 获取图片资源
    @param request: Request, 请求对象
    @param resource: str, 图片资源位
    @return: 200, 成功, 返回图片
             400, 失败
    ---------------------------------------
    @note: 2019.01.05  图片跟对外接口分离     完成
           2019.02.26  阿里云图片服务       
    """
    # 参数解析
    # 业务类型(图片目录)
    biz = request.args.get('Biz')
    # 图片文件名
    image = request.args.get('Image')
    # 根目录+业务+具体图片名
    image_file = '{}/{}/{}'.format(image_path, biz, image)
    if os.path.isfile(image_file) == False:
        abort(400)
    return await response.file(image_file)


#@image_bp.route("/producticon/<image>", methods=["GET"])
#async def iconHandler(request, image):
#    """
#    @summary: 获取图片资源
#              oss
#    @param request: Request, 请求对象
#    @param image: str, 图片名
#    @return: 200, 成功, 返回图片
#             400, 失败
#    ------------------------------------------
#    @note: 2019.03.11   新增接口      完成
#           2019.xx.xx  
#    """
#    uri = 'https://image.finartwork.com/image/icon/{}'.format(image)
#    return response.redirect(uri)
