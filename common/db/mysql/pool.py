# -*- coding: UTF-8 -*-

"""
数据库连接池

TODO: 数据库连接(通过连接池实现)

REMARK:
"""

import pymysql 
from DBUtils.PooledDB import PooledDB
#
from . import config
from ...base.comp import BaseComponent


def getConnectionPool():
    """
    @summary: 获取连接池
    """
    return ConnectionPool()


class ConnectionPool(BaseComponent):
    """
    @summary: 数据库
              连接池对象
    """
    __pool = None     # 连接池对象
    
    def __init__(self, params={}):
        """
        @summary: 数据库构造函数,
                  从连接池中取出连接, 并生成操作游标
        @param params: dict类型, 配置参数
        """
        super(ConnectionPool, self).__init__(params)

    def __enter__(self):
        """
        @summary: with进入函数
        @return: 数据库连接池
        """
        self.conn = self.__connect()
        self.cursor = self.conn.cursor()
        return self
    
    def __connect(self):
        """
        @summary: 获取数据库连接 
        """
        if self.__pool is None:
            self.__pool = PooledDB(creator=pymysql, mincached=config.DB_MIN_CACHED, maxcached=config.DB_MAX_CACHED, 
                                   maxshared=config.DB_MAX_SHARED, maxconnections=config.DB_MAX_CONNECYIONS, 
                                   blocking=config.DB_BLOCKING, maxusage=config.DB_MAX_USAGE, 
                                   setsession=config.DB_SET_SESSION,
                                   host=config.DB_HOST, port=config.DB_PORT, 
                                   user=config.DB_USER, passwd=config.DB_PASSWORD, 
                                   db=config.DB_DBNAME, use_unicode=False, charset=config.DB_CHARSET)
        return self.__pool.connection()

    def __exit__(self, type, value, trace):
        """
        @summary: 释放连接池资源
        """
        self.cursor.close()
        self.conn.close()
        return 
